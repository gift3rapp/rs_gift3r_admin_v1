<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('logout', 'Admin\HomeController@logout'); 
Route::prefix('admin')->group(function () {
//homelogin
Route::get('login','Admin\HomeController@home');
Route::post('/login','Admin\HomeController@postlogin');
//changepasword
Route::get('/pages/Admin/changepassword','Admin\HomeController@changepassword');
Route::post('pages/Admin/changepassword', 'Admin\HomeController@changepasswordpost');
// dashboard
Route::get('dashboard', 'Admin\HomeController@dashboard')->middleware('user');
Route::get('dashboard', 'Admin\HomeController@users')->middleware('user');
//user table detail
Route::get('/pages/Admin/users','Admin\HomeController@userDetails')->middleware('user');
//get add user
Route::get('/pages/Admin/add_users','Admin\HomeController@addUser')->middleware('user');
//post add user
Route::post('/pages/Admin/add_users','Admin\HomeController@addUserpost')->middleware('user');
//get merchants
Route::get('/pages/Admin/merchants','Admin\HomeController@merchnatsDetails')->middleware('user');
//form of add merchants
Route::get('/pages/Admin/add_merchants','Admin\HomeController@addMerchants')->middleware('user');
//add merchants
Route::post('/pages/Admin/add_merchants','Admin\HomeController@addMerchantsPost')->middleware('user');
//form of edit merchants
Route::get('/pages/Admin/edit_merchants/{id}','Admin\HomeController@editMerchants')->middleware('user');
//edit merchants
Route::post('/pages/Admin/edit_merchants/{id}','Admin\HomeController@editMerchantsPost')->middleware('user');
//update merchant
Route::post('/pages/Admin/merchants','Admin\HomeController@updatemerchant')->middleware('user');
//update merchant
Route::post('/pages/Admin/merchants_desc/{id}','Admin\HomeController@updatemerchant')->middleware('user');

//desc merchatns
Route::get('/pages/Admin/merchants_desc/{id}','Admin\HomeController@merchantsDesc')->middleware('user');
//desc users
Route::get('/pages/Admin/users_desc/{id}','Admin\HomeController@usersDesc')->middleware('user');
//user_cards
Route::get('/pages/Admin/users_cards/{status}/{id}','Admin\HomeController@usersCard')->middleware('user');

Route::get('/pages/Admin/cards/{id}','Admin\HomeController@showCards')->middleware('user');

Route::get('/pages/Admin/user_cards_history/{id}','Admin\HomeController@userCardsHistory')->middleware('user');
//edit user
Route::get('/pages/Admin/edit_users/{id}','Admin\HomeController@editUser')->middleware('user');
//post add user
Route::post('/pages/Admin/edit_users/{id}','Admin\HomeController@editUserpost')->middleware('user');

//update merchant
Route::post('/pages/Admin/users_enable','Admin\HomeController@updateuser')->middleware('user');

Route::get('/pages/Admin/transaction_history/{user_id}/{id}','Admin\HomeController@history')->middleware('user');

Route::get('/pages/Admin/purchased_trans/{card_id}/{id}','Admin\HomeController@purchasedHistory')->middleware('user');

Route::get('/pages/Admin/received_trans/{card_id}/{id}','Admin\HomeController@receivedHistory')->middleware('user');
//send card hsotory
Route::get('/pages/Admin/sendcards/{id}','Admin\HomeController@sendcardshistory')->middleware('user');
//buy cards
Route::get('/pages/Admin/buycards/{id}','Admin\HomeController@buycardshistory')->middleware('user');
//redeem cards
Route::get('/pages/Admin/redeemcards/{id}','Admin\HomeController@redeemcardshistory')->middleware('user');
//merchant saled

Route::get('/pages/Admin/merchant_saled','Admin\HomeController@Sale')->middleware('user');
//delete card
 Route::post('/pages/Admin/merchant_delete','Admin\HomeController@deletemerchant')->middleware('user');
 Route::post('pages/Admin/users_delete','Admin\HomeController@deleteusers')->middleware('user');
 Route::get('/pages/Admin/mer-all-cards/{id}','Admin\HomeController@showAllMerCards')->middleware('user');
 Route::get('/pages/Admin/mercards/{id}','Admin\HomeController@showMerCards')->middleware('user');
  Route::get('/pages/Admin/total_purchased/{id?}','Admin\HomeController@totalPurchased')->middleware('user');
   Route::get('/pages/Admin/total_redemption/{id?}','Admin\HomeController@totalRedemption')->middleware('user');
});





//route for merachnt dashboard
Route::get('merchantlogout', 'Merchant\AdminController@logout'); 
Route::prefix('merchant')->group(function () {
Route::get('/','Merchant\AdminController@login');

//homelogin
Route::get('login','Merchant\AdminController@login');
Route::post('login','Merchant\AdminController@postlogin');
Route::get('forgetpassword','Merchant\AdminController@forgetpassword');
Route::post('forgetpassword','Merchant\AdminController@postforgetpassword');

Route::get('resetPassword','Merchant\AdminController@resetPassword');
Route::post('resetPassword','Merchant\AdminController@postresetPassword');

Route::get('dashboard','Merchant\AdminController@showDashboard')->middleware('appauth');
Route::get('gift_card/{status?}','Merchant\AdminController@showGiftCard')->middleware('appauth');
Route::get('gift_card/{id}','Merchant\AdminController@showGiftCard')->middleware('appauth');
//delete card
Route::post('gift_card','Merchant\AdminController@deleteCard')->middleware('appauth');

Route::get('/pages/Merchant/card_details/{id}','Merchant\AdminController@showCardDetails')->middleware('appauth');
//cards buy
Route::get('buy_cards','Merchant\AdminController@showbuycarddashboard')->middleware('appauth');
// //cards buy
// Route::get('buy_cards','Merchant\AdminController@showbuycard')->middleware('appauth');
// //send card
 Route::get('send_cards','Merchant\AdminController@showsendcarddashboard')->middleware('appauth');
// //redeem card
 Route::get('redeem_cards','Merchant\AdminController@showredeemcarddashboard')->middleware('appauth');

  Route::get('/pages/Merchant/total_purchased','Merchant\AdminController@totalPurchased')->middleware('user');
   Route::get('/pages/Merchant/total_redemption','Merchant\AdminController@totalRedemption')->middleware('user');

//send card hsotory
Route::get('/pages/Merchant/send_cards/{id}','Merchant\AdminController@showsendcard')->middleware('appauth');
//buy cards
Route::get('/pages/Merchant/buy_cards/{id}','Merchant\AdminController@showbuycard')->middleware('appauth');
//redeem cards
Route::get('/pages/Merchant/redeem_cards/{id}','Merchant\AdminController@showredeemcard')->middleware('appauth');
//changepasword
Route::get('/pages/Merchant/changepassword','Merchant\AdminController@changepassword');
Route::post('pages/Merchant/changepassword', 'Merchant\AdminController@changepasswordpost');
//get add card
Route::get('/pages/Merchant/addgiftcard','Merchant\AdminController@addCard')->middleware('appauth');
//post add card
Route::post('/pages/Merchant/addgiftcard','Merchant\AdminController@addCardpost')->middleware('appauth');
//user table detail
// Route::get('/pages/Merchant/users','Merchant\AdminController@userDetails')->middleware('appauth');
// //desc users
// Route::get('/pages/Merchant/users_desc/{id}','Merchant\AdminController@usersDesc')->middleware('appauth');
// get manage store
Route::get('/pages/Merchant/merchants','Merchant\AdminController@editStore')->middleware('appauth');
//post manage store
Route::post('/pages/Merchant/merchants','Merchant\AdminController@editStorePost')->middleware('appauth');
//merchant profile
Route::get('/pages/Merchant/merchantprofile','Merchant\AdminController@merchantProfile')->middleware('appauth');
//enable disable merchant
Route::post('/pages/Merchant/merchantprofile','Merchant\AdminController@updatemerchantProfile')->middleware('appauth');
//purachaes card of store
Route::get('/pages/Merchant/purchasecards/{id}','Merchant\AdminController@purchaseCards')->middleware('appauth');
//transaction history
Route::get('/pages/Merchant/transactionhistory/{id}','Merchant\AdminController@transactionHistory')->middleware('appauth');
//edit card get form
Route::get('/pages/Merchant/editcard/{id}','Merchant\AdminController@editcard')->middleware('appauth');
//edit card post form
Route::post('/pages/Merchant/editcard/{id}','Merchant\AdminController@editCardpost')->middleware('appauth');







});

//image intervention
Route::get ( 'thumb/uploads/{ext}', 
        function ($ext) {
          
            $img = Image::make ( 'https://www.gift3rapp.com/gift3r/public/assets/uploads/' . $ext );
          
            return $img->response ( $img );
     
        } );
Route::get ( 'thumb/uploads/{ext}/{width}', 
        function ($ext, $width) {
            $img = Image::make ( 'https://www.gift3rapp.com/gift3r/public/assets/uploads/' . $ext );
            $img->resize ( $width, null, 
                    function ($constraint) {
                        $constraint->aspectRatio ();
                    } );
        
            return $img->response ( $img );
         
        } );
Route::get ( 'thumb/uploads/{ext}/{width}/{height}', 
        function ($ext, $width=500, $height=500) {
         $img = Image::make ( 'https://www.gift3rapp.com/gift3r/public/assets/uploads/' . $ext );
             $img->resize($width, $height ,function($constraint){
               	                       $constraint->aspectRatio();
               	                   });
       
            return $img->response ( $img );
         
        } );
        
        	

	
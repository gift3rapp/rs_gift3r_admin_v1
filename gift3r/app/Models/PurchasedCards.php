<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PurchasedCards extends Model
{
    //
    protected $table = 'purchased_cards';

    public static function setDate($timezone,$timestamp)
    {
  		$date = Carbon::createFromFormat('j F,Y',$timestamp,'UTC');
  		$date = $date->setTimezone($timezone);
  		return $date->format('Y');
	}
}

<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class FavouriteStore extends Model
{
    //
  protected $table='favourite_store';

  protected $fillable=['user_id','store_id','created_at','updated_at'];


}

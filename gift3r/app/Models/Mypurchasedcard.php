<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Mypurchasedcard extends Model
{
    //
    protected $table='mypurchased_cards';
    protected $fillable=['sender_id','receiver_id','card_id','amount','usercard','message','created_at'];
   
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
    //
    protected $table='transactions';
    protected $fillable=['sender_id','receiver_id','card_id','balance','transaction_id','status','created_at','updated_at','amount_redeem'];
    
    public static function generateDateRange(Carbon $start_date, Carbon $end_date)
{
    $dates = [];

    for($date = $start_date; $date->lte($end_date); $date->addDay()) {
        $dates[] = $date->format('Y-m-d');
    }

    return $dates;
}

public static function getSearch($request,$status,$query){

  if($status==0){
  if($request->search && !$request->start_date && !$request->end_date){
  $search = $request->search;
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });
}
elseif(!$request->search && $request->start_date && !$request->end_date) {
  $start_date = $request->start_date; //convert date format
  $query->where('transactions.created_at','>=',$start_date)->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });;
}
elseif(!$request->search && !$request->start_date && $request->end_date) {
  $end_date = $request->end_date; //convert date format
  $query->where('transactions.created_at','<=',$end_date)->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });
}
elseif($request->search && $request->start_date && !$request->end_date) {
  $search = $request->search;
  $start_date = $request->start_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','>=',$start_date)->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });
}
elseif($request->search && !$request->start_date && $request->end_date) {
  $search = $request->search;
  $end_date = $request->end_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','<=',$end_date)->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });
}
elseif(!$request->search && $request->start_date && $request->end_date) {
  $start_date = $request->start_date; //convert date format
  $end_date = $request->end_date; //convert date format
  $query->where('transactions.created_at','>=',$start_date)->where('transactions.created_at','<=',$end_date)
        ->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });;
}
elseif($request->search && $request->start_date && $request->end_date) {
  $search = $request->search;
  $start_date = $request->start_date; //convert date format
  $end_date = $request->end_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','<=',$end_date)->where('transactions.created_at','>=',$start_date)
  ->where(function($data) use($status)
                {
                    $data->where('transactions.status',$status)
                         ->orwhere('transactions.status',5);

                });
}
  }
  else{
  if($request->search && !$request->start_date && !$request->end_date){
  $search = $request->search;
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })->where('transactions.status',$status);
}
elseif(!$request->search && $request->start_date && !$request->end_date) {
  $start_date = $request->start_date; //convert date format
  $query->where('transactions.created_at','>=',$start_date)->where('transactions.status',$status);
}
elseif(!$request->search && !$request->start_date && $request->end_date) {
  $end_date = $request->end_date; //convert date format
  $query->where('transactions.created_at','<=',$end_date)->where('transactions.status',$status);
}
elseif($request->search && $request->start_date && !$request->end_date) {
  $search = $request->search;
  $start_date = $request->start_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','>=',$start_date)->where('transactions.status',$status);
}
elseif($request->search && !$request->start_date && $request->end_date) {
  $search = $request->search;
  $end_date = $request->end_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','<=',$end_date)->where('transactions.status',$status);
}
elseif(!$request->search && $request->start_date && $request->end_date) {
  $start_date = $request->start_date; //convert date format
  $end_date = $request->end_date; //convert date format
  $query->where('transactions.created_at','>=',$start_date)->where('transactions.created_at','<=',$end_date)
        ->where('transactions.status',$status);
}
elseif($request->search && $request->start_date && $request->end_date) {
  $search = $request->search;
  $start_date = $request->start_date; //convert date format
  $end_date = $request->end_date; //convert date format
  $query->where(function($db) use($search)
                {
                    $db->where('u.name','like','%'.$search.'%')
                       ->orwhere('r.name','like','%'.$search.'%');

                })
  ->where('transactions.created_at','<=',$end_date)->where('transactions.created_at','>=',$start_date)
  ->where('transactions.status',$status);
}
}
  
  return $query;
}
}

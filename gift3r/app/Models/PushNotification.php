<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;



class PushNotification
{   
    public static function sendpush($body, $data, $fcm_reg_id)
    {    
    	$optionBuilder = new OptionsBuilder;
    	$optionBuilder->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder('null');
		$notificationBuilder->setBody($body)
		                    ->setSound('default');

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData($data);

		$option = $optionBuilder->build();
	$notification = $notificationBuilder->build('null');
		$data = $dataBuilder->build();

		$token = $fcm_reg_id;
		//$token = "AIzaSyBKdx3P48cxusNBNWHde6D4FCk5Odk6j4I";

		$downstreamResponse = FCM::sendTo($token, $option, null, $data);

		$downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		//return Array - you must remove all this tokens in your database
		$downstreamResponse->tokensToDelete(); 

		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$downstreamResponse->tokensToModify(); 

		//return Array - you should try to resend the message to the tokens in the array
		$downstreamResponse->tokensToRetry();

		 // dd($downstreamResponse);

		// return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    //
    protected $table='users';
    protected $fillable = ['id', 'name','email','password','image','status','created_at', 'updated_at','country_code','phone'];

    public function merchants()
    {
       return $this->hasMany('App\Models\Merchant', 'merchant_id', 'id');
    }


   public static function validateSessionId($session_id) 
   {

     $user_data = DB::table('app_login')->where( 'session_id', $session_id )->first();
     if (empty ( $user_data )) return 0;
     else return $user_data->user_id;

    }


    public static function getUser($result)
    {
        return ['user_id'=>$result->id,
                'name'=>$result->name,
                'email'=>$result->email,
                'phone_number'=>$result->phone];
    }
    
}

<?php

namespace App\Models;
use  App\Models;
use DB;
use softDelete;

use Illuminate\Database\Eloquent\Model;

class GiftCards extends Model
{
    //
  protected $table='gift_cards';
    protected $softDelete = true;
      protected $dates = ['deleted_at'];

  const UPLOAD_PICTURE_PATH = "/public/assets/uploads/";
  const PICTURE_PATH = "thumb/uploads/";
  const DEFAULT_PATH = "thumb/uploads/";

  public function  getImageAttribute($value) 
  {
    if ($value) 
    {
      return url ( self::PICTURE_PATH . $value );
    }
    else
    {
      return url ( self::PICTURE_PATH .'0_default.png' );
    }
  }


 


}

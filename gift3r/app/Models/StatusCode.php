<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusCode extends Model
{
    //
    const AVAILABLE_GIFT_CARDS = 1;
    const REDEEMED_GIFT_CARDS = -1;
}

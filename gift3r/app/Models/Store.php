<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Store extends Model
{
    //
  protected $table='stores';

  protected $fillable = [ 'id', 'name', 'image', 'email', 'phone_number', 'website','latitude','longitude','address'];
  const UPLOAD_PICTURE_PATH = "/public/assets/uploads/";
  //const PICTURE_PATH = "thumb/uploads/";
  const PICTURE_PATH = "assets/uploads/";
  const DEFAULT_PATH = "thumb/uploads/";
  public static function getImageAttribute($value) 
  {
    if ($value) 
    {
      return url ( self::PICTURE_PATH . $value );
    }
    else
    {
      return url ( self::PICTURE_PATH .'default_store.png' );
    }
  }

  public function giftcard()
  {
   return $this->belongsTo('App\Models\GiftCards', 'id', 'store_id');
  }
 
 public static function getResponse($result)
 {
// SELECT * FROM `gift_cards` LEFT JOIN stores on stores.id=gift_cards.store_id
    $cards=GiftCards::leftjoin('user_cards','user_cards.gift_card_id','=','gift_cards.id')->where('store_id',$result->s_id)->where('deleted_at',NULL)->select('gift_cards.id as gift_card_id','gift_cards.name as gift_card_name','gift_cards.price as price','user_cards.message as message')->groupBy('user_cards.gift_card_id')->get();

    $user= DB::table('users')->where('id',$result->user_id)->first();
   //print_r(count($cards)); die;
   
    $res=['store_id'=>$result->s_id ?$result->s_id:"",
          'name'=>$result->s_name ?$result->s_name:"",
          'image'=>$result->image,
          'zipcode'=>$result->zipcode ?$result->zipcode:"",
          'address'=>$result->address ?$result->address:"",
          'city'=>$result->city ?$result->city:"",
          'website'=>$result->website ?$result->website:"",
          'phone_no'=>$result->phone_number ?$result->phone_number:"",
          'email'=>$result->email,
          'available_cards'=>$cards ];
      //$res['profile']=$user;

                 
    return $res;
       

       
     
     

  
  }
}

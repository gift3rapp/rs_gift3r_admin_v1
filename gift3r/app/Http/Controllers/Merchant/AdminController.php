<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Store;
use App\Models\Users;
use App\Models\Category;
use App\Models\GiftCards;
use App\Models\UserCards;
use App\Models\StatusCode;
use App\Models\Transaction;
use App\Models\PurchasedCards;
use App\Library\Functions;
use Charts;
use DB;
use SoftDeletes;
use Carbon\Carbon;
use Twilio;
use XmlAuthRequest;
use Plivo\RestClient;



class AdminController extends Controller
{
    //
	public function login(Request $request){
		return View('home/merchantlogin');
	}
  public function forgetpassword(Request $request){
    return View('home/forgotpassword');
  }
   public function postforgetpassword(Request $request){
   $validator = Validator::make ( $request->all (), [ 
      'email' => 'required'
      ]);
    if ($validator->fails ()) 
    {
      return redirect('home/forgotpassword')->withErrors($validator)->withInput();
    }
    $stores = Store::where ( 'email', $request->email )->first ();
    if($stores) {
      $storename=$stores->name;
      $email=$stores->email;

        $string = str_random(32);

      Store::where('email',$request->email)->update(['recovery' => $string]);

      $email = $request->email;

      Mail::send( "home.forgot_password",

        [

        "recovery" => $string,
        "username" => $storename,
        "email"    =>$email

        ],

        function ($m) use ($email) 

        {

          $m->from ( "testaativa@gmail.com", "support" );

          $m->to ($email)

          ->subject ( "Choose a new password for GIFT3R" );

        } 

        );
 return redirect ('merchant/login')->with('success'," link to reset password has been sent to your e-mail");
      

    }
    else
    {
      return redirect()->back()->withErrors(array ("This Email is not valid") )->withInput();
    }

  }

 //reset password form

  public function resetPassword(Request $request)

  {    

    $recovery = Store::where('recovery',$request->key)->first();

    if(! $recovery)

    {

      return response ()->json (['error' => 'bad_request',

        'error_description' =>"link expired"

        ], 200 );

    }

    return View('home.resetpassword',['id'=>$recovery->id]);

  }

  //reset password for new password

  public function postresetPassword(Request $request)

  {

    $validator = Validator::make ($request->all (),['new_password' => 'required', 

      'confirm_password' => 'required|same:new_password', 

      ]);

    if($validator->fails ()) 

    {

      return response()->json(['error' => 'bad_request',

        'error_description' => $validator->getMessageBag()->first () 

        ],400);

    }

    $id=$request->id;

    $user = Store::where('id',$id)->update(['password' => md5($request->new_password),

      'recovery' => 'null'

      ]);

    return redirect ('merchant/login')->with('success',"  password has been  updated.");

  }

	public function postlogin(Request $request){
		$validator = Validator::make ( $request->all (), [ 
			'email' => 'required',
			'password' => 'required']);
		if ($validator->fails ()) 
		{
			return redirect('merchant')->withErrors($validator)->withInput();
		}
		$password = md5 ( $request->password );
		$stores = Store::where ( 'email', $request->email )->where('password',$password)->first ();

		if ($stores) {
		if($stores['status']==1){
			session (
				array (
					'user' => array (
						"role" => 'merchant',
						"email" => $stores->email,
						"name"=>$stores->name,
            "image"=>$stores->image,
						"id" => $stores->id
					)
				) );
			return redirect('merchant/dashboard');
		} 
		elseif($stores['status']==0){
		return redirect()->back()->withErrors(array ("You have been blocked by the admin.") )->withInput();
		}
		}
		else {
			return redirect()->back()->withErrors(array ("wrong Email/Password") )->withInput();
		}

	}

	 //get chnage password
    
    public function changepassword()
    {
   return View('pages/Merchant/changepassword');
    }
    
    //post change password
   
    public function changepasswordpost(Request $request){

        $validator = Validator::make ( $request->all (), [ 
      'password' => 'required',
      'new_password' => 'required', 
      'confirm_password' => 'required|same:new_password', ] );
       if ($validator->fails ()) 
       {
            return redirect ()->back()->withErrors ( $validator );
       }
       $id =  session('user.id');
       $password = md5($request->password);

        // check if entered password is correct
     $user = Store::where('id', '=',$id)->first();
     
     if($user['password']==$password){

      if (md5($request->password, $user->password)) 
      {   
     
          $user->password=md5($request->new_password);

         $user->save();
        
      }
      else
      {
         return redirect ()->back()->withErrors ("Password didnot match");
      }
         return redirect ()->back()->with('success',"Password Successfully changed");

}
else{
return redirect ()->back()->withErrors ("Password didnot match");
}
    }




	public function showDashboard(Request $request){
    $store_id = session('user.id');
    $gift_cards = GiftCards::where('store_id',$store_id)->where('deleted_at',NULL)->get()->count();
    $num_purchased = Transaction::leftJoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('stores','stores.id','gift_cards.store_id')
                          ->where('stores.id',$store_id)
                          ->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })->count();
                          
    
$num_cards = GiftCards::where('store_id',$store_id)->count();
$total_sales = Transaction::leftJoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('stores','stores.id','gift_cards.store_id')
                          ->where('stores.id',$store_id)
                          ->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })->sum('transactions.transaction_balance');
$total_redeemed_amt = Transaction::leftJoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('stores','stores.id','gift_cards.store_id')
                          ->where('stores.id',$store_id)
                          ->where(function($query)
                {
                    $query->where('transactions.status',2);

                })->sum('transactions.transaction_balance');


		
    return View('pages/Merchant/dashboard')->with(['num_purchased'=>$num_purchased,'gift_cards'=>$gift_cards,'num_cards'=>$num_cards,'total_sales'=>$total_sales,'redeemed_amt'=>$total_redeemed_amt]);
	}
 

  public function showbuycarddashboard(Request $request)
  {
    $store_id = session('user.id');
    $query =  DB::table('transactions');
    if($request->id)
    {
       $query->where('sender_id',$request->id);
    }
    $buy_cards=$query->select('users.id as u_id','users.phone as u_phone','users.name','gift_cards.name as g_name','gift_cards.price','transactions.created_at')->leftjoin('stores','transactions.sender_id','stores.id')->leftjoin('users','transactions.sender_id','users.id')->leftJoin('gift_cards','transactions.card_id','gift_cards.id')->where('transactions.status',0)->where('stores.id',$store_id)->orderBy('stores.created_at','DESC')->get();

    $id=Transaction::select('r.id as r_id','users.id as s_id','r.phone as r_phone','users.phone as u_phone','r.name as r_name','users.name')->leftjoin('users','transactions.sender_id','users.id')->leftjoin('users as r','transactions.receiver_id','r.id')->where('transactions.status',1)->orderBy('stores.created_at','DESC')->distinct()->get();




  // $buy_cards=Transaction::select('users.phone as u_phone','users.name','gift_cards.name as g_name','gift_cards.price','transactions.created_at')->leftjoin('stores','transactions.sender_id','stores.id')->leftjoin('users','transactions.sender_id','users.id')->leftJoin('gift_cards','transactions.card_id','gift_cards.id')->where('transactions.status',0)->get();

  return view('pages/Merchant/buy_cards',['cards'=>$buy_cards,'id'=>$id]);
  }
   public function showsendcarddashboard(Request $request)
  {
    $store_id = session('user.id');
    $query = DB::table('transactions');
    if($request->id || $request->r_id){
      if($request->id && !$request->r_id){
      $query->where('sender_id',$request->id);
      }
      elseif($request->r_id && !$request->id){
      $query->where('receiver_id',$request->r_id);
      }
      elseif($request->r_id && $request->id){



       $query->where('receiver_id',$request->r_id)->where('sender_id',$request->id);  
      }
    }
      

    $send_cards = $query->select('r.phone as r_phone','users.phone as u_phone','r.id as r_id', 'r.name as r_name','users.name','gift_cards.name as g_name','gift_cards.price','transactions.created_at','users.id as u_id')->leftjoin('users','transactions.sender_id','users.id','transacations.created_at')->leftjoin('stores','transactions.sender_id','stores.id')
  ->leftjoin('users as r','transactions.receiver_id','r.id')->leftJoin('gift_cards','transactions.card_id','gift_cards.id')->where('transactions.status',1)->where('stores.id',$store_id)->get();


$id=Transaction::select('r.id as r_id','users.id as s_id','r.phone as r_phone','users.phone as u_phone','r.name as r_name','users.name')->leftjoin('users','transactions.sender_id','users.id')->leftjoin('users as r','transactions.receiver_id','r.id')->where('transactions.status',1)->distinct()->get();
return view('pages/Merchant/send_cards',['cards'=>$send_cards,'id'=>$id]);

  }

public function showredeemcarddashboard(Request $request)
  {

$user_id=session('user.id');
       $amount='';
 $query=Usercards::leftjoin('gift_cards','gift_cards.id','user_cards.gift_card_id')->leftjoin('stores','gift_cards.store_id','stores.id')->leftjoin('transactions','user_cards.transaction_id','transactions.id')->where( function($q) use ($user_id)
      {
        $q->where('transactions.sender_id',$user_id)
        ->orWhere('transactions.receiver_id',$user_id);
      })->where('user_cards.user_id',$user_id)->select('transactions.card_id' ,'gift_cards.name as g_name','stores.name as s_name','stores.image');
      $res=$query->get();
//        foreach ($res as $user_cards) 
//     {             
//   $amount=UserCards::getResponse($user_cards ,$user_id,$flag=2);
// }



     $store_id = session('merchant.id');
    
     $query =  DB::table('transactions');
    if($request->id)
    {
       $query->where('sender_id',$request->id);
    }
    $redeem_cards=$query->select('bill_amount.bill_amount as b_a','bill_amount.receipt_num as r_num','users.name','gift_cards.name as g_name','gift_cards.price','transactions.created_at')->leftjoin('stores','transactions.sender_id','stores.id')->leftjoin('users','transactions.sender_id','users.id')->leftJoin('gift_cards','transactions.card_id','gift_cards.id')->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('transactions.status',2)->where('stores.id',$store_id)->get();
    
    $id=Transaction::select('r.id as r_id','users.id as s_id','r.phone as r_phone','users.phone as u_phone','r.name as r_name','users.name')->leftjoin('users','transactions.sender_id','users.id')->leftjoin('users as r','transactions.receiver_id','r.id')->where('transactions.status',2)->distinct()->get();

 

     // $redeem_cards=Transaction::select('users.name','gift_cards.name as g_name','gift_cards.price','transactions.created_at')->leftjoin('stores','transactions.sender_id','stores.id')->leftjoin('users','transactions.sender_id','users.id')->leftJoin('gift_cards','transactions.card_id','gift_cards.id')->where('transactions.status',2)->get();
  return view('pages/Merchant/redeem_cards',['cards'=>$redeem_cards,'id'=>$id,'res'=>$res]);
  }


public function showCardDetails(Request $request){
$num_purchased = Transaction::where('card_id',$request->id)->where(function($query)
                {
                    $query->where('status',0)
                    ->orWhere('status',5);

                })->count();
$total_sales = Transaction::where('card_id',$request->id)->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })->sum('transaction_balance');
$redemption_amount= Transaction::where('card_id',$request->id)->where('status',2)->sum('transaction_balance');
return view('pages/Merchant/card_details')->with(['purchased'=>$num_purchased,'total_sales'=>$total_sales,'redemption_amount'=>$redemption_amount,'id'=>$request->id]);
}
  


	public function showGiftCard(Request $request)
  {
        $store_id = session('user.id');

		if(($request->status==1) || ($request->status==-1)){
			$cards = GiftCards::where('status',$request->status)->where('store_id',$store_id)->orderBy('id')->paginate(10);
		}


		else{
			
		 $cards = GiftCards::where('store_id',$store_id)->where('deleted_at',NULL)->orderBy('id')->paginate(10);
	    }
		return view('pages/Merchant/giftcard')->with(['cards'=>$cards]);
	}
	//logout
	public function logout(Request $request) 
    {
$type= session('user.role');

        $request->session ()
            ->flush ();
            return redirect('merchant/login');
 } 



 //add card get form
  public function addCard(Request $request)
  {
      $amount = Db::table('cards_amount')->get();
      return View('pages/Merchant/addgiftcard')->with(['amount'=>$amount]);
     }
  public function addCardpost(Request $request)
  {
      	 $validator = Validator::make($request->all(),[
         'name' => 'required',
         'price' => 'required',]);
    
     if($validator->fails())
     {
       return Redirect::back()->withErrors($validator )->withInput ();   
     } 
     $store_id = session('user.id');
     
     $total_amount = GiftCards::where('store_id',$store_id)->where('deleted_at',NULL)->sum('price');
     $new_amount = $total_amount + $request->price;
     if($total_amount>=250 || $new_amount>250){
     return redirect::back()->withErrors('You have reached the maximum limit for cards.');
     }
     else{
     $card=new GiftCards();
     $card->name=$request->name;
     $card->price=$request->price;
     $card->store_id=$store_id;
     $card->save();
     return redirect::back()->with('success','sucesfully added');
     }


      }

//manage store
public function editStore(Request $request)
{
  $c_id=Category::get();
   $store_id = session('user.id');
   $store=Store::where('id',$store_id)->where('status',1)->select('image as store_image','website','name','email','phone_number',
                                                                  'latitude','longitude','address','city','zipcode','category_id')->first();
return view('pages/Merchant/merchants',['c_id'=>$c_id,'store'=>$store]);
}
//post edit store details
  public function editStorePost(Request $request)
  {
  
    $validator = Validator::make($request->all(),[
         'name' => 'required',
         'link' => 'required',
         'email'=>'required',
         'number'=>'required',
          'address' => 'required',
         'city' => 'required',
         'code'=>'required',
         'image'=>'image']);
    
     if($validator->fails())
     {
       return Redirect::back()->withErrors($validator )->withInput ();   
     }
     
    $store_id = session('user.id');
     $store=Store::where('id',$store_id)->where('status',1)->first();
      if($request->file('image'))
      {
       $store->image=Functions::upload_picture($request->file('image'), '/public/assets/uploads/' ); 
      }
      else{
      $store->image = $request->alt_image;
      }
       $store->website=$request->link;
       $store->name=$request->name;
       $store->email=$request->email;
       $store->phone_number=$request->number;
       $store->latitude=$request->lat;
       $store->longitude=$request->log;
       $store->address=$request->address;
      $store->city=$request->city;
       $store->zipcode= $request->code ;
         
       $store->category_id= $request->category  ? $request->category : 0;
       $store->save();
        return redirect::back()->with('success','sucesfully edited');


     }
     //merchant profile
     public function merchantProfile(Request $request){
         $store_id = session('user.id');
      $profile=Store::where('id',$store_id)->get();
      return view('pages/Merchant/merchantprofile',['profile'=>$profile]);
     }
     //enbale disable merchant
      public function updatemerchantProfile(Request $request){
         $store_id = session('user.id');
         $store=Store::where('id',$store_id)->update(['status' => $request->status]);
           return Redirect::back();

       }
       //show details of cards that who buy card from store
       public function purchaseCards(Request $request)
       { 
        $user_id = session('user.id');
        $id=$request->id;
        $cards=Transaction::select('users.id as u_id',
                                  'users.name',
                                  'users.email',
                                  'users.phone',
                                  'transactions.card_id'
                                 )
                                  ->leftjoin('users','transactions.sender_id','users.id')
                                  ->where('transactions.status',0)->where('transactions.card_id',$request->id)->distinct()->get();

        $cards_count=Transaction::select('users.id as u_id',
                                  'users.name',
                                  'users.email',
                                  'users.phone',
                                  DB::raw("SUM(transactions.transaction_balance) as bal"))
                                  ->leftjoin('users','transactions.sender_id','users.id')
                                  ->where('transactions.card_id',$request->id)->count();


   // $query=Transaction::leftjoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('user_cards','user_cards.transaction_id','transactions.id')->where('transactions.card_id',$request->id)->select('transactions.card_id' ,'gift_cards.name as g_name');
   //    return $res=$query->get();
        $balance=0;
       foreach ($cards as $card) 
    {  

 $balance=UserCards::getResponse($card ,$card->u_id,$flag=2);
}  

if($balance){
  $final_bal = $balance;
}                       
else{
  $final_bal = 0;
}                          
       
        return view('pages/Merchant/userbuycard',['cards'=>$cards,'id'=>$id,'cards_count'=>$cards_count,'balance'=>$final_bal]);
       }

       //transaction history
       public function transactionHistory(Request $request)

       {
         $id=$request->id;
       
        $history=Transaction::select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')
                            ->where('transactions.sender_id',$request->id)->get();

                           
         return view('pages/Merchant/transactionhistory',['id'=>$id,'history'=>$history]);
       }
       public function editcard(Request $request)
       {
        $id=$request->id;
        $cards = GiftCards::where('id',$id)->first();
        $amount = Db::table('cards_amount')->get();
        return view('pages/Merchant/editcard',['id'=>$id,'cards'=>$cards,'amount'=>$amount]);
       }
       
       public function editCardpost(Request $request)
       {
        $validator = Validator::make($request->all(),[
   'name' => 'required',
   'price' => 'required' ]);

 if($validator->fails())
 {
   return Redirect::back()->withErrors($validator )->withInput ();   
 } 
 
       $total_amount = GiftCards::where('store_id',$request->id)->sum('price');
       $new_amount = $total_amount + $request->price;
       if($total_amount>=250 || $new_amount>250){
       return redirect::back()->withErrors('You have reached the maximum limit for cards.');
       }
       else{
        $cards=GiftCards::where('id',$request->id)->first();
        $cards->name=$request->name;
        $cards->price=$request->price;
        $cards->save();
        return redirect::back()->with('success','sucesfully edited');
        }
       }
       
       public function deleteCard(Request $request)
       {
        $store_id = session('user.id');
      
        $delete_card=GiftCards::where('id',$request->id)->where('store_id',$store_id)->update(['deleted_at'=>1]);
        return redirect::back();
       }


public function showbuycard(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
    $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
    $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=0,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));

 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->where(function($data)
                {
                    $data->where('transactions.status',0)
                         ->orwhere('transactions.status',5);

                })->paginate(50);
                            return view('pages/Merchant/buycards',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,'search'=>$search,
                                                                   'start_date'=>$start_date,'end_date'=>$end_date
                                                                   ]);
}


public function showredeemcard(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
  $id=$request->id;
   $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
        $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=2,$query);
  }
 
  
  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();


 $history=$query->select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->where('transactions.status',2)->paginate(50);
                            // return $request->id;
return view('pages/Merchant/redeem_cards',['history'=>$history,'id'=>$id,'desc'=>$desc,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}
 
 
 public function totalPurchased(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
$user_id = session('user.id');
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
        $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=0,$query);
  }


 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('gift_cards.store_id',$user_id) ->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })->paginate(50);
                            
return view('pages/Merchant/total_purchased',['history'=>$history,'id'=>$id,'desc'=>$desc,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}


public function totalRedemption(Request $request)
{
   $search = '';
    $start_date = '';
    $end_date = '';
$user_id = session('user.id');
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
            $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=2,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));

 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('gift_cards.store_id',$user_id)->where('transactions.status',2)->paginate(50);
                            
return view('pages/Merchant/total_redemption',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}
}

<?php
 
# These values are used to identify and validate the account that you are using. They are mandatory.
$gateway = 'nuvei';			# This is the Nuvei payments gateway that you should use, assigned to the site by Nuvei.
$terminalId = '99303';		# This is the Terminal ID assigned to the site by Nuvei.
$currency = 'USD';			# This is the 3 digit ISO currency code for the above Terminal ID.
$secret = '123456789G1';			# This shared secret is used when generating the hash validation strings. 
						# It must be set exactly as it is in the Nuvei SelfCare  system.
$testAccount = true;
$receiptPageURL = '';	# This should be Url to receipt php file eg. http://localhost:8000/testingPhpCode/PHPHostedPayments/nuvei_receipt_page.php

# These are used only in the case where the response hash is incorrect, which should
# never happen in the live environment unless someone is attempting fraud.
$adminEmail = '';
$adminPhone = '';
 
?>
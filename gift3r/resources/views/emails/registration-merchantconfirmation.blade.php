<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>GIFt3R Email Template - Welcome</title>
        <meta name="viewport" content="width=device-width" />
       <style type="text/css">
            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper { background-color: transparent !important; }
                body[yahoo] .button { padding: 0 !important; }
                body[yahoo] .button a { background-color: #9b59b6; padding: 15px 25px !important; }
            }

            @media only screen and (min-device-width: 601px) {
                .content { width: 600px !important; }
                .col387 { width: 387px !important; }
            }
        </style>
    </head>
    <body bgcolor="#ffffff" style="margin: 0; padding: 0;" yahoo="fix">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td>
        <![endif]-->
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;text-align: center;" class="content">
            <tr>
                <td align="center" bgcolor="#ffffff"
                    style="padding: 40px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;">
                    <img src="http://18.221.106.68/gift3r/public/assets/gift3rlogo.jpg" 
                    style="display: block;" />
                </td>
            </tr>
          
           
          <tr>
        <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
            <table style="width:100%;border:0;" cellspacing="0" cellpadding="0">
             <tr>
                <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
                    Your password is <?php echo $password ?> <br/>
                        Your one-stop platform for lasting memories!<br/>
                        Give the gift of joy to your family, friends, and local community by sending a personalized gift card from your favorite local business!<br/>
                        Choosing a thoughtful gift that’s sure to be appreciated is sometimes quite the challenge! But, with GIFT3Rapp, gifting just got a lot easier.<br/> 
                </td>
             </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
            <table style="width:100%;border:0;" cellspacing="0" cellpadding="0">
             <tr>
                <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">With GIFT3R, it’s easy to give the gift of joy and meaningful experience!</p>
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">1. Download the GIFT3R app for iOS or Android.</p>
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">2. Create your free GIFT3R buyer’s account.</p>
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">3. Discover new and old local favorite shops, cafes, and eateries in your community.</p>
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">4. Look at your friends’ gift card destination wish lists and craft your own.</p>
                    <p style="font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">5. Simply select your business, select your gift card amount, and instantly send a personalized gift card to your loved ones.<br/>Simply shop, buy, and send. It’s that easy!</p>
                </td>
             </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
            <table style="width:100%;border:none;"cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;text-align:center;">
                        <a href="https://www.gift3rapp.com/gift3r/public/merchant">Get Started</a>
                    </td>

                </tr>
                
            </table>
        </td>
    </tr>
           
    </table>
      
    </body>
</html>
<?php $i=1; ?>

<!DOCTYPE html>

<html>

<head>

<title>API test page - GIFT3R</title>

<link rel="stylesheet" href="{{ url('/css/bootstrap.css') }}">



<meta name="viewport" content="width=device-width, initial-scale=1">

<style>

</style>

</head>

<body class="container">

 <h1 class="text-center">GIFT3R APIs</h1>

    <p>

        <b>Error response structure:</b>

    <pre>

        {

            "error": "bad_request",

            "error_description": "The quest is poorly structured or contains invalid data. The email field is required. The password field is required. The device type field is required. The device id field is required. The device token field is required."

        }

        </pre>

             </pre>

    <br>

    <b>Http request status codes:</b>

    <br> 200 - Ok

    <br> 400 - bad_request, when there is missing parameter or there are bad values in a

    request

    <br> 401 - unauthorized, when request is made without session_id or format of

    session_id is invalid

    <br> 403 - forbidden, when session_id is either expired or invalid

    <br>

    </p>

    <p>Note: * fields are mandatory.</p>

   

<div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse" data-target="#otp">

            <h4>{{$i++}}.Send OTP</h4>

        </div>

        <div class="panel-body collapse" id="otp">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/otp"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{url('api/v1/otp')}}" method="post" enctype="multipart/form-data"  files ="true">

                {{csrf_field()}}

              

                <div class="form-group">

                <label class="col-sm-4 control-label">country_code*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="country_code">

                    </div>

               

                    <label class="col-sm-4 control-label">phone_number*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="phone_number">

                    </div>

                     <label class="col-sm-4 control-label">email*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="email">

                    </div>

                   

               

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit" name="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>



    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse" data-target="#user-login">

            <h4>{{$i++}}.Signup</h4>

        </div>

        <div class="panel-body collapse" id="user-login">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/signup"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{url('api/v1/signup')}}" method="post" enctype="multipart/form-data"  files ="true">

                {{csrf_field()}}

              

                <div class="form-group">

               

                    <label class="col-sm-4 control-label">email*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="email">

                    </div>

                     <label class="col-sm-4 control-label">country_code*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="country_code">

                    </div>

                    <label class="col-sm-4 control-label">phone_number*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="phone_number">

                    </div>

                    <label class="col-sm-4 control-label">name*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="name">

                    </div>

                     

                     <label class="col-sm-4 control-label">password</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="password">

                    </div>

                     <label class="col-sm-4 control-label">otp</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="otp">

                    </div>

                    <label class="col-sm-4 control-label">device_type* (iphone/android)</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_type">

                    </div>

                    <label class="col-sm-4 control-label">device_id</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_id">

                    </div>

                    <label class="col-sm-4 control-label">device_token</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_token">

                    </div>

               

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit" name="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse" data-target="#login">

            <h4>{{$i++}}.Login</h4>

        </div>

        <div class="panel-body collapse" id="login">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/login"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{url('api/v1/login')}}" method="post" enctype="multipart/form-data"  files ="true">

                {{csrf_field()}}

              

                <div class="form-group">

               

                    <label class="col-sm-4 control-label">email*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="email">

                    </div>

                    <label class="col-sm-4 control-label">password*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="password" name="password">

                    </div>

                    

                    

                    <label class="col-sm-4 control-label">device_type* (iphone/android)</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_type">

                    </div>

                    <label class="col-sm-4 control-label">device_id</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_id">

                    </div>

                    <label class="col-sm-4 control-label">device_token</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="device_token">

                    </div>

               

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit" name="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>



  <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse" data-target="#password">

            <h4>{{$i++}}.Change Password</h4>

        </div>

        <div class="panel-body collapse" id="password">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/change-password"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{url('api/v1/change-password')}}" method="post" enctype="multipart/form-data"  files ="true">

                {{csrf_field()}}

              

                <div class="form-group">

               

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id">

                    </div>

                    <label class="col-sm-4 control-label">password*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="password" name="password">

                    </div>

                    <label class="col-sm-4 control-label">new_password*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="password" name="new_password">

                    </div>

                 

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit" name="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#user-forgot-password">

            <h4>{{$i++}}. User Forgot Password</h4>

        </div>

        <div class="panel-body collapse" id="user-forgot-password">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/forgot-password"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

              <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/forgot-password') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">email*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="email">

                    </div>

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

     <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#user-logout">

            <h4>{{$i++}}.Logout</h4>

        </div>

        <div class="panel-body collapse" id="user-logout">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/logout"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/logout') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#Listing">

            <h4>{{$i++}}.Store Listing</h4>

        </div>

        <div class="panel-body collapse" id="Listing">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/store-list"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/store-list') }}" method="get">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                    <label class="col-sm-4 control-label">search(city,address,zipcode)</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="search"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">limit</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="limit">

                    </div>

                    <label class="col-sm-4 control-label">page</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="page">

                    </div>

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

<div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#Store">

            <h4>{{$i++}}.Single Store </h4>

        </div>

        <div class="panel-body collapse" id="Store">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/store"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/store') }}" method="get">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                    <label class="col-sm-4 control-label">store_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="store_id"

                            value="">

                    </div>

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#cards">

            <h4>{{$i++}}.My Cards </h4>

        </div>

        <div class="panel-body collapse" id="cards">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/my-cards"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/my-cards') }}" method="get">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                    <label class="col-sm-4 control-label">search</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="search"

                            value="">

                    </div>

                  

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>







     <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#s-cards">

            <h4>{{$i++}}.Send My Cards </h4>

        </div>

        <div class="panel-body collapse" id="s-cards">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/send-cards"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/send-cards') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                    <label class="col-sm-4 control-label">card_id</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="card_id"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">phone_number</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="phone_number"

                            value="">

                    </div>
                    <label class="col-sm-4 control-label">mypurchasedcard_id</label>

                    <div class="col-sm-8">

                <input class="form-control" type="text" name="mypurchasedcard_id"

                            value="">

                    </div>
                    <label class="col-sm-4 control-label">message</label>

                    <div class="col-sm-8">

                <input class="form-control" type="text" name="message"

                            value="">

                    </div>

                  

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

 <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#redeemcards">

            <h4>{{$i++}}.Redeem Card</h4>

        </div>

        <div class="panel-body collapse" id="redeemcards">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/redeem-cards"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/redeem-cards') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">card_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="card_id"

                            value="">

                    </div>

                      <label class="col-sm-4 control-label">receipt_number*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="receipt_number"

                            value="">

                    </div>

                    <label class="col-sm-4 control-label">bill_amount*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="bill_amount"

                            value="">

                    </div>



                  <label class="col-sm-4 control-label">amount*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="amount"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">mypurchasedcard_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="mypurchasedcard_id"

                            value="">

                    </div>


                  

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>
    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#5favourite_store">

            <h4>{{$i++}}.favourite Store</h4>

        </div>

        <div class="panel-body collapse" id="5favourite_store">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/favourite-store"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/favourite-store') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">store_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="store_id"

                            value="">

                    </div>


                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>



      <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#buycards">

            <h4>{{$i++}}.Buy Card</h4>

        </div>

        <div class="panel-body collapse" id="buycards">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/buy-cards"); ?></dd>

                    <dt>Request:</dt>

                    <dd>POST</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/buy-cards') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">session_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="session_id"

                            value="">

                    </div>

                     <label class="col-sm-4 control-label">card_id*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="card_id"

                            value="">

                    </div>

                      <label class="col-sm-4 control-label">phone_number</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="phone_number"

                            value="">

                    </div>
                    <label class="col-sm-4 control-label">message</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="message"

                            value="">

                    </div>

                 

                  

                     

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>

     <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#get_featured">

            <h4>{{$i++}}. Terms&conditons</h4>

        </div>

        <div class="panel-body collapse" id="get_featured">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/terms"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/terms') }}" method="get">

                <div class="form-group">

             

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                       <a href={{url('api/terms&conditons')}}> <input class="btn btn-success" type="submit"></a>

                    </div>

                </div>

            </form>

        </div>

    </div>

     <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#1get_featured">

            <h4>{{$i++}}. Policy&privacy</h4>

        </div>

        <div class="panel-body collapse" id="1get_featured">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/policy"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/policy') }}" method="get">

                <div class="form-group">

             

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                       <a href={{url('api/policy&privacy')}}> <input class="btn btn-success" type="submit"></a>

                    </div>

                </div>

            </form>

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#2get_featured">

            <h4>{{$i++}}. About US</h4>

        </div>

        <div class="panel-body collapse" id="2get_featured">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/about-us"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/about-us') }}" method="get">

                <div class="form-group">

             

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                       <a href={{url('api/about-us')}}> <input class="btn btn-success" type="submit"></a>

                    </div>

                </div>

            </form>

        </div>

    </div>

    
 <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#checknumber">

            <h4>{{$i++}}. check Numbber register or not</h4>

        </div>

        <div class="panel-body collapse" id="checknumber">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/checknumberstatus"); ?></dd>

                    <dt>Request:</dt>

                    <dd>Post</dd>

                </dl>

            </code>
<form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/checknumberstatus') }}" method="post">

                <div class="form-group">

                    <label class="col-sm-4 control-label">country_code*</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="country_code"

                            value="">

                    </div>

                    

                      <label class="col-sm-4 control-label">phone_number</label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" name="phone_number"

                            value="">

                    </div>
                   

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                        <input class="btn btn-success" type="submit">

                    </div>

                </div>

            </form>

        </div>

    </div>
    <div class="panel panel-default">

        <div class="panel-heading" data-toggle="collapse"

            data-target="#5get_featured">

            <h4>{{$i++}}. Terms of Use</h4>

        </div>

        <div class="panel-body collapse" id="5get_featured">

            <code>

                <dl class="dl-horizontal">

                    <dt>url:</dt>

                    <dd><?php echo url("/api/v1/terms-use"); ?></dd>

                    <dt>Request:</dt>

                    <dd>GET</dd>

                </dl>

            </code>

            <form target="_new" class="form-horizontal"

                action="{{ url('/api/v1/terms-use') }}" method="get">

                <div class="form-group">

             

                    <div>&nbsp;</div>

                    <div class="col-sm-8 col-sm-offset-2">

                       <a href={{url('api/terms_of_use')}}> <input class="btn btn-success" type="submit"></a>

                    </div>

                </div>

            </form>

        </div>

    </div>





 <script type="text/javascript" src="{{ url('/js/jquery.js') }}"></script>

    <script type="text/javascript" src="{{ url('/js/bootstrap.min.js') }}"></script>

</body>

</html>


<center><h1>AboutUs</h1></center>
<p>
We are a team of young and genuine individuals with a background in owning and running small businesses. As entrepreneurs, we have succeeded and failed multiple times over our endeavors, and as a result, we have come to value integrity, hard work, and trustworthiness when it comes to building partnerships and working alongside people. We believe in building relationships, and not just a list of contacts. We believe in giving, and if you are here, we want you to believe in giving too.

We appreciate that you have chosen to join our GIFT3R family…We are looking forward to spreading the feeling of giving among friends, families, and communities.

Anastasios Tirkas – Founder

Anastasis Polycarpou – Chief Technology

Panos Linos – Information Technology Consultant

Alexis Karkotis – Cultural Anthropologist
</p>
<center><h1>Privacy Policy</h1></center>
<p>PRIVACY POLICY

Thank you for electing to be a part of our community at GIVEMORE, LLC, an Arizona limited liability company, d/b/a Gift3r (“Gift3r”).  We consider the privacy and security of user information an important component of the services offered on our mobile application and website at www.gift3r.com (collectively, the “Site”).  Any information stored on Gift3r’s platform is treated as confidential.

This Privacy Policy seeks to clearly explain how Gift3r collects and uses information obtained from users in connection with services available on the Site and platform (taken together, the “Service”).  By visiting and using the Site, you consent to the collection and use of your Personal Data (defined hereafter).

IF YOU DO NOT AGREE WITH THE TERMS SET OUT HEREIN, PLEASE DO NOT USE THE SITE AND OUR SERVICE.  IF REQUIRED BY APPLICABLE LAW, WE WILL SEEK YOUR EXPLICIT CONSENT TO PROCESS PERSONAL DATA COLLECTED ON THE SITE OR VOLUNTEERED BY YOU.  ANY CONSENT YOU GIVE WILL BE ENTIRELY VOLUNTARY.

Information We Collect.
While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you (“Personal Data”).  All Personal Data that you provide to us must be true, complete and accurate, and you must notify us of any changes to such Personal Data.  We collect Personal Data that you voluntarily provide to us when registering on the Site, expressing an interest in obtaining information about us or our Service, when participating in activities on the Site or otherwise contacting us.  Personal Data may include, but is not limited to:

Name and Contact Data. We collect contact data that includes your first and last name, e-mail address, phone number, postal address and other similar contact data.
Credentials. We collect passwords, password hints, and similar security information used for authentication and account access.
Payment Data. We collect data necessary to process your payment if you make purchases, such as your credit card number and the security code associated with your credit card.  All payment data is stored by our payment processor and you should review its privacy policies and contact the payment processor directly to respond to your questions.
Social Media Data. We provide you with the option to register using social media account details, like Facebook, Instagram or other social media accounts.  If you provide Gift3r with any of your social media account details, Gift3r may retrieve publicly available information about you from the social media platforms.
Information Collected Through Our Application. If you use our mobile application, we may also collect the following information:
Geo-Location Information. We may request access or permission to and track location-based information from your mobile device, either continuously or while you are using our mobile application, to provide location-based services.  If you wish to change our access or permissions, you may do so in your device’s settings.
Mobile Device Access. We may request access or permission to certain features from your mobile device, including your mobile device’s Bluetooth, calendar, camera, contacts, microphone, reminders, sensors, SMS messages, social media accounts, storage, and other features.  If you wish to change our access or permissions, you may do so in your mobile device’s settings.
Mobile Device Data. We may automatically collect device information (such as your mobile device ID, model and manufacturer), operating system, version information and IP address.
Push Notifications. We may request to send you push notifications regarding your account or the mobile application. If you wish to opt-out from receiving these types of communications, you may turn them off in your device’s settings.]
Facebook Permissions. We by default access your Facebook basic account information, including your name, e-mail, gender, birthday, current city, and profile picture URL, as well as other information that you choose to make public.  We may also request access to other permissions related to your account, such as friends, check-ins, and likes, and you may choose to grant or deny us access to each individual permission.
Automatically Collected Information. We automatically collect certain information when you visit, use or navigate the Site.  This information does not reveal your specific identity, like your name or contact information, but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Site and other technical information.  This information is primarily needed to maintain the security and operation of our Sites, and for our internal analytics and reporting purposes.
Other Sources. We may obtain information about you from other sources, such as public databases, joint marketing partners, social media platforms, as well as from other third-parties. Examples of the information we receive from other sources include: social media profile information (your name, gender, birthday, email, current city, state and country, user identification numbers for your contacts, profile picture, URL and any other information that you choose to make public); marketing leads and search results and links, including paid listings (such as sponsored links).
Cookies. Like many businesses, we also collect information through cookies and similar technologies.  You can find out more about this in Section 6
Requesting Additional Information. At your election, you may request additional information concerning Gift3r.  If you do so, you may need to re-submit certain Personal Data to Gift3r.
Use of .
We use Personal Data collected via our Site for a variety of business purposes described below.  We process your Personal Data for these purposes in reliance on our legitimate business interests (“Business Purposes”), in order to enter into or perform a contract with you (“Contractual Purpose”), with your consent (“Consent”), and/or for compliance with our legal obligations (“Legal Reasons”).  We indicate the specific processing grounds we rely on below.  Gift3r uses the information it collects or receive as follows:

To Facilitate Account Creation and Log In Process With Your Consent. If you choose to link your account with us to a third-party account, such as your Google or Facebook, we use the information you allowed us to collect from those third-parties to facilitate account creation and the log in process.  See the section below named “Social Logins” for further information.
To Send You Marketing And Promotional Communications For Business Purposes and/or With Your Consent. We and/or our third-party marketing partners may use the Personal Data you send us for marketing purposes.  You can opt-out of our marketing e-mails at any time.
To Send Administrative Information To You For Business Purposes, Legal Reasons and/or Possibly Contractual Purposes. We may use your Personal Data in order to send you product, service and new feature information and/or information about changes to our terms, conditions, and policies.
Fulfill and Manage Your Orders For Contractual Reasons. We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Site.
Deliver Targeted Advertising To You For Our Business Purposes and/or With Your Consent. We may use your information to develop and display content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness.
Request Feedback For Our Business Purposes and/or With Your Consent. We may use your information to request feedback and to contact you about your use of our Site.
To Protect Our Sites For Business Purposes and/or Legal Reasons. We may use your information as part of our efforts to keep our Site safe and secure (for example, for fraud monitoring and prevention).
To Enable User-To-User Communications With Your Consent. We may use your information in order to enable user-to-user communications with each user’s consent.
To Enforce Our Terms, Conditions And Policies For Business Purposes, Legal Reasons and/or Possibly Contractual.
To Respond To Legal Requests And Prevent Harm For Legal Reasons. If we receive a subpoena or other legal request, we may need to inspect the data we hold to determine how to respond.
For Other Business Purposes. We may use your information for other Business Purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Site, products, services, marketing and your experience.
By using the Site and our Service, you agree to the collection and use of information in accordance with this Privacy Policy.

Sharing and Transfer of Information.
Currently, Gift3r will not share your Personal Data with any third-party for commercial purposes.  Gift3r may, however, disclose Personal Data if (1) reasonably necessary to perform the Service; (2) consented to by you; (3) otherwise permitted under this Privacy Policy; (3) if Gift3r uses a third-party advertising company to serve ads when you visit the Site, or (4) Gift3r is required to do so by applicable law or regulation, or in the good faith belief that such action is necessary to (i) conform or comply with applicable laws, regulations or legal process, (ii) protect or defend the rights or property of Gift3r or any other user, or (iii) enforce the Terms of Use for both merchants and clients.

Gift3r’s Site may display a third-party hosted “offer wall.”  Such an offer wall allows third-party advertisers to offer virtual currency, gifts, or other items to users in return for acceptance and completion of an advertisement offer.  Such an offer wall may appear in our mobile application and be displayed to you based on certain data, such as your geographic area or demographic information.  When you click on an offer wall, you will leave our mobile application.  A unique identifier, such as your user ID, will be shared with the offer wall provider in order to prevent fraud and properly credit your account.

Gift3r may in its sole and absolute discretion transfer Personal Data to any successor to all or substantially all of its business or assets that concern the Service.  Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.  If you are located outside United States and choose to provide information to us, please note that we transfer the data, including Personal Data, to United States and process it there.

Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.  Gift3r will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.

Security, Employees and Service Providers.
The computers and servers in which we store Personal Data are kept in a secure environment.  Only employees who need the information to perform a specific job, for example, billing or customer service, are granted access to personally identifiable information.  Wherever we collect sensitive information, such as credit card data, that information is encrypted and transmitted to us in a secure way.  You can verify this by looking for a lock icon in the address bar and looking for “https” at the beginning of the address of the web page.

We may employ third-party companies and individuals to facilitate our Service (“Service Providers”), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.  These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.

Gift3r                                                                           . For example, these may be measures:

To protect data against accidental loss;
To prevent unauthorised access, use, destruction or disclosure;
To ensure business continuity and disaster recovery;
To restrict access to personal information;
To conduct privacy impact assessments in accordance with the law and your business policies;
To train staff and contractors on data security;
To manage third party risks, through use of contracts and security reviews.
 

Gift3r keeps Personal Data and User Data for                             .

The security of your Personal Data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.

Third-Party Sites.
The Site may permit you to link to other websites on the Internet, and other websites may contain links to the Site.  These other websites are not under Gift3r control.  The privacy and security practices of websites linked to or from the Site are not covered by this Privacy Policy, and Gift3r is not responsible for the privacy or security practices or the content of such websites.

IP Addresses and Cookies.
When you access the Service by or through a mobile device or computer, Gift3r may collect and use certain information automatically, including, but not limited to, your mobile device unique ID, your IP address to help diagnose problems with its server and to administer the Site, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data.  Your IP address is used to help identify you and to gather broad demographic information.  IP addresses are also used to provide an audit trail in the case of any attempted illegal or unauthorized use of the Site.

We also use “cookies” on this Site.  “Cookies” are pieces of information that a website transfers to your computer’s hard disk for record-keeping purposes.  Cookies in and of themselves do not personally identify users, although they do identify a user’s computer.  For instance, when we use a cookie to identify a user’s computer, you would not have to log in a password more than once, thereby saving time while on our Site.  Cookies can also enable us to track and target the interests of our users to enhance the experience on our Site.  You may be able to set your web browser to refuse cookies.

Proprietary Rights.
You agree that all content and materials delivered via the Site and the Service, or otherwise made available by Gift3r are protected by copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws.  Except as expressly authorized by Gift3r in writing, you agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from such materials or content.  However, you may print or download a reasonable number of copies of the materials or content at this Site for your internal business purposes; provided, however, that you retain all copyright and other proprietary notices contained therein.

Children’s Privacy.
Our Service does not address anyone under the age of 18 (“Children”).  We do not knowingly collect Personal Data from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children provided us with Personal Data, please contact us immediately.  If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.

Opt-Out.
If you change your mind about Gift3r’s use of User Data volunteered by you, you may send Gift3r an email to support@gift3r.com.

.Gift3r reserves the right to change or update this Privacy Policy at any time by posting a notice on the Site.  Information collected by Gift3r is subject to the Privacy Policy that is in effect at the time of use.
If you have any questions regarding this Privacy Policy or your dealings with our Site, please contact Gift3r at info@Gift3r.com.  For support, you may also contact Gift3r at support@Gift3r.com.
Copyright and Trademark Notices.
Unless otherwise indicated, all content provided by Gift3r are copyright © 2018 GIVEMORE, LLC. A ll rights reserved.

Gift3r and the Gift3r’s logo are trademarks of GIVEMORE, LLC.</p>
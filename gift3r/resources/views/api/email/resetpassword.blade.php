@extends('layout/home') @section('login')
  <div class="passwordBox animated fadeInDown">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Reset password</h2>

                    <p>
                        Enter your password 
                    </p>
                    

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" method="get" action="{{url('api/v1/resetpasswordpost')}}">
                                  <input type="hidden" name="id" value="{{$id}}">
                          {{csrf_field()}}
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="new password" name="new_password" required="">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="confirm password" name="confirm_password" required="">
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Update</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
        
    </div>
    @endsection
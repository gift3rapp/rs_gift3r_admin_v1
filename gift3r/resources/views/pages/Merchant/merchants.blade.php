@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Manage Store</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">home</a>
                        </li>
                        <li class="active">
                            <a href="{{url('merchant/pages/Merchant/merchantprofile')}}">View Profile</a>
                        </li> 
                      
                       <li class="active">
                          <strong>manage store</strong>
                        </li> 
              
                    </ol>
                </div>
                </div>

                 @if (Session::get('success'))
    <div class="alert alert-success alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <ul>
           {{Session::get('success')}}
        </ul>
    </div>
    @endif
    @if(count($errors))
     <div class="alert alert-danger alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   
@endif

 <div class="wrapper wrapper-content animated fadeInRight">
              <div class="row">
         <!--       <div class="add col-sm-12 " style="margin-top: -5px; margin-bottom: 11px;">  
            <form action="add_merchants" method="post">
                <a href="merchant/pages/Merchant/merchantprofile"><button class="btn btn-success" type="button" >View Profile</button></a> </form>


            </div> --> 
                   <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit Store Details</h5>
                        </div>
                        <div class="ibox-content">
                  
                 

                       <form class="form-horizontal" action='merchant/pages/Merchant/merchants' method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                            
                 <div class="form-group"><label class="col-lg-2 control-label">name</label>

             <div class="col-lg-6"><input type="text" name="name" placeholder=" name" class="form-control" required="" value="{{$store['name']}}"> 
                                    </div>
                                </div>
                                
                              
                            
                                <div class="form-group"><label class="col-lg-2 control-label">email </label>
           <div class="col-lg-6"><input type="email" name="email" placeholder="email" class="form-control" required="" value="{{$store['email']}}"> 
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-lg-2 control-label">phone number</label>

            <div class="col-lg-6"><input type="text" name="number" placeholder="phone number" class="form-control" required="" value="{{$store['phone_number']}}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">image</label>
                               
                                    <div class="col-lg-6"><input type="file" name="image" placeholder="" class="form-control"> 
                                    <img src="assets/uploads/{{$store['store_image']}}" height="100" width="100">
                                    <input type="hidden" value="{{$store['store_image']}}"" name="alt_image">
                                </div>
                                </div>
                                 <div class="form-group"><label class="col-lg-2 control-label">website link</label>

       <div class="col-lg-6"><input type="text" name="link" placeholder="" class="form-control" required="" value="{{$store['website']}}"> 
                                </div>
                                </div>
                                
                                <div class="form-group"><label class="col-lg-2 control-label">Address</label>

       <div class="col-lg-6"><input type="text" name="address" placeholder="" class="form-control" required="" value="{{$store['address']}}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">city</label>

      <div class="col-lg-6"><input type="text" name="city" placeholder="" class="form-control" required="" value="{{$store['city']}}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">zipcode</label>

    <div class="col-lg-6"><input type="text" name="code" placeholder="" class="form-control" required="" value="{{$store['zipcode']}}"> 
                                </div>
                                </div>
                   
                                
                                
                                 
                                                                                                                                                                                                
                                

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">submit</button>
                                    </div>
                                </div>
                            </form>
                           
                        </div>
                    </div>
                </div>
          </div>  
</div>


@endsection
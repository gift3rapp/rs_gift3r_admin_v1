<style>
.table-responsive table.table.table-striped tr td{
	width: 7%;
	display: table-cell;
	padding-left: 9px;
	padding-right: 11px;
}
.table-responsive table.table.table-striped tr td:last-child {
	text-align: right;
}

</style>
<style media="screen">
a[href]:after {
		content: none !important;
	}
.noPrint {
	display: block !important;
}

.yesPrint {
	display: block !important;
}
</style>

<style media="print">
.noPrint {
	display: none !important;
}

.yesPrint {
	display: block !important;
}
</style>

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Admin</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('merchant/dashboard')}}">home</a>
			</li>
			<li class="active">
				<a href="merchant/send_cards">  <strong>send card</strong></a>     
			</li>
		</ol>
	</div>
</div>


<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			<!-- <form action="add_merchants" method="post">
				<a href="merchant/pages/Merchant/addgiftcard"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Cards</button></a> </form> -->


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Send cards </h5>
						<button class="btn btn-export  btn-success " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'SendCard', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
      width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
   
"><i class="fa fa-file-excel-o" aria-hidden="true"  style="margin-left:2px;"></i> Export</button>
 					<div id="test">

 							@if(!count($cards))
					<!-- <form action="" method="get" role="search">

						<select name="" id="selector2" class="form-control m-b"  style="width: 180px; margin-left:113px;margin-top: -6px;" >
							<option value="">Select User name </option>

						

							
							
						</select>
						<span class="input-group-btn">
							<button type="" class="btn btn-sm btn-danger" 
							style=" margin-top: -47px;
							float: right;
							margin-right: 617px;border-radius: 3px;"
							>
							GO</button>
						</span>
					</form> -->
					@else
				 <form action="{{url('merchant/send_cards')}}" method="get" role="search">
				 <select name="id" id="selector2" class="form-control m-b"  style="width: 180px; margin-left:113px;margin-top: -6px;" >
                        <option value="">Select Sender name </option>
                        
                        @foreach($id as $lists) 
                             
                        <option  value="{{$lists->s_id}}" >
                         {{$lists->name .' ('. $lists->u_phone .')'}} 
                        </option>
                        @endforeach
                     </select>
                  
                   <select name="r_id" id="selector3" class="form-control m-b"  style="width: 187px; margin-left:317px;margin-top: -50px;" >
                        <option value="">Select Receiver name </option>
                       
                        @foreach($id as $lists) 
                             
                        <option value="{{$lists->r_id}}" >
    
                         {{$lists->r_name .' ('. $lists->r_phone .')'}} 
                        </option>
                        @endforeach


                     </select>
                     <span class="input-group-btn">
										<button type="submit" class="btn btn-sm btn-danger" 
   style=" margin-top: -47px;
    float: right;
    margin-right: 417px;border-radius: 3px;"
>
											GO</button>
									</span>
					</form>
					@endif
					</div>
					</div>
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th> User Name</th>
											<th>Reciver Name</th>
											<th>Card name</th>
											<th> Price(In USD)</th>
											<th> Date</th>
											
										</tr>
									</thead>
									<tbody>
										@foreach($cards as $card)  
										<tr>                       
											<td>{{$card->name}}</td>
											<td>{{$card->r_name}}</td>
											<td>{{$card->g_name}}</td>
											<td>{{$card->price}}</td>
										<td style="text-align: left;">{{Carbon\Carbon::parse($card->created_at)->format('d M, Y ')}}</td>
										
										</tr>
										@endforeach


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right"></ul>
											</td>

										</tr>
									</tfoot>


								</table>
								@if(!count($cards))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
	
	  $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[3,4,5,.noPrint]'});
   $('.noPrint').show();
  });
});



</script>











	@endsection
@extends('layout/main') @section('content')
<style media="screen">
a[href]:after {
		content: none !important;
	}
.noPrint {
	display: block !important;
}

.yesPrint {
	display: block !important;
}
</style>

<style media="print">
.noPrint {
	display: none !important;
}

.yesPrint {
	display: block !important;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Admin</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('merchant/dashboard')}}">home</a>
			</li>
			<li class="">
				<a href="merchant/gift_card">  <strong>gift card</strong></a>     
			</li>
			<li class="active">
				<a href="merchant/pages/Merchant/purchasecards/{{$id}}">  <strong>Purchase Cards</strong></a>     
			</li>

		</ol>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">

					<h5 style="color: #EF4036;">Purchase Card </h5>
					<button class="btn btn-export  btn-success " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'Purchasecards', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
      width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
   
"><i class="fa fa-file-excel-o" aria-hidden="true"  style="margin-left:2px;"></i> Export</button>

				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="table-responsive">
							<table class="table table-striped" id="printTable">
								<thead>
									<tr>
										<th>User name</th>
										<th>Email </th>
										<th>Phone Number</th>
										<th>Available Balance(USD)</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($cards as $card)
									<tr>                       
										<td>{{$card->name}}</td>
										<td>{{$card->email}}</td>
										<td>{{$card->phone}}</td>
										<td>{{$balance}}</td>
										<td>
										@if($card->u_id)
										<!--  <a href="merchant/pages/Merchant/transactionhistory/{{$card->u_id}}">
										<button class="btn btn-success" type="button" >Transaction History </button></a> -->



										<!-- <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="merchant/pages/Merchant/send_cards/{{$card->u_id}}" style="color: white" ><i class="fa fa-folder" aria-hidden="true" style="padding: 3px;"> <b>Send Cards</b></i></a></label>  

                                 <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="merchant/pages/Merchant/buy_cards/{{$card->u_id}}" style="color: white" ><i class="fa fa-folder" aria-hidden="true" style="padding: 3px;"> <b>Buy Cards</b></i></a></label>

                                 <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="merchant/pages/Merchant/redeem_cards/{{$card->u_id}}" style="color: white" ><i class="fa fa-folder" aria-hidden="true" style="padding: 3px;"> <b>Redeem Cards</b></i></a></label>     -->



										@endif 
										</td>
									</tr>
									@endforeach
								</tbody>
								<tfoot >
									<tr>
										<td colspan="12">
											<ul class="pagination pull-right"></ul>
										</td>
									</tr>
								</tfoot>
							</table>
							@if(!$cards_count)
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
							@endif
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
	
	  $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[.noPrint]'});
   $('.noPrint').show();
  });
});
</script>
@endsection
<style>
.table-responsive table.table.table-striped tr td{
	display: table-cell;
	padding-left: 9px;
	padding-right: 11px;
}
.table-responsive table.table.table-striped tr td:last-child {
	text-align: right;
}

</style>
<style media="screen">
a[href]:after {
		content: none !important;
	}
.noPrint {
	display: block !important;
}

.yesPrint {
	display: block !important;
}
</style>

<style media="print">
.noPrint {
	display: none !important;
}

.yesPrint {
	display: block !important;
}
</style>

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Gift Cards</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('merchant/dashboard')}}">home</a>
			</li>
			<li class="active">
				  <strong>gift card</strong>    
			</li>
		</ol>
	</div>
</div>


<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			<form action="add_merchants" method="post">
				<a href="merchant/pages/Merchant/addgiftcard"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Cards</button></a> </form>


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Gift cards </h5>
						<button class="btn btn-export  btn-success " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'GiftCard', separator:';', escape:'false' ,ignoreColumn:'[2,3,4]'}); $('.noPrint').show();" style="
      width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
   
"><i class="fa fa-file-excel-o" aria-hidden="true"  style="margin-left:2px;"></i> Export</button>

					</div>
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th> Name</th>
											<th> Price (In USD) </th>
											<th></th>
											<th></th>
											
										</tr>
									</thead>
									<tbody>
										@foreach($cards as $card)  
										<tr>                       
											<td>{{$card->name}}</td>
											<td>{{$card->price}}</td>
											
			<!--<td> <a href="merchant/pages/Merchant/editcard/{{$card->id}}"><button class="btn btn-success noPrint" type="button" >Edit</button></a> </td> -->
<!-- <td style="text-align: left;"><a href="merchant/pages/Merchant/deletecards/{{$card->id}}">
<button class="btn btn-success" type="button" >Delete </button></a> </td> -->
<!-- <td> <form action="{{url('merchant/gift_card')}}" method="POST"
              style="display: inline"
              onsubmit="return confirm('Are you sure to delete card?');">
            <input type="hidden" name="id" value="{{$card->id}}">
            {{ csrf_field() }}
            <button class="btn btn-danger" style=" background-color: #EF4036 !important;">Delete</button>
        </form></td> -->
         <td style="text-align: left;"><a href="merchant/pages/Merchant/card_details/{{$card->id}}"><button class="btn btn-success" type="button" >View</button></a> </td> 
										</tr>
										@endforeach


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right"></ul>
											</td>

										</tr>
									</tfoot>


								</table>
								@if(!count($cards))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
<style>
   .table-responsive table.table.table-striped tr td{
   display: table-cell;
   padding-left: 9px;
   padding-right: 11px;
   }
   .table-responsive table.table.table-striped tr td:last-child {
   text-align: right;
   }
</style>
<style media="screen">
   a[href]:after {
   content: none !important;
   }
   .noPrint {
   display: block !important;
   }
   .yesPrint {
   display: block !important;
   }
</style>
<style media="print">
   .noPrint {
   display: none !important;
   }
   .yesPrint {
   display: block !important;
   }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Users</h2>
      <ol class="breadcrumb">
         <li>
            <a href="{{url('admin/dashboard')}}">Home</a>
         </li>
         <li class="active">
        <strong>Users</strong>    
         </li>
      </ol>
   </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
   <div class="row">
      <div class="add col-sm-12 " style="margin-top: -5px;">
         <form action="add_users" method="post">
            <a href="admin/pages/Admin/add_users"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Users</button></a> 
         </form>
      </div>
      <div class="col-lg-12">
         <div class="ibox float-e-margins">
            <div class="ibox-title" style="min-height: 55px;margin-bottom: 7px;">
               
  
          <form action="admin/pages/Admin/users" method="get" style="display:inline;">
          <div class="col-sm-3">
           <div class="search input-group">
             <input type="text" name="search" placeholder="Search" class="input-sm form-control" value="{{$search}}"> <span class="input-group-btn">
               <button type="submit" class="btn btn-sm btn-primary" style="background-color:#10CDDA ;border-color:#10CDDA ;"> Go</button> </span>
             </div>
           </div>     
         </form>
                  
               
               <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'Users', separator:';', escape:'false' ,ignoreColumn:'[3,4,5,6]',ignoreCSS:'test'}); $('.noPrint').show();" style="
                  width: 68px;
                  height: 30px;
                  padding-top: 4px;
                  padding-left:  5px;
                  margin-right:20px;
                  padding-right:66px;float: right;
                  "><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>
            </div>
            <div class="ibox-content">
               <div class="row">
                 
                  <div class="table-responsive">
                     <table class="table table-striped" id="printTable">
                        <thead>
                           <tr>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Phone Number</th>
                              <th>Action</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <!-- <th>Available balance</th> -->
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($users as $user)
                           <tr>
                              <td>@if($user->name){{$user->name}} @else {{'-'}} @endif</td>
                              <td>@if($user->email){{$user->email}} @else {{'-'}} @endif</td>
                              <td style="text-align: left;">{{$user->phone}}</td>
                              <td style="text-align: left; padding-top: 20px;">
                              <form action="{{url('admin/pages/Admin/users_enable')}}" method="post" >
                                 {{csrf_field()}}
                                 <input type="hidden" name="id" value="{{$user->id}}">
                                   

                                
                                    @if($user->status==1)
                                    <button class="btn btn-primary btn-sm"  type="submit" name="status" value="1">       
                                    Enabled
                                    </button> 
                                    <button class="btn btn-default btn-sm " type="submit" name="status" value="0">
                                    Disable
                                    </button>
                                    @else 
                                    <button class="btn btn-default btn-sm " type="submit" name="status" value="1">
                                    Enable
                                    </button>                                   
                                    <button class="btn btn-danger btn-sm  " type="submit" name="status" value="0">       
                                    Disabled
                                    </button>
                                    @endif
                                
                                 </form>
                                  </td>

     <form action="{{url('admin/pages/Admin/users_delete')}}" id="{{$user->id}}" method="POST"  style="display: none;" onsubmit="return confirm('Are you sure to delete user?');">
         <input type="hidden" name="user_id" value="{{$user->id}}">
         {{ csrf_field() }}
     </form>

<td>                                
<div class="dropdown">
  <button class="btn btn-danger btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Action</button>
  <ul class="dropdown-menu">
    <li><a href="admin/pages/Admin/edit_users/{{$user->id}}">Edit</a></li>
    <li><a href="javascript:void(0)" onClick="deluser('{{$user->id}}');" class="del-user">Delete</a></li>
  </ul>
</div>
  </td>                               
   
                              <td style="text-align: left;">
                                 <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="admin/pages/Admin/cards/{{$user->id}}" style="color: white;" >User Cards</a></label>    
                              </td>
                           </tr>
                           @endforeach
                          
                        </tbody>
                        <tfoot >
                           <tr class="test">
                              <td colspan="12">
                                 <ul class="pagination pull-right ">{{$users->appends(Request::except('page'))->links()}}</ul>
                              </td>
                           </tr>
                        </tfoot>
                     </table>
                     @if(!count($users))
                     <div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@section('scripts')
<script>
function deluser(id)
{
$('#'+id).submit();
}
</script>
@endsection
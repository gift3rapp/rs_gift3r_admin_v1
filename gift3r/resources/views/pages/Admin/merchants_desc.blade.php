<style >
  .table-responsive {
    
    border: 1px solid #3333;
    margin-top: 40px;
}
.wrapper-content {
    padding: 20px 10px 65px !important;
   
}
</style>
@extends('layout/main') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Merchant descriptiom</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="">
                      <a href="{{url('admin/pages/Admin/merchants')}}">merchants</a>     
                        </li>
                      <li class="active">
                       
                        <strong>merchants description</strong>

                        </li>
                    </ol>
                </div>
            </div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header" style="width: 100%; float:  left;">
            <div class="col-sm-4">
                    <div class="ibox ">

                        <div class="ibox-content">
                            <div class="tab-content">
                              <div id="contact-1" class="tab-pane active">
                                    <div class="row m-b-lg">
                                        <div class="col-lg-4 ">
                                            <h2>Profile</h2>

                                            <div class="m-b-sm">
                                             <img  class="" src="{{$descs->image}}" style="width: 200px; height: 200px;">
                                             <h2></h2>
                                            </div>
                                        </div>
                                           
                                    </div>
                                     <td style="text-align: left; padding-top: 20px;">
                                    <form action="admin/pages/Admin/merchants_desc/{{$id}}" method="post">
                                    {{csrf_field()}}
                                   
                                    <input type="hidden" name="id" value="{{$descs->s_id}}">
                                
                                   @if($descs->status==1)
                                 
                              
                                   <button class="btn btn-primary "  type="submit" name="status" value="1">       
                                   Enabled
                                   </button> 
                                   <button class="btn btn-default  " type="submit" name="status" value="0">
                                  Disable
                                   </button>
                         
                                   
                                   @else 

                                   <button class="btn btn-default  " type="submit" name="status" value="1">
                                Enable
                                   </button>                                   
                             <button class="btn btn-danger   " type="submit" name="status" value="0">       
                            Disabled
                               </button>
                               @endif
                                </form>
                               </td>
                         
                                    
                                  
                                </div>

                            </div>

                            </div>
                            </div>

            </div>

           







            <div class="col-sm-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Details</h5>
                     
                    </div>
                     
                    <div class="ibox-content">
 
                        <table class="table" >

                           <tbody >
                           
                            <tr >
                               <td style="border-style: none;"><a data-toggle="tab" href="#contact-1" class="client-link"> Name: </a></td>
                                                    <td style="border-style: none;">{{$descs->name}} </td>
                            </tr>
                            <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Email: </a></td>
                                                    <td style="line-height: 2.42857;" >{{$descs->email}} </td>
                            </tr>

                            <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Phone Number: </a></td>
                                                    <td style="line-height: 2.42857;"> {{$descs->phone_number}}
                                                   </td>
                            </tr>
                            <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Website: </a></td>
                                                   <td style="line-height: 2.42857;"> <a href="{{$descs->website}}">{{$descs->website}}</a> </td>
                            </tr>
                              <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Address: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$descs->address}}</td>
                            </tr>
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link">City:</a></td>
                                                    <td style="line-height: 2.42857;">{{$descs->city}} </td>
                            </tr>
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Zipcode: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$descs->zipcode}}</td>
                            </tr>
                            
                            </tbody>
                        </table>

                    </div>
                  
                    </div>
                </div>
                
                         <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-4 space_div">
                    <a href="admin/pages/Admin/mer-all-cards/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Number of Gift Cards</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;">{{$num_cards}}  </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>
               
                    <div class="col-lg-4 space_div">
                    <a href="admin/pages/Admin/total_purchased/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Total Sale</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;"> 
                              <?php echo round($total_sales,2) .' USD'; ?>
                                 </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>

                     <div class="col-lg-4 space_div">
                    <a href="admin/pages/Admin/total_redemption/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Total Redemption</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;">
                                 <?php $amt = str_replace('-','',$redeemed_amt) ; echo round($amt,2). ' USD';?>
                                  </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div> 
                   
        </div>
       

          
       
                                   
</div>
          
</div>
        
   </div>
              
        </div>
 @endsection

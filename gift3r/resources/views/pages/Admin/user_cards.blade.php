@extends('layout/main') @section('content')
<style media="screen">
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>User Cards</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('admin/dashboard')}}">home</a>
			</li>
			
			<li class="">
			 <a href="admin/pages/Admin/users/"> users</a>     
			</li>
			<li class="active">
			  <strong>user cards</strong>    
			</li>
		

		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">User Cards</h5>
						  <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'UserCards', separator:';', escape:'false' ,ignoreColumn:'[5,6,7]'}); $('.noPrint').show();" style="
  width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>
                          

					</div>
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th>Card Id</th>
											<th>Card name </th>
											<th>Store ID </th>
											<th>Store name</th>
											<th>Available Balance (In USD) </th>
											<th>Purchase Count </th>
											<th>Last Modified Date </th>
											<th></th>
											<th></th>
									
											<th></th>
										</tr>
									</thead>
									<tbody>
									@foreach($cards as $hist)
										<tr>                       
											<td>{{$hist->g_id}}</td>
											<td>{{$hist->g_name}}</td>
											<td>{{$hist->store_id}}</td>
											<td>{{$hist->name}}</td>
											<td>{{$hist->transaction_balance}}</td>
											<td>{{$hist->count}}</td>
											<td> {{Carbon\Carbon::parse($hist->updated_at)->format('d M, Y ')}}</td>
											
											<!-- 	@if($hist->status==0)
												<td ><label class="badge badge-success ">Buy Cards </label></td>
												@elseif($hist->status==1)
												<td ><label class="badge badge-primary ">Gifted Cards </label></td>
												@else
												<td><label class="badge badge-danger ">Redeemed Cards </label></td>
											@endif
 -->

											<td> 
											@if($hist->g_id)
<label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="admin/pages/Admin/transaction_history/{{$id}}/{{$hist->g_id}}" style="color: white" >View Transaction History</a></label>  
											






												@endif	</td>
										</tr>
									@endforeach
									


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right"></ul>
											</td>

										</tr>
									</tfoot>


								</table>
							   @if(!count($cards))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
								</div>
							@endif
							
							</div>
							


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
  
    $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[5,.noPrint]'});
   $('.noPrint').show();
  });
});
</script>

@endsection

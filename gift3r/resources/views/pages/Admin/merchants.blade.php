<style>
  .table-responsive table.table.table-striped tr td{
      width: 28%;
display: table-cell;
padding-left: 9px;
padding-right: 9px;
  }
  .table-responsive table.table.table-striped tr td:last-child {
    text-align: right;
}

</style>
<style media="screen">
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Merchants</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="active">
                       <strong>merchants</strong>     
                        </li>
                    </ol>
                </div>
            </div>

  
<div class="wrapper wrapper-content  animated fadeInRight">
<div class="row">
<div class="add col-sm-12 " style="margin-top: -5px;">  
 <form action="add_merchants" method="post">
   <a href="admin/pages/Admin/add_merchants"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Merchants</button></a> </form>
                      
                 
                             </div>
                                
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="min-height: 55px;
    margin-bottom: 7px;">
                                     <form action="admin/pages/Admin/merchants" method="get" style="display:inline;">
          <div class="col-sm-3">
           <div class="search input-group">
             <input type="text" name="search" placeholder="Search" class="input-sm form-control" value="{{$search}}"> <span class="input-group-btn">
               <button type="submit" class="btn btn-sm btn-primary" style="background-color:#10CDDA ;border-color:#10CDDA ;"> Go</button> </span>
             </div>
           </div>     
         </form>
                            
                            
                            
                             <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'Merchants', separator:';', escape:'false' ,ignoreColumn:'[1,2,3,4,5,6]'}); $('.noPrint').show();" style="
  width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>

                          
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                 
                            <div class="table-responsive">
                                <table class="table table-striped" id="printTable">
                                    <thead>
                                    <tr>
                                     <th> Id</th>
                                       <th> Name</th>
                                       <th> Image </th>
                                        <!-- <th> Email</th>
                                        <th>Phone Number</th>
                                        <th>Website</th> -->
                                         <th>Action</th>
                                       
                                         <th></th>
                                         <th></th>
                                  
                                    </tr>
                                    </thead>
                                    <tbody>
                                 @foreach($merchants as $merch)  
                                    <tr>
                                         <td> {{$merch->id}}</td>
                                       <td>{{$merch->name}}</td>
                                       


                                      <td class="tg" > 
                          @if($merch->image=="http://localhost/GIFT3R/public/thumb/uploads/default.png")
  <img src="{{url('assets/uploads/0_default.png')}}" style="width: 50px; height:50px;" class="img-circle"></td>
                      @else
<img src="{{$merch->image}}" style="width: 50px; height:50px;" class="img-circle">
                     @endif

                                    
                                       <!--  <td>{{$merch->email}}</td>
                                       <td>{{$merch->phone_number}}</td>
                                       <td><a href="{{$merch->website}}">{{$merch->website}}</a></td> -->
                                     

                                        
                                       <form action="{{url('admin/pages/Admin/merchants')}}" method="post" class="form">
                                    {{csrf_field()}}
                                   
                                    <input type="hidden" name="id" value="{{$merch->id}}">
                                    
                                   <td style="text-align: left; padding-top: 20px;">
                                   
                                   @if($merch->status==1)
                                   <button class="btn btn-primary btn-xs"  type="submit" name="status" value="1">       
                                   Enabled
                                   </button> 
                                   <button class="btn btn-default btn-xs " type="submit" name="status" value="0">
                                  Disable
                                   </button>
                         
                                   
                                   @else 
                                   <button class="btn btn-default btn-xs " type="submit" name="status" value="1">
                                Enable
                                   </button>                                   
                             <button class="btn btn-danger btn-xs  " type="submit" name="status" value="0">       
                            Disabled
                               </button>
                               @endif
                               
                               </td>
                              </form>
                                 
                                 
      
      <form action="{{url('admin/pages/Admin/merchant_delete')}}" id="{{$merch->id}}" method="POST"  style="display: none;" onsubmit="return confirm('Are you sure you want to delete the merchant?');">
         <input type="hidden" name="mer_id" value="{{$merch->id}}">
         {{ csrf_field() }}
     </form>  
      
                                 
                                 

<td>                                
<div class="dropdown">
  <button class="btn btn-danger btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Action</button>
  <ul class="dropdown-menu">
    <li><a href="admin/pages/Admin/edit_merchants/{{$merch->id}}">Edit</a></li>
    <li><a href="javascript:void(0)" onClick="deluser('{{$merch->id}}');" class="del-user">Delete</a></li>
  </ul>
</div>
  </td>




                           
                                      
                              

                                   
                                  <td > 
                                <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="admin/pages/Admin/merchants_desc/{{$merch->id}}" style="color: white" >View</a></label>    
                              </td> 
                                      

                                    
                                     
                                    </tr>
                                  
                                   @endforeach

                               
                                       </tbody>
                                      <tfoot >
                                <tr>
                                    <td colspan="12">
                                      <ul class="pagination pull-right ">{{$merchants->appends(Request::except('page'))->links()}}</ul>  
                                    </td>
                                   
                                </tr>
                                </tfoot>
                                   
                               
                                </table>
                                
                                 @if(!count($merchants))
							<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

                            </div>
           

                        </div>
                    </div>
                </div>

            </div>
   </div>
   </div>
  



@endsection

@section('scripts')
<script>
function deluser(id)
{
$('#'+id).submit();
}
</script>
@endsection
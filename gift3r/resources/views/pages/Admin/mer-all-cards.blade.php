<style>
   .table-responsive table.table.table-striped tr td{
   display: table-cell;
   padding-left: 9px;
   padding-right: 11px;
   }
   .table-responsive table.table.table-striped tr td:last-child {
   text-align: right;
   }
</style>
<style media="screen">
   a[href]:after {
   content: none !important;
   }
   .noPrint {
   display: block !important;
   }
   .yesPrint {
   display: block !important;
   }
</style>
<style media="print">
   .noPrint {
   display: none !important;
   }
   .yesPrint {
   display: block !important;
   }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Merchant Cards</h2>
      <ol class="breadcrumb">
         <li>
            <a href="{{url('admin/dashboard')}}">Home</a>
         </li>
                        <li>
                      <a href="{{url('admin/pages/Admin/merchants')}}"> merchants</a>     
                        </li>        
         <li>
            <a href="admin/pages/Admin/merchants_desc/{{$id}}">Merchant Description</a>
         </li>
         <li class="active">
          <strong> <strong>Merchant Cards</strong></strong>     
         </li>
      </ol>
   </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
   <div class="row">

      <div class="col-lg-12">
         <div class="ibox float-e-margins">
            <div class="ibox-title">
               <h5 style="color: #EF4036;">Cards </h5>
               <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'Merchant Cards', separator:';', escape:'false' ,ignoreColumn:'[2,3,4,5,6]'}); $('.noPrint').show();" style="
                  width: 68px;
                  height: 30px;
                  padding-top: 4px;
                  padding-left:  5px;
                  margin-right:20px;
                  padding-right:66px;float: right;margin-top: -6px;
                  "><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>
            </div>
            <div class="ibox-content">
               <div class="row">
                 
                  <div class="table-responsive">
           <table class="table table-striped" id="printTable">
                                    <thead>
                                    <tr>
                                       <th> Giftcard Name</th>
                                       <th> Card balanace (In USD) </th>
                                     
                                  
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cards as $descs)
                                <tr>
                                     <td>{{$descs->g_name}}</td>
                                        <td >
                                   {{$descs->price}}
                               </td>
                            <td> @if($descs->g_id)
                                <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="admin/pages/Admin/mercards/{{$descs->g_id}}" style="color: white" > View</a></label>  

                                




                                @endif  
                              </td> 



                                 </tr>
                                 @endforeach
                                   </tbody>
                               
                                      <tfoot >
                                <tr>
                                    <td colspan="12">
                                        <ul class="pagination pull-right "></ul>
                                    </td>
                                   
                                </tr>
                                </tfoot>
                                   
                               
                                </table>
                               
                                @if(!$descs->g_id)
                                
							<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">
               No results found     
              </div>
              @endif  
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@section('scripts')
<script>
function deluser(id)
{
$('#'+id).submit();
}
</script>
@endsection
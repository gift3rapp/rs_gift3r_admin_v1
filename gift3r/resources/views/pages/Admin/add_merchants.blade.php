@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Merchants</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">home</a>
                        </li>
                        <li>
                            <a href="{{url('admin/pages/Admin/merchants')}}">merchants</a>
                        </li>
                      
                       <li class="active">
                          add merchants
                        </li> 
              
                    </ol>


                </div>




            </div>
           @if (Session::get('success'))
    <div class="alert alert-success alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;
">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
           {{Session::get('success')}}
        </ul>
    </div>
    @endif
    @if(count($errors))
     <div class="alert alert-danger alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;
">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>


   
@endif

 
            <div class="wrapper wrapper-content animated fadeInRight">
              <div class="row">
                   <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add merchants</h5>
                        </div>
                        <div class="ibox-content">
                  
                 

                       <form class="form-horizontal" action='admin/pages/Admin/add_merchants' method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                            
                   <div class="form-group"><label class="col-lg-2 control-label">name</label>

                    <div class="col-lg-6"><input type="text" name="name" placeholder=" name" class="form-control" required=""
                     value="{{ old('name') }}"> 
                                    </div>
                                </div>
                                
                              
                            
                                <div class="form-group"><label class="col-lg-2 control-label">email </label>

                                    <div class="col-lg-6"><input type="email" name="email" placeholder="email" class="form-control" required=""
                                    value="{{ old('email') }}"> 
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-lg-2 control-label">phone number</label>

                                    <div class="col-lg-6"><input type="text" name="number" placeholder="phone number" class="form-control" required=""
                                    value="{{ old('number') }}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">image</label>

                                    <div class="col-lg-6"><input type="file" name="image" placeholder="" class="form-control"> 
                                </div>
                                </div>
                                 <div class="form-group"><label class="col-lg-2 control-label">website link</label>

                                    <div class="col-lg-6"><input type="text" name="link" placeholder="Website" class="form-control" required=""
                                    value="{{ old('link') }}"> 
                                </div>
                                </div>
                                 
                                
                                <div class="form-group"><label class="col-lg-2 control-label">Address</label>

                                    <div class="col-lg-6"><input type="text" name="address" placeholder="Address" class="form-control" required=""
                                    value="{{ old('address') }}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">city</label>

                                    <div class="col-lg-6"><input type="text" name="city" placeholder="City" class="form-control" required=""
                                    value="{{ old('city') }}"> 
                                </div>
                                </div>
                                <div class="form-group"><label class="col-lg-2 control-label">zipcode</label>

                                    <div class="col-lg-6"><input type="text" name="code" placeholder="Zipcode" class="form-control" required=""
                                    value="{{ old('code') }}"> 
                                </div>
                                </div>
                  
                                 
                                                                                                                                                                                                
                                

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">submit</button>
                                    </div>
                                </div>
                            </form>
                           
                        </div>
                    </div>
                </div>
          </div>  
</div>




@endsection
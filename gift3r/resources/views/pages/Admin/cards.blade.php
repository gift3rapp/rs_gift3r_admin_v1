@extends('layout/main') @section('content')
<style media="screen">
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>User Cards</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('admin/dashboard')}}">home</a>
			</li>
			
			<li class="">
			 <a href="admin/pages/Admin/users/"> users</a>     
			</li>
			<li class="active">
			<strong>user cards</strong>    
			</li>
		

		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
	
					<div class="ibox-content">
						<div class="row">
                    <div class="col-lg-4">
                    <a href="admin/pages/Admin/users_cards/0/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title cards" style="background-color: #ec5666;">
                                <h5 style="color: white;display:block;float:none;text-align:center;">Purchased Cards</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color:#eb5766;text-align:center">{{$purchased}}  </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>
                    
                    <div class="col-lg-4">
                    <a href="admin/pages/Admin/users_cards/1/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title cards" style="background-color: #ec5666;">
                                <h5 style="color: white;display:block;float:none;text-align:center;">Received Cards</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #eb5766;text-align:center"> {{$received}} </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>
							
                    <div class="col-lg-4">
                    <a href="admin/pages/Admin/users_cards/2/{{$id}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title cards" style="background-color: #ec5666;">
                                <h5 style="color: white;display:block;float:none;text-align:center;">Sent Cards</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #eb5766;text-align:center;">{{$sent}}   </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
  
    $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[5,.noPrint]'});
   $('.noPrint').show();
  });
});
</script>

@endsection

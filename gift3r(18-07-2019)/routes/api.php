<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'v1/'],function(){
    Route::get('/',function(){
    return \view('api/v1-test');
});
//send otp
Route::post('otp','Api\HomeController@sendOtp');

//signup
Route::post('signup','Api\HomeController@signup');
//login
Route::post('login','Api\HomeController@login');
//change password
Route::post('change-password','Api\HomeController@changePassword');
//forgot password
Route::post('forgot-password','Api\HomeController@forgotPassword');
//get reset password form
Route::get('/resetpassword','Api\HomeController@resetPassword');
//post reset passwrd
Route::get('resetpasswordpost','Api\HomeController@resetPasswordPost');
//logout
Route::post('logout','Api\HomeController@logout');
//store lsiting
Route::get('store-list','Api\HomeController@storeList');
//single store
Route::get('store','Api\HomeController@store');
//my cards
Route::get('my-cards','Api\HomeController@myCards');
//send my card
Route::post('send-cards','Api\HomeController@sendCards');
//send card details
Route::post('sendcarddetails','Api\HomeController@sendcarddetails');
//redeem amount
Route::post('redeem-cards','Api\HomeController@redeemCards');
//buy cards
Route::post('buy-cards','Api\HomeController@buyCards');
// favourite store
Route::post('favourite-store','Api\HomeController@favouritestore');
//payment 
Route::post('payment','Api\HomeController@payment');
Route::post('payment_v1','Api\HomeController@payment_v1');
//favouritestorelist

Route::post('pdfview','Api\HomeController@pdfview');
Route::post('favourite-storelist','Api\HomeController@favouritestorelist');
//myfavouritestorelist
Route::post('myfavourite-storelist','Api\HomeController@myfavouritestorelist');

//contact show list
Route::post('contact-list','Api\HomeController@contactlist');

Route::post('contact-lists','Api\HomeController@contactlists');

//send app url as message
Route::post('sendappurl','Api\HomeController@sendappurl');

// check number is on app or not
Route::post('checknumberstatus','Api\HomeController@checknumberstatus');


//terms&conditions
Route::get("terms", function(){ return View::make("api.terms&conditions"); });
//privacy&policy
Route::get("policy", function(){ return View::make("api.policy&privacy"); });
//about us
Route::get("about-us", function(){ return View::make("api.about-us"); });
//terms of use
Route::get("terms-use", function(){ return View::make("api.terms_of_use"); });
Route::get("braintree", function(){ return View::make("api.braintree_paypal"); });


});
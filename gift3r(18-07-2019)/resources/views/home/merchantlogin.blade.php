<!DOCTYPE html>
<html lang="en">
<head>
  <title>login</title>
  <meta charset="utf-8">
   <link rel="stylesheet" href="font.css">
  <link href='https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900' rel='stylesheet' type='text/css'>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="css/scrolling-nav.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link href="{{url('assets/css/my.css')}}" rel="stylesheet" />
</head>
<body class="login">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    	  <div class="modal-dialog">
				<div class="loginmodal-container">
					<img src="{{url('assets/img/logo.png')}}"><br>
					
					   @if(count($errors))
     <div class="alert alert-danger alert-dismissable" style="
   
    width: 95.5%;
    margin-left: 20px;
">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
 

   
@endif	
 @if (Session::get('success'))
    <div class="alert alert-success alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;
">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
           {{Session::get('success')}}
        </ul>
    </div>
    @endif
					
           <form action="{{url('merchant/login')}}" method="post">
           	<span>Welcome to GIFT3R</span>
              {{csrf_field()}}
                    <input type="email" placeholder="Email" name="email" required="">
             
                    <input type="password" name="password" placeholder="Password" required="">
              
                <input type="submit" name="login" class="login loginmodal-submit" value="Login">

               
            </form>
            <a class="forgot-pass" style="float: right;text-align: right;" href="{{url('merchant/forgetpassword')}}">Forgot Password</a>
				</div>
			</div>
		  </div>
</body>
</html>
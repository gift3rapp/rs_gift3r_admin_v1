<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>gift3r - Invoice</title>
</head>
<body yahoo="fix" style="margin: 0; padding: 50px 10px; font-family: 'Open Sans', sans-serif; -webkit-text-size-adjust: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="max-width: 992px; background-color:#ffffff;border:1px solid #dedede;">
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 20px 40px">
                        <img style="width:60px;display:block;" src="http://18.221.106.68/gift3r/public/assets/gift3r.jpg" alt="logo" />
                    </td>
                    <td style="width:100%;text-align:right;padding: 0px 40px">
                        <h1 style="margin: 0px;font-size:34px">INVOICE:</h1>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 10px 40px;text-align:center;">
                        <p>Thank you for purchasing a Gift3rapp eGift Card. The following message provides information regarding your order.</p>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 30px 40px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <address style="font-style:normal;font-size:15px">
                                        <span><strong>Information for GIFT3Rapp:</strong></span><br>
                                        Name: GIFT4WD Corporation / GIFT3Rapp<br/>
                                        Address: 4400 N. Scottsdale Rd. Scottsdale, AZ. 85251..<br> 
                                        Website : https://www.gift3rapp.com/<br> Email:info@gift3rapp.com <br>Phone #:602.820.8941<br/>
                                    </address>
                                </td>
                                <td>
                                    <div>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word; border:1px solid #e5e5e5;font-size:15px">Invoice Number</td>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px"><?php echo $invoice_number; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word; border:1px solid #e5e5e5;font-size:15px">Order Number</td>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px"><?php echo $orderId; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word; border:1px solid #e5e5e5;font-size:15px">Invoice Date</td>
                                                <td style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px"><?php echo $invoice_date; ?></td>
                                            </tr>
                                         </tbody>
                                    </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
             <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 30px 40px;width:50%;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:100%;">
                                    <address style="font-style:normal">
                                        <span><strong>Customer Information:</strong></span><br>
                                        <?php echo ucfirst(@$items->name); ?><br> <?php echo @$items->email; ?><br/>
                                        <?php echo @$items->phone; ?>
                                    </address>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding: 30px 40px;width:50%;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:100%;">
                                    <address style="font-style:normal">
                                        <span><strong>Description/Location:</strong></span><br>
                                        <?php echo ucfirst(@$store_info->name); ?><br> <?php echo @$store_info->address; ?><br><?php echo @$store_info->phone_number ?>
                                    </address>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 30px 40px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:100%">
                                    <table border="2" cellpadding="6" cellspacing="0" style="color:#333333;vertical-align:middle;width:100%;border:2px solid #e5e5e5;">
                                        <thead>
                                            <tr>
                                                <th scope="col" colspan="1" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:8px 10px;text-align:left;border-top-width:1px;font-size:15px;background-color:#f3f3f3">Qty</th>
                                                <th scope="col" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:8px 10px;text-align:left;border-top-width:1px;font-size:15px;background-color:#f3f3f3">Gift value</th>
                                                <th scope="col" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:5px 10px;text-align:left;border-top-width:1px;font-size:15px;background-color:#f3f3f3">Service Fee</th>
                                                <th scope="col" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:5px 10px;text-align:left;border-top-width:1px;font-size:15px;background-color:#f3f3f3">Sub Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td scope="row" colspan="1" style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word; border:1px solid #e5e5e5;font-size:15px">1</td>
                                                <td scope="row" colspan="1" style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px"><span class="amount"><span class="price-symbol">$</span><?php echo $amount; ?></span></td>
                                                <td scope="row" colspan="1" style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px">Free</td>
                                                <td scope="row" colspan="1" style="color:#333333;vertical-align:middle;padding:5px 10px;text-align:left;word-wrap:break-word;border:1px solid #e5e5e5;font-size:15px"><span class="amount"><span class="price-symbol">$</span><?php echo $amount ?></span></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
           
           <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 30px 40px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size:15px;">If you have any questions or concerns regarding this transaction,<br> please contact us at 602.820.8941 or by email at <a style="color:#FE0103;text-decoration:none;" href="mailto:info@gift3rapp.com">info@gift3rapp.com</a> Gift3rapp customer representatives can be reach by phone Monday - Friday, 9 am - 5 pm (Mountain Time) and Saturday at 9 am.<br/>
                                For additional Information regarding Gift3rapp eGifts, please visit our FAQ area at <a style="color:#FE0103;text-decoration:none;" href="http://gift3r.com/faqs-for-gift-buyer/">http://gift3r.com/faqs-for-gift-buyer/</a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
        </td>
    </tr>
</table>
    
</body>
</html>
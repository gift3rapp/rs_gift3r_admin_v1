<center><h1>Terms&conditions</h1></center>
<p style="color: grey;">
MERCHANT TERMS OF USE AND CONDITIONS
PLEASE READ THESE TERMS OF USE AND CONDITIONS (THE “TERMS”) CAREFULLY.  THESE TERMS CONSTITUTE A BINDING LEGAL AGREEMENT BETWEEN YOU THE MERCHANT, AND GIVEMORE, LLC D/B/A GIFT3R (THIS “AGREEMENT”).  THE WEBSITE (HEREINAFTER DEFINED), APPLICATION AND ITS RELATED SERVICES SHALL HEREINAFTER COLLECTIVELY BE REFERRED TO AS THE “APP.”  BY USING OUR APP, YOU UNCONDITIONALLY AGREE TO BE BOUND BY THESE TERMS, INCLUDING ALL EXCLUSIONS AND LIMITATIONS OF LIABILITY HEREIN, AND WARRANT THAT YOU HAVE FULL AUTHORITY AND CAPACITY TO ENTER INTO THIS AGREEMENT, WHETHER LEGAL OR OTHERWISE.  IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH ANY OF THE TERMS OF THE AGREEMENT, YOU MUST NOT ACCEPT THE AGREEMENT AND SHOULD NOT USE OUR APP.  GIFT3R, IN ITS SOLE AND ABSOLUTE DISCRETION, MAY MODIFY THE TERMS OF THIS AGREEMENT FROM TIME TO TIME.  IF MERCHANT DOES NOT WISH TO ACCEPT THE NEW TERMS, ITS SOLE REMEDY, AND GIFT3R’S SOLE LIABILITY, SHALL BE MERCHANT’S TERMINATION OF THIS AGREEMENT.

By accessing or using the App, “you”, as a “Merchant,” agree to be bound by this Agreement.  GIVEMORE, LLC d/b/a GIFT3R (“GIVEMORE, LLC,” “GIFT3R,” “us,” “we,” “our” or “Company”) (collectively, the “Parties”) provides the App information and services to you, conditioned upon your acceptance and without modification of the terms, conditions, and notices contained herein.  Your use of this App constitutes your agreement to all terms, conditions and notices, including, but not limited to, our Privacy Policy and Merchant Payment, Gateway and Integration Agreement.

THEREFORE, you agree to the following terms and conditions of GIFT3R’s App and services:

SECTION 1. DEFINED TERMS

As used in this Agreement or on the GIFT3R App, these terms have the following definitions:

1.1 “Agreement” has the meaning assigned in the preamble above.

1.2 “Client” refers to the person who buys the Voucher.

1.3 “Client Agreement” means the agreement between the Merchant and the Client who bought the Voucher through the GIFT3R Site or App.

1.4 “CMS” means the content management system provided by GIFT3R to all Merchants to access and manage their sale transactions.

1.5 “Company,” “our,” “us,” or “we,” has the same meaning assigned in the preamble above.

1.6 “e-Gifts” refers to the Merchant’s products and/or services that are available to the Client through our App.

1.7 “Intellectual Property Rights” or “IPR” refers to the laws of any country or jurisdiction in or outside of the United States that are confirmed by law, which includes statues that have been re-enacted or amended.  This includes equity, civil law, common law, or otherwise in connection to any discovery, dramatic work, literary work, musical work, invention, artistic work, trademark, service mark, copyright, database, design (includes 2D or 3D), confidential information, trade secret, semiconductor topography, know-how, material forms, or otherwise as it can be stored, recorded, or embodied in forms encompassing transient or electronic mediums, and as such it includes all applications of rights and all extensions for renewal of rights.

1.8 “Liability” means responsibility for or cause of one or more actions which result in injury and includes: breach of statutory duty, dereliction of duty, breach of contract, misrepresentation, restitution or any other cause that relates to or arises from any association with this Agreement.

1.9 “Limitation of Liability” means the limits of our liability to the Merchant.

1.10 “Merchant” or “you” means the person who sells goods and services through our App in Voucher form to a Client.

1.11 “Merchant Information” means any content, data, or information given by the Merchant through any form (print, electronic, or otherwise), whether or not the information is Merchant owned, given by the Merchant to GIFT3R, uploaded to the App for any other purpose, or whether related directly or indirectly to the Merchant’s authority.

1.12 “Policy” or “Policies” refers to any policy which may be available to the Merchant by GIFT3R, including, but not limited to, the content and style guides.  Company, in its sole and absolute discretion, will notify Merchants through the CMS system regarding its policies and changes.

1.13 “Service” or “service,” whether in the plural or not, means all services provided by us or otherwise made available by us to Merchant through the App.

1.14 “Sign-up” or “Register” are synonymous with the creation of a GIFT3R account on the App.

1.15 “Terms” means the terms and conditions in this Agreement, or any amended versions made by us and posted in the same manner as these.

1.16 “Voucher” refers to an electronic form redeemable for services and/or goods that is created and sold through the GIFT3R App and that a Client may buy from a Merchant.  This Voucher entitles the person receiving the Voucher to exchange it for goods and/or services that are worth the same amount outlined on the Voucher given by the Merchant.  This Voucher is subject to the Terms hereof. 

1.17 “Website” refers to the website www.GIFT3R.com.

SECTION 2. OVERVIEW

2.1 Agreement.  This Agreement is between you and GIFT3R and is a binding legal contract.  By using our Services and App, you agree to all of these Terms.

2.2 About Us.  GIVEMORE, LLC is the owner and operator of the GIFT3R App, and our Company is registered in the United States of America at:

5881 E. Thomas Road, Scottsdale, Arizona 85251

2.3 Services We Provide.  We provide the following services:

(a) The GIFT3R platform provides services through the App. 

(b) We sell Vouchers through the GIFT3R platform.  The Vouchers are created by you and may be redeemed by Clients for your services, goods or products. 

(c) We act as a liaison or intermediary between the Merchant and Client.

2.4 GIFT3R is Not a Merchant.  The GIFT3R App is a sales engine, and as such, GIFT3R acts as a liaison or intermediary between you and the Client.  Any products or services are solely owned and supplied by the Merchant, not GIFT3R. 

2.5 Electronic Communications and Marketing.  When you use the App or send e-mails to GIFT3R, you are electronically communicating with GIVEMORE, LLC.  You consent to receive electronic communications related to your use of the App.  GIVEMORE, LLC will communicate with you by e-mail or by posting notices on the App.  You agree that all agreements, notices, disclosures and other communications that are provided to you electronically satisfy any legal requirement that such communications be in writing.  All notices from GIVEMORE, LLC intended for receipt by a customer shall be deemed delivered and effective when sent to the e-mail address you provide through the App.  In order to serve you better, e-mail communications for contract and administrative purposes are necessary to operate your account.  You may choose to unsubscribe from the App and any electronic communications from us at any time.

2.6 Use of Our Services.  Access is not permissible through any proprietary program—such as, spider, robot, site search and retrieval applications—or any other system, device that can scan, indexes or data mines in our App and content.  Acceptable use of access of our App is through trusted, standard web devices and browsers or similar consumer applications.  Spiders may be used to copy materials from the App through search engines for the App to be found by users and will not be created to store the content in any type of archive.  We reserve the right for this Agreement to be revoked, terminated, or suspended by us at any time and without incurring any liability to you.  It is in our sole and absolute discretion to prohibit you from using our App and Services at any time and for any reason whatsoever.

2.7 Client Agreement.  You have read and understood all terms of the Client Agreement between you and any Client or a prospective Client.  You agree that all purchases and transactions of a Voucher by a Client are subject to the Client Agreement that is entered in between you and the Client.  By acknowledging this Agreement, you understand that your goods, service or products are available to the Client indefinitely, unless the business ceases to exist, or is otherwise covered in this Agreement.

2.8 Removal of Terms and Conditions.  The updated Terms are available at www.GIFT3R.com.  You acknowledge and agree that GIVEMORE, LLC may, in its sole and absolute discretion, modify, add or remove any portion of these Terms at any time, and in any manner, by posting revised Terms on the App.  You shall not amend or modify these Terms under any circumstance.  It is your responsibility to periodically check for any changes we make.  If you continue to use App after any changes to the Terms have been made, you automatically accept the changes.

SECTION 3. LIMITATION OF LIABILITY

The Merchants whose products and services are available on the GIFT3R App are independent contractors and not agents or employees of GIVEMORE, LLC.  GIVEMORE, LLC is not liable for the acts, errors, omissions, representations, warranties, breaches or negligence of any Merchants or for any personal injuries, death, property damage, or other damages arising out of such acts or omissions.  Such damages shall also include, but not be limited to, any loss of goodwill, loss of contracts, loss of business, loss of reputation, loss of revenue, loss of actual or anticipated profits, loss of opportunity, loss of anticipated savings, loss of interest or consequential or special loss (foreseeable, known, or otherwise), loss of the use of money, loss of damage, corruption, data use or loss of expenses resulting therefrom.  Our total liability to you or any other party shall in no circumstance exceed, in aggregate, a sum of more than Thirty-Five Dollars ($35.00).

You acknowledge and agree that you assume full responsibility for the following:

(a) your use of the App for any GIVEMORE, LLC communications with third-parties,

(b) your purchase and use of the products and services that are available through the App;

(c) any information you send or receive during your use of the App may not be secure, and may be intercepted by unauthorized parties;

(d) your use of our App is at your own risk;

(e) the App is made available to you at no charge, as such you acknowledge and agree that, to the fullest extent permitted by applicable law (including, without limitation, consumer protection law), neither GIVEMORE, LLC nor its licensors or providers will be liable for any direct, indirect, punitive, exemplary, incidental, special, consequential or other damages arising out of or in any way related to:

(1) the App, or any other App or resource you access through a link from the App;

(2) any action we take or fail to take as a result of communications you send to us;

(3) any products or services made available or purchased through the App, including any damages or injury arising from any use of such products or services;

(4) any delay or inability to use the App or any information, products or services advertised in or obtained through the App;

(5) the modification, removal or deletion of any content submitted or posted on the App; or

(6) any use of the App, whether based in contract, tort, strict liability or otherwise, even if GIVEMORE, LLC has been advised of the possibility of damages.

(f) it is your responsibility to evaluate the accuracy, completeness or usefulness of any opinion, advice or other content available through the App or obtained from a linked app or resource.  This disclaimer applies, without any limitation, to any damages or injury arising from any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, file corruption, communication-line failure, network or system outage, loss of profits by you, or theft, destruction, unauthorized access to, alteration of, loss or use of any record or data, and any other tangible or intangible loss. 

(g) neither GIVEMORE, LLC nor its licensors, suppliers or third-party content providers shall be liable for any defamatory, offensive or illegal conduct of any user of the App.

(h) your sole remedy for any of the above claims or any dispute with GIVEMORE, LLC is to discontinue your use of the App.

(i) you and GIVEMORE, LLC agree that any cause of action arising out of or related to the App must commence within one (1) year after the cause of action accrues or the cause of action is permanently barred.  Because some jurisdictions do not allow limitations on how long an implied warranty lasts, or the exclusion or limitation of liability for consequential or incidental damages, all or a portion of the above limitation may or may not apply to you.

SECTION 4. ELIGIBILITY AND YOUR RESPONSIBILITIES.

4.1 Who Can Use Our Services.  Our Services cannot be used by minors or other suspended Merchants.  If any Merchant is an individual and not doing business under the auspices of a legal entity, then such individual Merchant must agree that they are over 18 years old; you may not use our App if you do not meet these criteria.  We reserve the right to suspend or refuse access to anyone who does not abide by this Agreement or abuses the rights related to the App.

4.2 Compliance.  You represent and warrant that you have full right and authority to use our Services and to be bound by these Terms.  You agree that you will fully comply with all applicable laws, regulations, statutes, ordinances and the Terms herein.  You agree that all the information you provided us when you Registered on the App is accurate, and if any changes occur, you will notify us immediately or within a reasonable time.  You promise you shall not defraud or attempt to defraud, GIVEMORE, LLC or other users.  You promise you shall not act in bad faith when using our Services.  If GIVEMORE, LLC determines that you acted in bad faith and in violation of any of these Terms, or if GIVEMORE, LLC determines that your actions fall outside of reasonable community standards, GIVEMORE, LLC may, at its sole and absolute discretion, terminate your Account and/or prohibit you from using our Services.  You agree that you shall not:

(a) Download the App, create an Account, or access or use any part of the Service if you are an individual under the age of 18;

(b) Use the Service for any commercial purpose or for the benefit of any third-party in a manner not permitted by these Terms;

(c) Attempt to probe, scan or test the vulnerability of any GIVEMORE, LLC system or network or breach any security or authentication measures;

(d) Access, tamper with or use non-public areas of the Service, GIVEMORE, LLC computer systems, or the computer systems of our providers or partners;

(e) Avoid, bypass, remove, deactivate, impair, descramble or otherwise circumvent any technological measure implemented by GIVEMORE, LLC or any of our providers or any other third-party (including another user) to protect the Service or any part thereof;

(f) Attempt to use the Service on or through any platform or service that is not authorized by GIVEMORE, LLC;

(g) Post, upload, publish, submit, provide access to or transmit any user Content that:

(i) infringes, misappropriates or violates a third-party’s patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy;

(ii) violates or encourages any conduct that would violate any applicable law or regulation or would give rise to civil liability;

(iii) is fraudulent, false, misleading or deceptive;

(iv) is defamatory, obscene, pornographic, vulgar or offensive;

(v) promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group;

(vi) is violent or threatening or promotes violence or actions that are threatening to any other person; or

(vii) promotes illegal or harmful activities or substances.

(h) Upload or transmit (or attempt to upload or transmit) files that contain viruses, Trojan horses, worms, time bombs, cancellous, corrupted files or data, or any other similar software or programs that may damage the operation of the Service or the computers of other users of the Service;

(i) Develop, distribute, use, or publicly inform other members of cheats, automation software, bots, hacks, mods or any other unauthorized third-party software or applications;

(j) Exploit, distribute or publicly inform other users of the Service of any game error or bug which gives users an unintended advantage;

(k) Use “virtual items” in a manner that violates these Terms, including transferring or selling virtual items or fraudulently obtaining or acquiring virtual or other products or services;

(l) Attempt to interfere with, intercept or decipher any transmissions to or from the servers for the Service;

(m) Interfere with, or attempt to interfere with the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, or mail-bombing the Service; or

(n) Encourage or enable any other individual or group to do any of the foregoing.

4.3 Preset Voucher Values.  The Merchant has discretion whether to sell $5, $10, $15, $20, $25, $50, $100, $150 or $250 Vouchers from the services we provide on our platform.

4.4 Insurance.  The Merchant shall maintain at all times, and at its own expense, the required insurance in relation to its business.  GIFT3R, at any time and in its sole and absolute discretion, may ask the Merchant to provide proof of its insurance or any certificates.

SECTION 5. DISCLAIMER OF WARRANTIES

We do not make any express or implied warranties, endorsements or representations whatsoever as to the accuracy of the information or contents, materials contained on the App or the operation or accuracy or appropriateness of the App for any purposes.  The App and all contents are provided “as is.”

SECTION 6. APPLICATION AND SERVICE USAGE RULES

6.1 Service Updates.  You acknowledge and agree that GIVEMORE, LLC may update the Service with or without notifying you.  GIVEMORE, LLC may require that you accept certain updates to the Service and you may also need to update third-party software from time to time to receive the Service.  GIVEMORE, LLC conducts maintenance work on its system from time to time and a portion, or sometimes all, of the Service features will not be available during these maintenance periods.  You also acknowledge and agree to the following:

(a) GIFT3R is a sales engine that allows access to Merchants to sell preset value Vouchers and certificates to its customers.  The Merchants authorize and appoint GIFT3R as its commercial agent that negotiates the sale between the Merchant and the Client for all Voucher sales. 

(c) Any sale or purchase of a Voucher on GIFT3R is solely between the Merchant and the Client.  GIFT3R takes absolutely no part in these contracts.

(d) You acknowledge that transmitting information via the Internet is not completely secure, and for reasons outside of our control, we cannot guarantee that electronic means will reach the intended destination.  You acknowledge that it is not possible for us to provide the service free from errors or faults at all times.

6.2 Registration of Accounts.  You acknowledge and agree to provide us with accurate and up to date information regarding your accounts.  Information we request during Registration or at a later time may include, but is not limited to, business name and address, phone number, e-mail or any other personal information that may be needed to complete your Registration.  You agree that it is solely your responsibility to take all steps necessary to protect your passwords and log in information that are issued to you by GIFT3R.  You agree to notify us immediately if your account is breached and your personal information is compromised.

6.3 Intellectual Property Rights. The Service and all content thereon is protected by copyright, trademark, and other laws of the United States and foreign countries.  Except as expressly provided in these Terms, GIVEMORE, LLC and its licensors exclusively own all right, title and interest in the Service and all content thereon or therein, including all associated intellectual property rights.  You will not remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Service.  You agree that you shall not:

(a) Modify, reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for any underlying software or other intellectual property used to provide the Service without GIVEMORE, LLC’s express and prior written permission;

(b) Use, display, mirror or frame the Service, or any individual element within the Service;

(c) Use the intellectual property of GIVEMORE, LLC, or any GIVEMORE, LLC licensor, to adapt, modify or create derivative works based on such intellectual property;

(d) Rent, lease, loan, trade, sell or re-sell access to the Service or any information therein, in whole or part; or

(e) Use or reproduce any GIVEMORE, LLC licensor, or third-party trademark or logo without the prior express written consent of the owner of such trademark or logo.

6.4 Claims of Intellectual Property Infringement.  GIVEMORE, LLC respects the intellectual property of others and we that ask our users do the same.  You are hereby notified that GIVEMORE, LLC has adopted and reasonably implemented a policy that provides for the termination, in appropriate circumstances, of App users or GIVEMORE, LLC members who are repeat copyright infringers.

6.5 Termination of Accounts.  GIVEMORE, LLC may, in appropriate circumstances, and at its sole and absolute discretion, disable or terminate the accounts or memberships of users who may be infringing the intellectual property of a third-party.  If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide GIVEMORE, LLC’s Copyright Agent the following information (to be effective, the notification must be in writing when provided to our Copyright Agent):

(a) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;

(b) a description of the copyrighted work or other intellectual property that you claim has been infringed, or if multiple copyrighted works are covered by a single notification, a representative list of such works at that App;

(c) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and a description of where the material that you claim is infringing is located on the App;

(d) your address, your telephone number, and your e-mail address;

(e) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent or the law; and

(f) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.

SECTION 7. LEGAL NOTICES

7.1 Address.  Any written correspondences under this Agreement shall be sent by certified mailed, return receipt requested, to GIVEMORE, LLC at the address stated below:

5881 E. Thomas Road Scottsdale, Arizona 85251

7.2 How to Contact Us.  If you have any questions or comments about these Terms or the App, please contact us by email at info@gift3r.com.  GIVEMORE, LLC may update this contact information at any time and without notice to you.  You can check our App for the most up to date contact information.

SECTION 8. MISCELLANEOUS PROVISIONS

8.1 Promotional Offers.  From time to time, GIVEMORE, LLC may offer special promotional offers that may or may not apply to your GIVEMORE, LLC account.  You agree to be bound by any additional terms and conditions for these special offers.

8.2 Severability.  The invalidity or unenforceability of any one or more provisions of this Agreement shall in no way affect any other provision.  If any court of competent jurisdiction determines any provision of this Agreement to be invalid, illegal or unenforceable, that portion shall be deemed severed from the rest, which shall remain in full force and effect as though the invalid, illegal or unenforceable portion had never been a part of the Agreement.

8.3 Headings.  Section headings are for reference purposes only and do not limit the scope or extent of such section. 

8.4 Entire Agreement.  This Agreement constitutes the entire Agreement between the parties in relation to its subject matter.

8.5 Rights of Third-Parties.  This Agreement is made and entered into for the sole protection and benefit of GIVEMORE, LLC and Merchant.  No other persons or entities have any right of action under this Agreement.

8.6 Waiver.  The failure of GIVEMORE, LLC to act with respect to a breach of these Terms by you or others does not waive GIVEMORE, LLC’s right to act with respect to subsequent or similar breaches. 

8.7 Succession.  The terms of this Agreement shall bind and benefit the heirs, legal representatives, successors, and assigns of the parties; provided, however, that Client may not assign this Agreement or assign or delegate any of its rights or obligations without GIVEMORE, LLC’s prior written permission. 

8.8 Arbitration and Dispute Resolution.  If a dispute arises under these Terms between you and GIVEMORE, LLC, such dispute shall be resolved, at the filing party’s election, in either a small claims court or if the amount in dispute exceeds the jurisdictional limit of the small claims court, by final and binding arbitration administered by the National Arbitration Forum or the American Arbitration Association, under their rules for consumer arbitrations.  The venue for all disputes arising under these Terms shall be the City of Phoenix in the State of Arizona.  All disputes in arbitration will be handled solely between the named parties, and not on any representative or class basis.  ACCORDINGLY, YOU ACKNOWLEDGE THAT YOU MAY NOT HAVE ACCESS TO A COURT (OTHER THAN A SMALL CLAIMS COURT) OR TO A JURY TRIAL.  Notwithstanding any other provision of these Terms, GIVEMORE, LLC may resort to court action for injunctive relief at any time in its sole and absolute discretion.  You are responsible for informing yourself of the laws of your jurisdiction and complying with them.

8.9 Governing Law.  This Agreement shall be governed by, and construed in accordance with, the laws of the State of Arizona, without regard to the choice of law rules of that state, except to the extent that any of such laws may now or hereafter be preempted by Federal law.  Merchant consents to the jurisdiction of any Federal or State court within the State of Arizona, submits to venue in such state, and also consents to service of process by any means authorized by Federal law or the law of such state.  Without limiting the generality of the foregoing, Client hereby waives and agrees not to assert by way of motion, defense, or otherwise in such suit, action, or proceeding, any claim that (i) Client is not subject to the jurisdiction of the courts of the above-referenced state or the United States District Court for such state, or (ii) such suit, action, or proceeding is brought in an inconvenient forum, or (iii) the venue of such suit, action, or proceeding is improper.

8.10 Waiver of Jury Trial.  TO THE EXTENT NOT PROHIBITED BY APPLICABLE LAW, MERCHANT HEREBY KNOWINGLY, VOLUNTARILY, AND INTENTIONALLY WAIVES ANY RIGHT TO TRIAL BY JURY OR OF ANY CLAIM, DEMAND, ACTION, OR CAUSE OF ACTION BASED UPON OR ARISING UNDER THIS AGREEMENT OR IN ANY WAY CONNECTED WITH OR RELATED OR INCIDENTAL TO THE DISCUSSIONS, DEALINGS, OR ACTIONS OF THE PARTIES TO THIS AGREEMENT OR EITHER OF THEM (WHETHER ORAL OR WRITTEN) WITH RESPECT THERETO, OR TO THE TRANSACTIONS RELATED THERETO, IN EACH CASE WHETHER NOW EXISTING OR HEREINAFTER ARISING, AT LAW OR IN EQUITY, INCLUDING WITHOUT LIMITATION, CONTRACT CLAIMS, TORT CLAIMS, BREACH OF DUTY CLAIMS, AND ALL OTHER COMMON LAW OR STATUTORY CLAIMS.  MERCHANT HEREBY CONSENTS AND AGREES THAT ANY SUCH CLAIM, DEMAND, ACTION, OR CAUSE OF ACTION SHALL BE DECIDED BY A TRIAL COURT WITHOUT A JURY, AND THAT EITHER PARTY TO THIS AGREEMENT MAY FILE AN ORIGINAL COUNTERPART OR A COPY HEREOF WITH ANY COURT AS WRITTEN EVIDENCE OF THE CONSENT OF THE MERCHANT TO THE WAIVER OF ITS RIGHT TO TRIAL BY JURY.  MERCHANT ACKNOWLEDGES AND AGREES THAT IT HAS RECEIVED FULL AND SUFFICIENT CONSIDERATION FOR THIS PROVISION (AND EACH OTHER PROVISION OF THIS AGREEMENT AND EACH OTHER DOCUMENT TO WHICH IT IS A PARTY) AND THAT THIS PROVISION IS A MATERIAL INDUCEMENT FOR COMPANY TO ENTER THIS AGREEMENT.  MERCHANT FURTHER REPRESENTS AND WARRANTS THAT IT HAS BEEN REPRESENTED IN THE SIGNING OF THIS AGREEMENT AND IN THE MAKING OF THIS WAIVER BY INDEPENDENT LEGAL COUNSEL, OR HAS HAD THE OPPORTUNITY TO BE REPRESENTED BY INDEPENDENT LEGAL COUNSEL SELECTED OF ITS OWN FREE WILL, AND THAT IT HAS HAD THE OPPORTUNITY TO DISCUSS THIS WAIVER WITH COUNSEL.
</p>

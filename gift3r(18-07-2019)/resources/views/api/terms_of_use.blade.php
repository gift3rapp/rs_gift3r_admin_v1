<center><h1>Terms Of Use</h1></center>
<p>
BUYER TERMS OF USE AND CONDITIONS
PLEASE READ THIS NOTICE BEFORE YOU PURCHASE A VOUCHER:

The terms “Client,” “you,” and “your” refer to the person who is purchasing the Voucher (hereinafter defined in Section 1.14).

The terms “Company,” “we,” “us,” and “our” refer to GIVEMORE, LLC d/b/a GIFT3R.

Our website at www.GIFT3R.com, our application and its related services shall hereinafter collectively be referred to as the “App.” 

By agreeing to use our App, but prior to purchasing a Voucher, you must read the following Terms of Use and Conditions (the “Terms”) that are outlined below, as well as our Privacy Policy and SMS Terms and Conditions.  You shall be bound by all of the Terms in this “Agreement.”  The Agreement refers to both “GIVEMORE, LLC” and “GIFT3R” throughout.

Once you purchase a Voucher, “you,” the “Client,” agree and acknowledge that when an order is placed through the App, and the Merchant accepts it, you and the Merchant are legally bound by a contract.

The App is owned and operated by GIVEMORE, LLC and any of its subsidiaries or affiliates.  These Terms apply to your use of the App.  Any purchase or use of any products or services available through the App are governed by the posted Terms & Conditions of Service.  GIFT3R offers the App, including all information, tools and services available on the App to you, the user, which is conditioned upon your acceptance of these Terms.  Your continued use of the App constitutes your agreement to these Terms.  If you do not wish to be bound by these Terms, please do not use the App.

THEREFORE, you agree to the following terms and conditions of GIFT3R’s App and services:

SECTION 1. DEFINED TERMS 

As used in this Agreement, the GIFT3R App, these terms have the following definitions:

1.1 “Client Agreement” or “Agreement” has the meaning assigned in the preamble above. 

1.2 “Client” or “User” refers to you, the Voucher buyer, or the person receiving the Voucher from a third-party.

1.3 “CMS” means the content management system provided by GIFT3R to the Client to access, make a purchase and manage e-Gifts.

1.4 “Company,” “our,” “us,” or “we,” has the same meaning assigned in the preamble above.

1.5 “e-Gifts” refers to the products and services that are available for sale through our App from the Merchant to you.

1.6 “Intellectual Property Rights” or “IPR” refers to the laws of any country or jurisdiction in or outside of the United States that are confirmed by law, which includes statues that have been enacted or amended.  This includes equity, civil law, common law, or otherwise in connection to any discovery, dramatic work, literary work, musical work, invention, artistic work, trademark, service mark, copyright, database, design (includes 2D or 3D), confidential information, trade secret, semiconductor topography, know-how, material forms, or otherwise as it can be stored, recorded, or embodied in forms encompassing transient or electronic mediums, and as such it includes all applications of rights and all extensions for renewal of rights.

1.7 “Limitation of Liability” means the limits of our liability to you.

1.8 “Merchant” means the specified store, place or entity whose business sells goods and/or services in Voucher form to you.

1.9 “Policy” or “Policies” refers to any policy which may be available to the Merchant by GIFT3R, including, but not limited to, the content and style guides.  Company, in its sole and absolute discretion, will notify Merchants through the CMS system regarding its policies and changes.

1.10 “Service” or “service,” whether in the plural or not, means all services provided by us or otherwise made available by us to Merchant through the GIFT3R App.

1.11 “Sign-up” or “Register” are synonymous with the creation of a GIFT3R account on the App.

1.12 “Terms” means the terms and conditions in this Agreement, or any amended versions made by us and posted in the same manner as these.

1.13 “Voucher” refers to an electronic form redeemable for services and/or goods that is created and sold through the GIFT3R App and that a Client may buy from a Merchant.  This Voucher entitles the person receiving the Voucher to exchange it for goods and/or services that are worth the same amount outlined on the Voucher given by the Merchant.  This Voucher is subject to the Terms hereof.

SECTION 2. ELIGIBILITY

To use our Service, you must be a natural person of at least 18 years old.  At our sole and absolute discretion, we may require proof that you meet this condition concerning your use of our Services. Failure to comply with this Condition will result in the closing of your Account and the loss of all e-Gifts.

SECTION 3. ACCOUNT INFORMATION

3.1 Personal Information.  Currently GIFT3R does not gather any personal information from its Users.  GIFT3R does, however, reserve the right to change and update a User’s account information at any time and without any notice to the User.  GIFT3R reserves the right to change its Terms with regard to personal information without giving notice to the User in order to create an account using your email and your phone number.  You hereby give GIFT3R permission to do the above-mentioned changes and updates to your account information.

3.2 License.  To use our Services, you must have a mobile device that is compatible with the App.  GIFT3R does not warrant that the App will be compatible with your mobile device.  If you decide to use our services, it is subject to your agreement and compliance with these Terms, the Privacy Policy, and any and all applicable terms and conditions of e-Gifts or Vouchers.  GIFT3R hereby grants you a non-exclusive, non-transferable and revocable license to install and use an “object code copy” of the App for one (1) registered account on one (1) mobile device that is owned or leased solely by you.  All “virtual items,” text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, icons, images, audio clips, downloads, artwork, computer code and software (collectively, the “Content”)—including, but not limited to, the design, structure, selection, coordination, expression, “look and feel” and arrangement of such Content—contained on the App is owned, controlled or licensed by or to GIFT3R, and is protected by trademark, copyright and other applicable U.S. laws.

SECTION 3. ABOUT US

3.1 Services We Provide.  The GIFT3R App is a sales engine that allows Merchants to sell preset value Vouchers.  The Vouchers are made available to you through our portal.  GIFT3R acts as a liaison or intermediary between you and the Merchant.  GIFT3R is not a Merchant and we are not liable for the supply of any products and/or services that the Merchant may provide you.

3.2 How It Works and Your Responsibilities.  At the time that you Register or Sign-up on the App, you acknowledge and agree that all information provided by you is correct, accurate, and up-to-date.  You will have access to all Merchants that are using our Services when you Sign-up on the App.  You ensure, and it is your responsibility, that you enter the correct phone number of the person you wish to receive the Voucher.  It is extremely important that this is done in an error free manner in order to ensure that the proper recipient is receiving the Voucher.  If you are for any reason unsure of the phone number of the recipient, you may purchase the Voucher for yourself and then forward the Voucher to the appropriate party upon confirmation of their accurate contact information.  When making a purchase, your payment will be processed by GIVEMORE, LLC through our online payment getaways.

3.3 Forms of Payment.  We accept the following forms of payments: American Express, Visa, Mastercard, and debit cards. Your credit card or debit card statement will show the name GIVEMORE, LLC once an Voucher purchase is made.  If your purchase exceeds the total cost of goods, services, and items that are to be purchased using the Voucher, you agree to pay the rest of the amount in an acceptable form of payment to the proper Merchant.

3.4 Purchasing the Voucher.  After purchasing the Voucher, you should receive an e-mail receipt from us and then the Voucher should appear in the GIFT3R App under the App section labeled “My e-Gifts.”  GIFT3R Vouchers are at all times delivered in an electronic form through our CMS.  By reading these Terms, you acknowledge and accept that all Vouchers delivered via an electronic form cannot be printed out for redemption.  The only acceptable form to redeem a Voucher is electronically through your phone or other mobile device.

3.5 Balance.   GIFT3R Vouchers carry a balance and can be used multiple times until the balance runs out.  GIFT3R Vouchers never expire and do not carry any renewal fees or re-activation fees.

3.6 GIFT3R is not a Merchant.  You acknowledge and agree that GIFT3R is not a Merchant.

3.7 Service Fees.  You agree and accept GIFT3R’s Terms prior to making a purchase.  GIFT3R Vouchers are subject to a service fee when making a purchase and this service fee will appear on your final bill before you place an order.  Your purchase of any Voucher or other product in an amount less than Twenty–Five Dollars ($25.00) will incur a service fee of Forty-Nine Cents ($0.49), while the purchase of any Voucher or other product in an amount greater than Twenty–Five Dollars ($25.00), will incur a service fee of One Dollar ($1.00).

3.8 Redemption.  You agree that your Voucher is only redeemable for current purchases and cannot be used to satisfy any amounts owed to Merchant from a previous transaction with such Merchant.  You agree and acknowledge that when you purchase a Voucher from a Merchant, the use of the Voucher is solely for personal use and not for any business purpose.  You have the right to gift the Voucher to someone else of your choosing.  By purchasing a Voucher from a specific Merchant, you agree that you can only use the Voucher at the specific location of the Merchant, unless the Merchant has identified multiple locations that you may use.

3.9 Internet Security.  You acknowledge that transmitting information via the Internet is not completely secure, and for reasons outside of our control we cannot guarantee that information transmitted electronically will reach the intended destination on time.  You acknowledge that it is not technically possible for us to provide the service free from errors or faults at all times.  You acknowledge and agree that you are responsible for any lost, stolen, or mishandled delivery through the electronic form available of any Voucher.  We are not liable or responsible for any delivery failure or delay in regard to a Voucher that is not properly delivered. If subject to any of the above, you can access your Vouchers through the App’s electronic dashboard for redemption so long as they weren’t mishandled and used in the process. 

3.10  If you are not satisfy with the product you have purchase we want to hear it and we want to make it right. We will provide refunds if a consumer is unhappy with the products (on a case by case basis). Contact us at support@gift3rapp.com and let us know what happened.

3.11 Mistake in Purchase.  If a Voucher was purchased by mistake, you must contact the Merchant directly.  The Merchant reserves the right to make the final decision in any dispute.  Your Voucher cannot be redeemed if a Merchant from whom you purchased a Voucher is no longer in business. You acknowledge that GIFT3R is not responsible if such an event occurs and is not responsible for providing any refunds or redemption or any option to trade your Voucher.

3.12 Updating Terms and Conditions.  GIFT3R may from time to time amend, update or supplement these Terms.  The App will host any changes to this page or any other pages.  You understand that it is your responsibility to stay up-to-date on any changes provided herein.

SECTION 4. TERMINATION

GIFT3R, in its sole and absolute discretion, reserves the right to terminate your account, with or without cause, and with or without notice.  By using our App, you unequivocally agree to this provision.

SECTION 5. General Terms of Use, Usage and Sale

5.1 Registered User.  You represent and warrant that you are at least 18 years old, you are the Registered User of the App and you have authority to use the Service and to be bound by these Terms.

5.2 Compliance.  You represent and warrant that you have full right and authority to use our Services and to be bound by these Terms.  You agree that you will fully comply with all applicable laws, regulations, statutes, ordinances and the Terms herein.  You agree that all the information you provided us when you Registered on the App is accurate, and if any changes occur, you will notify us immediately or within a reasonable time.  You promise you shall not defraud or attempt to defraud, GIVEMORE, LLC or other users using the GIFT3R App.  You promise you shall not act in bad faith when using our Services.  If GIVEMORE, LLC determines that you acted in bad faith and in violation of any of these Terms, or if GIVEMORE, LLC determines that your actions fall outside of reasonable community standards, GIVEMORE, LLC may, at its sole and absolute discretion, terminate your Account and/or prohibit you from using our Services.  You agree that you shall not:

(a) Download the App, Register or access or use any part of the Service if you are under the age of 18 years old;

(b) Use the Service for any commercial purpose or for the benefit of any third-party in a manner not permitted by these Terms;

(c) Attempt to probe, scan or test the vulnerability of any GIVEMORE, LLC system or network or breach any security or authentication measures;

(d) Access, tamper with or use non-public areas of the Service, GIVEMORE, LLC computer systems, or the computer systems of our providers and partners;

(e) Avoid, bypass, remove, deactivate, impair, descramble or otherwise circumvent any technological measure implemented by GIVEMORE, LLC or any of our providers or any other third-party (including another user) to protect the Service or any part thereof;

(f) Attempt to use the Service on or through any platform or service that is not authorized by GIVEMORE, LLC;

(g) Post, upload, publish, submit, provide access to or transmit any User Content that:

(i) infringes, misappropriates or violates a third-party’s patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy;

(ii) violates or encourages any conduct that would violate any applicable law or regulation or would give rise to civil liability;

(iii) is fraudulent, false, misleading or deceptive;

(iv) is defamatory, obscene, pornographic, vulgar or offensive;

(v) promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group;

(vi) is violent or threatening or promotes violence or actions that are threatening to any other person; or

(vii) promotes illegal or harmful activities or substances.

(h) Upload or transmit (or attempt to upload or transmit) files that contain viruses, Trojan horses, worms, time bombs, cancellous, corrupted files or data, or any other similar software or programs that may damage the operation of the Service or the computers of other users of the Service;

(i) Develop, distribute, use, or publicly inform other members of cheats, automation software, bots, hacks, mods or any other unauthorized third-party software or applications;

(j) Exploit, distribute or publicly inform other users of the Service of any game error or bug which gives users an unintended advantage;

(k) Use “virtual items” in a manner that violates these Terms, including transferring or selling virtual items or fraudulently obtaining or acquiring virtual or other products or services;

(l) Attempt to interfere with, intercept or decipher any transmissions to or from the servers for the Service;

(m) Interfere with, or attempt to interfere with the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, or mail-bombing the Service;

(n) Encourage or enable any other individual or group to do any of the foregoing; or

(o) Violate any applicable law or regulation.

SECTION 6. INTELLECTUAL PROPERTY OWNERSHIP

6.1 Intellectual Property Rights.  The Service and all content thereon are protected by copyright, trademark, and other laws of the United States and foreign countries.  Except as expressly provided in these Terms, GIVEMORE, LLC and its licensors exclusively own all right, title and interest in the Service and all content thereon or therein, including all associated intellectual property rights.  You will not remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Service.  You agree that you shall not:

(a) Modify, reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for any underlying software or other intellectual property used to provide the Service without GIVEMORE, LLC’s express and prior written permission;

(b) Use, display, mirror or frame the Service, or any individual element within the Service;

(c) Use the intellectual property of GIVEMORE, LLC, or any GIVEMORE, LLC licensor, to adapt, modify or create derivative works based on such intellectual property;

(d) Rent, lease, loan, trade, sell or re-sell access to the Service or any information therein, in whole or part; or

(e) Use or reproduce any GIVEMORE, LLC licensor, or third-party trademark or logo without the prior express written consent of the owner of such trademark or logo.

6.2 Claims of Intellectual Property Infringement; Termination of Accounts.  GIVEMORE, LLC respects the intellectual property of others, and we ask that our Users do the same.  You are hereby informed that GIVEMORE, LLC has adopted and reasonably implemented a policy that provides for the termination of User accounts, in appropriate circumstances, of GIFT3R App Users or GIVEMORE, LLC members who are repeat copyright infringers.  GIVEMORE, LLC may, in appropriate circumstances, and at its sole and absolute discretion, disable and terminate the accounts and memberships of Users who may be infringing the intellectual property of a third-party.  If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide GIVEMORE, LLC’s Copyright Agent the following information (to be effective, the notification must be in writing and provided to our Copyright Agent):

(a) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;

(b) a description of the copyrighted work or other intellectual property that you claim has been infringed, or if multiple copyrighted works are covered by a single notification, a representative list of such works at that App;

(c) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and a description of where the material that you claim is infringing is located on the App;

(d) your address, your telephone number, and your e-mail address;

(e) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent or the law; and

(f) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.

SECTION 7. LIMITATION OF LIABILITY

7.1 Limitation of Liability.  The Merchants whose products and services are available on the GIFT3R App are independent contractors and not agents or employees of GIVEMORE, LLC.  GIVEMORE, LLC is not liable for the acts, errors, omissions, representations, warranties, breaches or negligence of any such Merchants or for any personal injuries, death, property damage, or other damages arising out of such acts or omissions.  Such damages shall also include, but not be limited to, any: loss of goodwill, loss of contracts, loss of business, loss of reputation, loss of revenue, loss of actual or anticipated profits, loss of opportunity, loss of anticipated savings, loss of interest or consequential or special loss (foreseeable, known, or otherwise), loss of the use of money, loss of damage, corruption, data use or loss of expenses resulting therefrom.  Our total liability to you, or any other party, shall in no circumstances exceed, in aggregate, a sum of more than Thirty-Five Dollars ($35.00).

You acknowledge and agree that you assume full responsibility for the following:

(a) your use of the App for any GIVEMORE, LLC communications with third-parties,

(b) your purchase and use of the products and services that are available through the App;

(c) any information you send or receive during your use of the App may not be secure, and may be intercepted by unauthorized parties;

(d) your use of our App is at your own risk;

(e) besides the applicable service fee, the App is made available to you at no charge, and as such, you acknowledge and agree that, to the fullest extent permitted by applicable law (including, without limitation, consumer protection law), neither GIVEMORE, LLC nor its licensors or providers will be liable for any direct, indirect, punitive, exemplary, incidental, special, consequential or other damages arising out of or in any way related to:

(1) the App, or any other App or resource you access through a link from the App;

(2) any action we take or fail to take as a result of communications you send to us;

(3) any products or services made available or purchased through the App, including any damages or injury arising from any use of such products or services;

(4) any delay or inability to use the App or any information, products or services advertised in or obtained through the App;

(5) the modification, removal or deletion of any content submitted or posted on the App; or

(6) any use of the App, whether based on contract, tort, strict liability or otherwise, even if GIVEMORE, LLC has been advised of the possibility of damages.

(g) it is your responsibility to evaluate the accuracy, completeness or usefulness of any opinion, advice or other content available through the App, or obtained from a linked app or resource.  This disclaimer applies, without any limitations, to any damages or injury arising from any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, file corruption, communication-line failure, network or system outage, loss of profits by you, or theft, destruction, unauthorized access to, alteration of, loss or use of any record or data, and any other tangible or intangible loss. 

(h) neither GIVEMORE, LLC nor its licensors, suppliers or third-party content providers shall be liable for any defamatory, offensive or illegal conduct of any user of the App.

(i) your sole remedy for any of the above claims or any dispute with GIVEMORE, LLC is to discontinue your use of the App.

(j) you and GIVEMORE, LLC agree that any cause of action arising out of or related to the App must commence within one (1) year after the cause of action accrues or the cause of action is permanently barred.  Because some jurisdictions do not allow limitations on how long an implied warranty lasts, or the exclusion or limitation of liability for consequential or incidental damages, all or a portion of the above limitation may or may not apply to you.

SECTION 8. LEGAL NOTICES

8.2 Address.  Any written correspondences under this Agreement shall be sent by certified mailed, return receipt requested, to GIVEMORE, LLC at the address stated below:

5881 E. Thomas Road Scottsdale, Arizona 85251

8.2 How to Contact Us.  If you have any questions or comments about these or the App, please contact us by e-mail at info@gift3r.com.  GIVEMORE, LLC may update this contact information at any time and without notice to you.  You can check our App for the most up to date contact information.

8.3 Notices.  Notices made under this Agreement will be in writing and sent to the persons and addresses set out on the App or in the Registration account.  They may be given and will be deemed received by: (1) first-class post: two Business Days after posting; (2) airmail: seven Business Days after posting; (3) hand delivery; facsimile: on receipt of a successful transmission report from the correct number; and e-mail: on receipt of a delivery or read receipt mail from the correct address.

SECTION 9. MISCELLANEOUS PROVISIONS

9.1 Promotional Offers.  From time to time, GIVEMORE, LLC may offer special promotional offers that may or may not apply to your GIVEMORE, LLC account.  You agree to be bound by any additional terms and conditions for these special offers.

9.2 Severability.  The invalidity or unenforceability of any one or more provisions of this Agreement shall in no way affect any other provision.  If any court of competent jurisdiction determines any provision of this Agreement to be invalid, illegal or unenforceable, that portion shall be deemed severed from the rest, which shall remain in full force and effect as though the invalid, illegal or unenforceable portion had never been a part of the Agreement.

9.3 Headings.  Section headings are for reference purposes only and do not limit the scope or extent of such section. 

9.4 Entire Agreement.  This Agreement constitutes the entire Agreement between the parties in relation to its subject matter.

9.5 Rights of Third-Parties.  This Agreement is made and entered into for the sole protection and benefit of GIVEMORE, LLC and Client.  No other persons or entities have any right of action under this Agreement.

9.6 Waiver.  The failure of GIVEMORE, LLC to act with respect to a breach of these Terms by you or others does not waive GIVEMORE, LLC’s right to act with respect to subsequent or similar breaches. 

9.7 Succession.  The terms of this Agreement shall bind and benefit the heirs, legal representatives, successors, and assigns of the parties; provided, however, that Client may not assign this Agreement or assign or delegate any of its rights or obligations without GIVEMORE, LLC’s prior written permission. 

9.8 Arbitration and Dispute Resolution.  If a dispute arises under these Terms between you and GIVEMORE, LLC, such dispute shall be resolved, at the filing party’s election, in either a small claims court, or if the amount in dispute exceeds the jurisdictional limit of the small claims court, by final and binding arbitration administered by the National Arbitration Forum or the American Arbitration Association, under their rules for consumer arbitrations.  The venue for all disputes arising under these Terms shall be the City of Phoenix in the State of Arizona.  All disputes in arbitration will be handled solely between the named parties, and not on any representative or class basis.  ACCORDINGLY, YOU ACKNOWLEDGE THAT YOU MAY NOT HAVE ACCESS TO A COURT (OTHER THAN A SMALL CLAIMS COURT) OR TO A JURY TRIAL.  Notwithstanding any other provision of these Terms, GIVEMORE, LLC may resort to court action for injunctive relief at any time in its sole and absolute discretion.  You are responsible for informing yourself of the laws of your jurisdiction and complying with them.

9.9 Governing Law.  This Agreement shall be governed by, and construed in accordance with, the laws of the State of Arizona, without regard to the choice of law rules of that state, except to the extent that any of such laws may now or hereafter be preempted by Federal law.  Client consents to the jurisdiction of any Federal or State court within the State of Arizona, submits to venue in such state, and also consents to service of process by any means authorized by Federal law or the law of such state.  Without limiting the generality of the foregoing, Client hereby waives and agrees not to assert by way of motion, defense, or otherwise in such suit, action, or proceeding, any claim that (i) Client is not subject to the jurisdiction of the courts of the above-referenced state or the United States District Court for such state, or (ii) such suit, action, or proceeding is brought in an inconvenient forum, or (iii) the venue of such suit, action, or proceeding is improper.

9.10 Waiver of Jury Trial.  TO THE EXTENT NOT PROHIBITED BY APPLICABLE LAW, CLIENT HEREBY KNOWINGLY, VOLUNTARILY, AND INTENTIONALLY WAIVES ANY RIGHT TO TRIAL BY JURY OR OF ANY CLAIM, DEMAND, ACTION, OR CAUSE OF ACTION BASED UPON OR ARISING UNDER THIS AGREEMENT OR IN ANY WAY CONNECTED WITH OR RELATED OR INCIDENTAL TO THE DISCUSSIONS, DEALINGS, OR ACTIONS OF THE PARTIES TO THIS AGREEMENT OR EITHER OF THEM (WHETHER ORAL OR WRITTEN) WITH RESPECT THERETO, OR TO THE TRANSACTIONS RELATED THERETO, IN EACH CASE WHETHER NOW EXISTING OR HEREINAFTER ARISING, AT LAW OR IN EQUITY, INCLUDING WITHOUT LIMITATION, CONTRACT CLAIMS, TORT CLAIMS, BREACH OF DUTY CLAIMS, AND ALL OTHER COMMON LAW OR STATUTORY CLAIMS.  CLIENT HEREBY CONSENTS AND AGREES THAT ANY SUCH CLAIM, DEMAND, ACTION, OR CAUSE OF ACTION SHALL BE DECIDED BY A TRIAL COURT WITHOUT A JURY, AND THAT EITHER PARTY TO THIS AGREEMENT MAY FILE AN ORIGINAL COUNTERPART OR A COPY HEREOF WITH ANY COURT AS WRITTEN EVIDENCE OF THE CONSENT OF THE CLIENT TO THE WAIVER OF ITS RIGHT TO TRIAL BY JURY.  CLIENT ACKNOWLEDGES AND AGREES THAT IT HAS RECEIVED FULL AND SUFFICIENT CONSIDERATION FOR THIS PROVISION (AND EACH OTHER PROVISION OF THIS AGREEMENT AND EACH OTHER DOCUMENT TO WHICH IT IS A PARTY) AND THAT THIS PROVISION IS A MATERIAL INDUCEMENT FOR LENDER IN MAKING THE LOAN.  CLIENT FURTHER REPRESENTS AND WARRANTS THAT IT HAS BEEN REPRESENTED IN THE SIGNING OF THIS AGREEMENT AND IN THE MAKING OF THIS WAIVER BY INDEPENDENT LEGAL COUNSEL, OR HAS HAD THE OPPORTUNITY TO BE REPRESENTED BY INDEPENDENT LEGAL COUNSEL SELECTED OF ITS OWN FREE WILL, AND THAT IT HAS HAD THE OPPORTUNITY TO DISCUSS THIS WAIVER WITH COUNSEL.
</p>
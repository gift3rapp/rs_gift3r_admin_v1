<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>gift3r</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
</head>
<body yahoo="fix" style="margin: 0; padding: 180px 10px; background-color: #ebebeb; font-family: 'Open Sans', sans-serif; -webkit-text-size-adjust: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="max-width: 600px; background-color:#ffffff;border:1px solid #dedede;border-radius:3px">
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding:10px 30px;text-align:center" bgcolor="#FE0103">
                        <img style="width:60px;display:block;margin:0 auto;" src="http://18.221.106.68/gift3r/public/assets/gift3rlogo.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding:48px 48px 0">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <!-- <p style="font-size:14px">You’ve received the following order from test test:</p> -->
                                    <div style="margin-bottom:40px">
                                        <table border="1" cellpadding="6" cellspacing="0" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;width:100%;">
                                            <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Store Name</th>
                                                     <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left;word-wrap:break-word"><?php echo $store_name; ?></td>
                                                    
                                                </tr>
                                            
                                            
                                                <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Full Name:</th>
                                                    <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><?php echo ucfirst($user_name); ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Redeem Amount:</th>
                                                    <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><span class="amount">$</span><?php echo $amount; ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Balance Amount:</th>
                                                    <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><span class="amount">$</span><?php echo $balance_amount; ?></td>
                                                </tr>
                                                 <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Store Name:</th>
                                                    <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><span class="amount"><?php echo ucfirst(@$store_name); ?></span></td>
                                                </tr>
                                                 <tr>
                                                    <th scope="row" colspan="2" style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">Gift Card Name:</th>
                                                    <td style="color:#333333;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left"><span class="amount"><?php echo ucfirst(@$gift_name); ?></span></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="2" valign="middle" style="padding:0 38px 38px 38px;border:0;color:#FE0103;font-size:14px;line-height:125%;text-align:center">
                                <!-- <p><span>gift3r</span></p> -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
</body>
</html>
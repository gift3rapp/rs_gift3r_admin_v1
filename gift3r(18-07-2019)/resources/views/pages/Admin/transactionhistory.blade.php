<style>

.table-responsive table.table.table-striped tr td {
    display: table-cell;
    font-size: 14px;
    line-height: 29px;   
}

.table-responsive table#printTable tr th {
    font-size: 12px;
    padding: 7px 23px 7px 0;
}
</style>
<style media="screen">
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Transaction History</h2>
		<ol class="breadcrumb">
		<li>
				<a href="{{url('admin/dashboard')}}">home</a>
			</li>
			
			<li class="">
			 <a href="admin/pages/Admin/users/"> users</a>     
			</li>
			<li>
			<a href="admin/pages/Admin/users/">  user cards   </a>
			</li>

	
			<li class="active">
			 <strong>Transacation History</strong>    
			</li>
		

		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Transaction History</h5>
						 <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'TransactionHistory', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
  width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>

					</div>
						<div class="ibox-title">
						
             <form action="admin/pages/Admin/transaction_history/{{$user_id}}/{{$id}}" method="get">
   
     
                    <div class="col-sm-3">
                 <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control datepicker" name="start_date" data-date-format="yyyy-mm-dd" value="{{$start_date}}">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
                 </div>
                 
                       <div class="col-sm-3">
                 <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control datepicker" name="end_date" data-date-format="yyyy-mm-dd" value="{{$end_date}}">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
</div>
                 <button type="submit" class="btn btn-sm btn-primary" style="background-color:#F26101 ;border-color:#F26101 ;"> Go</button> </span>
         </form>                 

					</div>
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
										        
											<th>Transacton Id</th>
											<th>Receipt Number</th>
											<th> Balance (In USD) </th>
											<th>Date </th>
											<th>Status</th>
											<th>Sent By/To</th>
											
										</tr>
									</thead>
									<tbody>
									@foreach($history as $hist)
										<tr>      
										                
											<td>{{$hist->t_id}}</td>
					<td>@if($hist->receipt_num){{$hist->receipt_num}}@else{{'-'}}@endif</td>
							<td>
			<?php  $bal = str_replace('-','',$hist->transaction_balance); echo round($bal,2);?>
							</td>
										<td>{{Carbon\Carbon::parse($hist->created_at)->format('d M, Y ')}}</td>
						
									       <td >
									       @if($hist->status==0)
		
		<label class="badge badge-success" style="cursor:pointer;">Purchased </label>
		
		@elseif($hist->status==1 || $hist->status==5)
		<?php if($hist->sender_id==$user_id && $hist->r_name){ ?>						       
		<label class="badge badge-primary" style="cursor:pointer;">Sent </label>
		<label class="badge badge-primary" style="cursor:pointer;">Delivered </label>
		<?php } elseif($hist->sender_id==$user_id && !$hist->r_name){ ?>
		<label class="badge badge-primary" style="cursor:pointer;">Sent </label>
		<label class="badge badge-primary" style="cursor:pointer;">Pending </label>		
		<?php } 
		elseif($hist->receiver_id==$user_id && !$hist->r_name){ ?>
		<label class="badge badge-primary" style="cursor:pointer;">Pending </label>
		<?php } elseif($hist->receiver_id==$user_id && $hist->r_name){ ?>
	        <label class="badge badge-primary" style="cursor:pointer;">Received </label>
		<?php } ?>

									       @elseif($hist->status==2)
									       <label class="badge badge-danger ">Redeemed </label>
									       </td>
									       @endif	
		<td>@if($hist->status==1) 
		<?php if($hist->sender_id==$user_id){ ?>
		@if($hist->r_name) {{$hist->r_name}} @else {{'-'}} @endif
		<?php }
		elseif($hist->receiver_id==$user_id){ ?>
		@if($hist->s_name) {{$hist->s_name}} @else {{'-'}} @endif
		<?php } ?>
		
		@else
		{{'-'}}
		@endif		
		</td>							       
										</tr>
									@endforeach
									


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
							<ul class="pagination pull-right">{{$history->appends(Request::except('page'))->links()}}</ul>
											</td>

										</tr>
									</tfoot>


								</table>
							   @if(!count($history))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
								</div>
							@endif
							
							</div>
							


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

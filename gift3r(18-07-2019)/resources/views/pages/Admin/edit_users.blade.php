@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Edit User</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">home</a>
                        </li>
                        <li>
                            <a href="{{url('admin/pages/Admin/users')}}">users</a>
                        </li>
                      
                       <li class="active">
                            edit user
                        </li> 
                      
                       
                       
                       
                    </ol>


                </div>




            </div>
           @if (Session::get('success'))
    <div class="alert alert-success alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;
">
    <a href="javscript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
           {{Session::get('success')}}
        </ul>
    </div>
    @endif
    @if(count($errors))
     <div class="alert alert-danger alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;
">
    <a href="javscript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  
                    
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>


   
@endif

 
            <div class="wrapper wrapper-content animated fadeInRight">
                   <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit user</h5>
                        </div>
                        <div class="ibox-content">
                  
                 

                       <form class="form-horizontal" action='admin/pages/Admin/edit_users/{{$id}}' method="post">
                          {{csrf_field()}}
                            

                          <input type="hidden" name="playlist_id" value="">
                                 
                               
                                <div class="form-group"><label class="col-lg-2 control-label">name</label>

                   <div class="col-lg-6"><input type="text" name="name" placeholder=" name" class="form-control" required="" value="{{$user->name}}"> 
                                    </div>
                                </div>
                                
                              
                            
                                <div class="form-group"><label class="col-lg-2 control-label">email </label>

                  <div class="col-lg-6"><input type="email" name="email" placeholder="email" class="form-control" required="" value="{{$user->email}}"> 
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-lg-2 control-label">phone number</label>

                  <div class="col-lg-6"><input type="text" name="number" placeholder="phone number" class="form-control" required=""
                   value="{{$user->phone}}"> 
                                </div>
                                </div>
                               
                                                                                                                                                                                                
                                

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">Update</button>
                                    </div>
                                </div>
                            </form>
                           
                        </div>
                    </div>
                </div>
          </div>  





@endsection
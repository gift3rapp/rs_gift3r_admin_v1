@extends('layout/main') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Admin</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="">
                      <a href="{{url('admin/pages/Admin/users')}}">  <strong>users</strong></a>     
                        </li>
                      <li class="active">
             
                <a href="admin/pages/Admin/users_desc/{{$id}}"> 
                <strong>userDescription</strong>
                </a>  
               
                        </li>
                    </ol>
                </div>
            </div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
						<h5 style="color: #EF4036;">Transaction History</h5>
				</div>
					<div class="ibox-content">
						<div class="row">
								<div class="table-responsive">
										<table class="table table-striped" id="printTable">
											<thead>
												<tr>
													<th>TransactonId</th>
													<th>sender name </th>
													<th>Card name </th>
													<th>Transaction Balance </th>
													<th>Receiver Name</th>
													<th>Receipt Number</th>
													<th>Total bill</th>
													<th>Date </th>
													<th>Status</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($descs as $hist)
													<tr>                       
														<td>{{$hist->t_id}}</td>
														<td>{{$hist->s_name}}</td>
														<td>{{$hist->g_name}}</td>
														<td>{{$hist->transaction_balance}}</td>
														@if(!$hist->receiver_id)
															<td> -- </td>
															<td> --- </td>
															<td> --- </td>
														@else
															<td> {{$hist->r_name}} </td>
															<td> {{$hist->receipt_num}} </td>
															<td>{{$hist->bill_amount}} </td>
														@endif

														<td> {{Carbon\Carbon::parse($hist->created_at)->format(' Y-m-d ')}}</td>
											
														@if($hist->status==0)
															<td ><label class="badge badge-success ">Buy Cards </label></td>
														@elseif($hist->status==1)
															<td ><label class="badge badge-primary ">Gifted Cards </label></td>
														@else
															<td><label class="badge badge-danger ">Redeemed Cards </label></td>
														@endif
													</tr>
												@endforeach
									
											</tbody>
													<tfoot >
														<tr>
															<td colspan="12">
																<ul class="pagination pull-right"></ul>
															</td>

														</tr>
													</tfoot>

										</table>
							  			@if(!count($descs))
											<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
											</div>
										@endif
								</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
 @endsection

<style>
  .table-responsive table.table.table-striped tr td{
      width: 28%;
display: table-cell;
padding-left: 9px;
padding-right: 9px;
  }
  .table-responsive table.table.table-striped tr td:last-child {
    text-align: right;
}

</style>
<style media="screen">
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>
@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Admin</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="active">
                      <a href="{{url('admin/pages/Admin/merchants')}}"> <strong> <strong>merchants</strong></strong></a>     
                        </li>
                    </ol>
                </div>
            </div>

  
<div class="wrapper wrapper-content  animated fadeInRight">
<div class="row">
<div class="add col-sm-12 " style="margin-top: -5px;">  
 <!-- <form action="add_merchants" method="post">
   <a href="admin/pages/Admin/add_merchants"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Merchants Saled</button></a> </form> -->
                      
                 
                             </div> 
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 style="color: #EF4036;">merchants </h5>
                            <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'MerchantSaled', separator:';', escape:'false' ,ignoreColumn:'[3,4]'}); $('.noPrint').show();" style="
  width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>
                          
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                 
                            <div class="table-responsive">
                                <table class="table table-striped" id="printTable">
                                    <thead>
                                    <tr>
                                       <th> Name</th>
                                       <th> Image </th>
                                       <th>Total sales</th>
                                       <th></th>
                                       
                                        
                                  
                                    </tr>
                                    </thead>
                                    <tbody>
                                 @foreach($merchants as $merch)  
                                    <tr>
                                        
                                       <td>{{$merch->name}}</td>
                                       


                                      <td class="tg" > 
                          @if($merch->image=="http://localhost/GIFT3R/public/thumb/uploads/default.png")
  <img src="{{url('assets/uploads/0_default.png')}}" style="width: 50px; height:50px;" class="img-circle"></td>
                      @else
<img src="{{$merch->image}}" style="width: 50px; height:50px;" class="img-circle">
                     @endif
                        


                              
                           <td><span class="label label-danger" style="background: #ed5464; color: white; border-radius: 30px;"
                           >
                          {{$counts}}</span></td> 
                                

                                   
                                 <td > 
                                <label class="btn btn-danger btn-sm view"  name="count" style="background-color:#EF4036 !important"> <a href="admin/pages/Admin/transaction_history/{{$merch->id}}" style="color: white" ><i class="fa fa-folder" aria-hidden="true" style="padding: 3px;"> <b>View</b></i></a></label>    
                              </td>  
                                      

                                    
                                     
                                    </tr>
                                  
                                  
                                   @endforeach

									
                               
                                       </tbody>
                                      <tfoot >
                                <tr>
                                    <td colspan="12">
                                        <ul class="pagination pull-right ">{{$merchants->links()}}</ul>
                                    </td>
                                   
                                </tr>
                                </tfoot>
                                   
                               
                                </table>
                                 @if(!count($merchants))
							<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

                            </div>
           

                        </div>
                    </div>
                </div>

            </div>
   </div>
   </div>
  
<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
  
    $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[5,.noPrint]'});
   $('.noPrint').show();
  });
});
</script>











@endsection
<style media="screen">
.table-responsive table.table.table-striped tr td {
    display: table-cell;
    font-size: 14px;
    line-height: 29px;   
}

.table-responsive table#printTable tr th {
    font-size: 12px;
    padding: 7px 23px 7px 0;
}
a[href]:after {
    content: none !important;
  }
.noPrint {
  display: block !important;
}

.yesPrint {
  display: block !important;
}
</style>

<style media="print">
.noPrint {
  display: none !important;
}

.yesPrint {
  display: block !important;
}
</style>

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Buy Cards</h2>
		<ol class="breadcrumb">
      <li>
            <a href="{{url('admin/dashboard')}}">Home</a>
         </li>
                        <li>
                      <a href="{{url('admin/pages/Admin/merchants')}}"> merchants</a>     
                        </li>        
         <li>
            <a href="admin/pages/Admin/merchants_desc/{{$id}}">Merchant Description</a>
         </li>
         <li>
  <a href="admin/pages/Admin/mer-all-cards/{{$id}}">  Merchant Cards    </a> 
         </li>
			<li>
		  <a href="admin/pages/Admin/mercards/{{$id}}"> 	 Card Details   </a>
			</li>
			<li class="active">
			 <strong>Buy Card History</strong>   
			</li>
		

		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Transaction History</h5>
						
						
						  <button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'BuyCardsHistory', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
  width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>
                          

					</div>

			<div class="ibox-title">
						
             <form action="admin/pages/Admin/buycards/{{$id}}" method="get">
          <div class="col-sm-2">
           <div class="search input-group">
             <input type="text" name="search" placeholder="Search" class="input-sm form-control" value="{{$search}}"> <span class="input-group-btn">
             </div>
           </div>    

                 <div class="col-sm-3">
                 <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control datepicker" name="start_date" data-date-format="yyyy-mm-dd" value="{{$start_date}}">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
                 </div>
                 
                       <div class="col-sm-3">
                 <div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control datepicker" name="end_date" data-date-format="yyyy-mm-dd" value="{{$end_date}}">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
                 </div>
                 <button type="submit" class="btn btn-sm btn-primary" style="background-color:#F26101 ;border-color:#F26101 ;"> Go</button> </span>
         </form>                 

					</div>

					
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th>TransactonId</th>
											<th>User name </th>
										    <th>Transaction Balance (In USD) </th>
											
											
											<th>Date </th>
											
											<th>View Detail</th>
										</tr>
									</thead>
									<tbody>
								<?php foreach($history as $hist)
								{
									$value=$hist->mypurchasedcard_id;
											$mypurchaseddetail=DB::table('mypurchased_cards')->leftjoin('payments','mypurchased_cards.payment_id','payments.id')->where('mypurchased_cards.id',$value)->select('payments.order_id')->first();

								?>
										<tr>                       
											<td style="display:none;" class="mypurchasedcard_id" >{{!empty($value)?$value:""}}</td>                      
											<td>{{!empty($mypurchaseddetail->order_id)?$mypurchaseddetail->order_id:""}}</td>
											<td>{{$hist->s_name}}</td>
											
											<td>{{$hist->transaction_balance}}</td>
											

											<td> {{Carbon\Carbon::parse($hist->created_at)->format(' d M, Y ')}}</td>
											<td><button type="button" class="btn btn-info btn-md mdlgiftcarddetail" data-toggle="modal"  data-target="#myModal">Detail</button></td>
												
										</tr>
									<?php } ?>
									


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right">
									{{ $history->appends(Request::except('page'))->links() }}
												</ul>
											</td>

										</tr>
									</tfoot>


								</table>
							   @if(!count($history))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
								</div>
							@endif
							
							</div>
							


						</div>


								<!-- Modal -->
    <div id="myModal" class="modal fade " role="dialog">
  <div class="modal-dialog"  style="width:85%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Gift Card Details</h4>
      </div>
      <div class="modal-body" style="height:450px; 
    overflow-y: auto;width:100%;">
    <table id="tblpurchasedcarddetail" class="table table-bordered table-hover">
    	<thead>
    		<tr>
    			<th>Sender/Buyer</th>
    			<th>Sender Contact</th>
    			<th>Receiver</th>
    			<th>Receiver Contact</th> 		
    			<th>Amount</th>
    			<th>Action</th>
    			<th>Name of Server</th>
    			<th> Date</th> 			
    		</tr>

    	</thead>
    	<tbody></tbody>

    </table>
       	
       
	  	 	
       
    
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

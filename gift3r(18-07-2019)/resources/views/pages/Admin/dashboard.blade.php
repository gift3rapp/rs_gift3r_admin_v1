@extends('layout/main') @section('content')
            
 
            <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-4">
                    <a href="{{url('admin/pages/Admin/users')}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Users</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;"> {{$u_count}} </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                    <a href="{{url('admin/pages/Admin/merchants')}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Merchants</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;">  {{$m_count}} </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>

                     <!-- <div class="col-lg-4">
                    <a href="{{url('admin/pages/Admin/merchant_saled')}}">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Sales</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;">  {{$sale}} </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div> -->
                
                   
        </div>
       


                </div>
     
   
    


@endsection
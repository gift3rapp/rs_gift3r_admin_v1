<style media="screen">
	a[href]:after {
		content: none !important;
	}
	.noPrint {
		display: block !important;
	}

	.yesPrint {
		display: block !important;
	}
</style>

<style media="print">
	.noPrint {
		display: none !important;
	}

	.yesPrint {
		display: block !important;
	}
</style>
@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Admin</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('admin/dashboard')}}">home</a>
			</li>
			@if(\Request::url('admin/pages/Admin/users'))
				<li><a href="admin/pages/Admin/users/">Users</a></li>
				
				@elseif(\Request::url('admin/pages/Admin/merchants'))	
					<li><a href="admin/pages/Admin/merchants/"> merchants</a></li>
			@endif 
			
			@if(\Request::url('admin/pages/Admin/users_cards/{{$id}}'))
				<li><a href="admin/pages/Admin/users_cards/{{$id}}"> User cards</a> </li>   		
				@elseif(\Request::url('admin/pages/Admin/merchants_desc/{{$desc->id}}'))
				<li><a href="admin/pages/Admin/merchants_desc/{{$desc->id}}"> merchant_description</a></li>	
			@endif 
			
			<li class="active">
				<a href="admin/pages/Admin/sendcards/{{$id}}"> <strong>Send Card History</strong></a>     
			</li>


		</ol>
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


		</div> 
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5 style="color: #EF4036;">Transaction History</h5>
					<button class="btn btn-export btn-success pull-right " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'SendCardsHistory', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
					width: 68px;
					height: 30px;
					padding-top: 4px;
					padding-left:  5px;
					margin-right:20px;
					padding-right:66px;float: right;margin-top: -6px;
					"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-left:2px;"></i> Export</button>

				</div>
				<div class="ibox-content">
					<div class="row">

						<div class="table-responsive">
							<table class="table table-striped" id="printTable">
								<thead>
									<tr>
										<th>TransactonId</th>
										<th>sender name </th>
										<th>Card name </th>
										<th>Transaction Balance(In USD) </th>
										<th>Receiver Name</th>

										<th>Date </th>

										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($history as $hist)
									<tr>                       
										<td>{{$hist->t_id}}</td>
										<td>{{$hist->s_name}}</td>
										<td>{{$hist->g_name}}</td>
										<td>{{$hist->transaction_balance}}</td>
										<td>{{$hist->r_name}}</td>


										<td> {{Carbon\Carbon::parse($hist->created_at)->format(' d M, Y ')}}</td>


									</tr>
									@endforeach
									


								</tbody>
								<tfoot >
									<tr>
										<td colspan="12">
											<ul class="pagination pull-right"></ul>
										</td>

									</tr>
								</tfoot>


							</table>
							@if(!count($history))
							<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
							</div>
							@endif
							
						</div>



					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

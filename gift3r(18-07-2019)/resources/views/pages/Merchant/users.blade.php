<style>
  .table-responsive table.table.table-striped tr td{
        width: 7%;
    display: table-cell;
        padding-left: 9px;
    padding-right: 11px;
  }
  .table-responsive table.table.table-striped tr td:last-child {
    text-align: right;
}

</style>
@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Admin</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">Home</a>
                        </li>
                        <li class="active">
                      <a href="{{url('merchant/pages/Merchant/users')}}"> <strong> <strong>Users</strong></strong></a>     
                        </li>
                    </ol>
                </div>
            </div>

  
<div class="wrapper wrapper-content  animated fadeInRight">
<div class="row">
<div class="add col-sm-12 " style="margin-top: -5px;">  
 <!-- <form action="add_users" method="post">
   <a href="admin/pages/Admin/add_users"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Users</button></a> </form> -->
                      
                 
                             </div> 
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 style="color: #EF4036;">users </h5>
                          
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                 <form action="{{url('users')}}" method="get" role="search">
                            </form>
                            <div class="table-responsive">
                                <table class="table table-striped" id="printTable">
                                    <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Image </th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                       <td>{{$user->name}}</td>
                                       <td> <img src="{{$user->image}}" style="width: 50px; height:50px;" class="img-circle"></td>

                                     
                                        <td>{{$user->email}}</td>
                                       <td style="text-align: left;">{{$user->phone}}</td>
                                     <td style="text-align: left;">
                                <label class="btn btn-warning btn-xs view"  name="count"> <a href="merchant/pages/Merchant/users_desc/{{$user->id}}" style="color: #23c6c8;" ><i class="fa fa-folder" aria-hidden="true" style="padding: 3px; color:white;"> <b>View</b></i></a></label>    
                              </td> 
                                     
                                    </tr>
                                    @endforeach
                                  
                               
                                       </tbody>
                                       <tfoot>
                                <tr>
                                    <td colspan="12">
                                        <ul class="pagination pull-right ">
                                       <!--  {{$users->links()}} -->
                                        </ul>
                                    </td>
                                </tr>
                                </tfoot>
                                   
                               
                                </table>
                                 @if(!count($users))
							<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

                            </div>
             

                        </div>
                    </div>
                </div>

            </div>
   </div>
   </div>
  












@endsection
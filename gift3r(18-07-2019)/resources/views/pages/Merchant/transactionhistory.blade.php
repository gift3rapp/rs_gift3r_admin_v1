@extends('layout/main') @section('content')
<style media="screen">
a[href]:after {
		content: none !important;
	}
.noPrint {
	display: block !important;
}

.yesPrint {
	display: block !important;
}
</style>

<style media="print">
.noPrint {
	display: none !important;
}

.yesPrint {
	display: block !important;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Admin</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('merchant/dashboard')}}">home</a>
			</li>
			<li class="">
				<a href="merchant/gift_card">  gift card</a>     
			</li>
			<!-- <li class="">
				<a href="merchant/pages/Merchant/purchasecards/{{$id}}"> Purchase Cards</a>     
			</li> -->
			<li class="active">
				<a href="merchant/pages/Merchant/transactionhistory/{{$id}}">  <strong>Transaction history</strong></a>     
			</li>

		</ol>
	</div>
</div>




<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Transaction History</h5>
 <button class="btn btn-export  btn-success " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'History', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
      width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
   
"><i class="fa fa-file-excel-o" aria-hidden="true"  style="margin-left:2px;"></i> Export</button>
					</div>

					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th>TransactonId</th>
											<th>sender name </th>
											<th>Card name </th>
											<th>Transaction Balance </th>
											<th>Receiver Name</th>
											<th>Receipt Number</th>
											<th>Total bill</th>
											<th>Date </th>
											<th>Status</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									@foreach($history as $hist)
										<tr>                       
											<td>{{$hist->t_id}}</td>
											<td>{{$hist->s_name}}</td>
											<td>{{$hist->g_name}}</td>
											<td>{{$hist->transaction_balance}}</td>
											@if(!$hist->receiver_id)
											<td> -- </td>
											<td> --- </td>
											<td> --- </td>
											@else
											<td> {{$hist->r_name}} </td>
											<td> {{$hist->receipt_num}} </td>
											<td>{{$hist->bill_amount}} </td>
											@endif

											<td> {{Carbon\Carbon::parse($hist->created_at)->format(' Y-m-d ')}}</td>
											
												@if($hist->status==0)
												<td ><label class="badge badge-success ">Buy Cards </label></td>
												@elseif($hist->status==1)
												<td ><label class="badge badge-primary ">Gifted Cards </label></td>
												@else
												<td><label class="badge badge-danger ">Redeemed Cards </label></td>
											@endif
										</tr>
									@endforeach
									


									</tbody>
									<tfoot class="noPrint" >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right"></ul>
											</td>

										</tr>
									</tfoot>


								</table>
							   @if(!count($history))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found
								</div>
							@endif
							
							</div>
							


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
	
	  $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[.noPrint]'});
   $('.noPrint').show();
  });
});
</script>
@endsection
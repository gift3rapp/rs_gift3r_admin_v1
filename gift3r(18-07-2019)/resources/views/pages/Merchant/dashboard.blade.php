@extends('layout/main') @section('content')
               {!! Charts::styles() !!}
 
            <div class="wrapper wrapper-content">
        <div class="row">
                     <div class="col-lg-3">
                         <a href="merchant/gift_card">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Gift Cards</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;"> {{$gift_cards}} </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3">
                    <a href="merchant/pages/Merchant/total_purchased">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">{{$num_purchased}}</span>
                                <h5 style="color: #ed5565;">Total Sale</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;">
                                 <?php echo round($total_sales,2) . ' USD' ; ?>
                                   </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div>

                     <div class="col-lg-3">
                    <a href="merchant/pages/Merchant/total_redemption">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">All</span>
                                <h5 style="color: #ed5565;">Total Redemption</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" style="color: #ed5565;"> 
                                <?php $bal= str_replace('-','',$redeemed_amt); echo round($bal,2) . ' USD';?>
                                 </h1>
                                <div class="stat-percent font-bold text-success"></div>
                               
                            </div>
                        </div>
                        </a>
                    </div> 
        </div>
            
     
      
</div>

@endsection

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Edit GiftCard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">home</a>
                        </li>
                        <li>
                            <a href="{{url('merchant/gift_card')}}">Gift card</a>
                        </li>
                      
                       <li class="active">
                           <strong>edit giftcard</strong>
                        </li> 
              
                    </ol>
                </div>
                </div>
                 @if (Session::get('success'))
    <div class="alert alert-success alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <ul>
           {{Session::get('success')}}
        </ul>
    </div>
    @endif
    @if(count($errors))
     <div class="alert alert-danger alert-dismissable" style="
    margin-top: 20px;
    margin-bottom:  1px;
    width: 95.5%;
    margin-left: 20px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   
@endif

   <div class="wrapper wrapper-content animated fadeInRight">
              <div class="row">
                   <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit Cards</h5>
                        </div>
                        <div class="ibox-content">
                  
                 

                       <form class="form-horizontal" action='merchant/pages/Merchant/editcard/{{$id}}' method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                            
                   <div class="form-group"><label class="col-lg-2 control-label">name</label>
<div class="col-lg-6"><input type="text" name="name" placeholder=" name" class="form-control" required="" value="{{$cards['name']}}"> 
                                    </div>
                                </div>
                                
                              
                            
     
                                
            <div class="form-group"><label class="col-lg-2 control-label">Select Amount </label>   
   <div class="col-lg-6">                             
 <select class="form-control" name="price" id="sel1" required value="{{$cards['price']}}">
 @foreach($amount as $data)
      <option>{{$data->amount}}</option>
 @endforeach     
</select>  
  </div> </div>                        
                                 
                                                                                                                                                                                                
                                

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">submit</button>
                                    </div>
                                </div>
                            </form>
                           
                        </div>
                    </div>
                </div>
          </div>  
</div>




@endsection
               
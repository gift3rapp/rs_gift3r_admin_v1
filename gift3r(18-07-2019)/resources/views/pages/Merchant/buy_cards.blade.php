<style>
.table-responsive table.table.table-striped tr td{
	width: 7%;
	display: table-cell;
	padding-left: 9px;
	padding-right: 11px;
}
.table-responsive table.table.table-striped tr td:last-child {
	text-align: right;
}

</style>
<style media="screen">
a[href]:after {
		content: none !important;
	}
.noPrint {
	display: block !important;
}

.yesPrint {
	display: block !important;
}
</style>

<style media="print">
.noPrint {
	display: none !important;
}

.yesPrint {
	display: block !important;
}
</style>

@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Admin</h2>
		<ol class="breadcrumb">
			<li>
				<a href="{{url('merchant/dashboard')}}">home</a>
			</li>
			<li class="active">
				<a href="merchant/buy_cards">  <strong>buy card</strong></a>     
			</li>
		</ol>
	</div>
</div>


<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="add col-sm-12 " style="margin-top: -5px;">  
			<!-- <form action="add_merchants" method="post">
				<a href="merchant/pages/Merchant/addgiftcard"><button class="btn btn-success" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Add Cards</button></a> </form> -->


			</div> 
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="color: #EF4036;">Buy cards </h5>
						<button class="btn btn-export  btn-success " id="btnExport" type="button" onClick ="$('.noPrint').hide() ;$('#printTable').tableExport({type:'excel',fileName:'BuyCard', separator:';', escape:'false' ,ignoreColumn:'[]'}); $('.noPrint').show();" style="
      width: 68px;
    height: 30px;
    padding-top: 4px;
    padding-left:  5px;
    margin-right:20px;
    padding-right:66px;float: right;margin-top: -6px;
   
"><i class="fa fa-file-excel-o" aria-hidden="true"  style="margin-left:2px;"></i> Export</button>


@if(!count($cards))
					<!-- <form action="" method="get" role="search">

						<select name="" id="selector2" class="form-control m-b"  style="width: 180px; margin-left:113px;margin-top: -6px;" >
							<option value="">Select User name </option>

						

							
							
						</select>
						<span class="input-group-btn">
							<button type="" class="btn btn-sm btn-danger" 
							style=" margin-top: -47px;
							float: right;
							margin-right: 617px;border-radius: 3px;"
							>
							GO</button>
						</span>
					</form> -->
					@else
 <form action="{{url('merchant/buy_cards')}}" method="get" role="search">
				 <select name="id" id="selector2" class="form-control m-b"  style="width: 180px; margin-left:113px;margin-top: -6px;" >
                        <option value="">Select User name </option>
                        
                        @foreach($id as $lists) 
                             
                        <option  value="{{$lists->s_id}}">
                         {{$lists->name .' ('. $lists->u_phone .')'}} 
                        </option>
                        @endforeach
                     </select>
                    <span class="input-group-btn">
										<button type="submit" class="btn btn-sm btn-danger" 
   style=" margin-top: -47px;
    float: right;
    margin-right: 617px;border-radius: 3px;"
>
											GO</button>
									</span>
                     </form>
                     @endif

					</div>
					<div class="ibox-content">
						<div class="row">

							<div class="table-responsive">
								<table class="table table-striped" id="printTable">
									<thead>
										<tr>
											<th> User Name</th>
											<th>Card name</th>
											<th> Price(In USD)</th>
											<th> Date</th>
											
										</tr>
									</thead>
									<tbody>
										@foreach($cards as $card)  
										<tr>                       
											<td>{{$card->name}}</td>
											<td>{{$card->g_name}}</td>
											<td>{{$card->price}}</td>
										<td style="text-align: left;">{{Carbon\Carbon::parse($card->created_at)->format('d M, Y ')}}</td>
										</tr>
										@endforeach


									</tbody>
									<tfoot >
										<tr>
											<td colspan="12">
												<ul class="pagination pull-right"></ul>
											</td>

										</tr>
									</tfoot>


								</table>
								@if(!count($cards))
								<div style="padding: 50px; font-size: 20px; text-align: center;padding-top:1px; font-family: sans-serif;">No results found</div>
								@endif

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<script>
    $(document).ready(function() {
  $("#btnExport").click(function(e) {
    e.preventDefault();
	
	  $('.noPrint').hide();
  $('#printTable').tableExport({type:'excel', separator:';', escape:'false' ,ignoreColumn:'[3,4,5,.noPrint]'});
   $('.noPrint').show();
  });
});
</script>











	@endsection
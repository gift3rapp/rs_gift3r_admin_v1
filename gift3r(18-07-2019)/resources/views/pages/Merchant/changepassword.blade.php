@extends('layout/main') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Change Password</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">home</a>
                        </li>
                        <li class="active">
                          <strong>change password</strong> 
                        </li>
                    </ol>
                </div>
            </div>
            <div>
            @if ($errors->any())
    <div class="alert alert-danger alert-dismissable" style="margin-top: 32px; margin-left: 21px;margin-right: 26px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    </div>
@endif
@if(Session::get('success'))
              
    <div class="alert alert-success alert-dismissable" style=" margin-top: 32px; margin-left: 21px;margin-right: 26px;">
    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{Session::get('success')}}
                </div>

@endif
  <div class="wrapper wrapper-content animated fadeInRight">
   <div class="col-lg-12">
      <div class="ibox float-e-margins">
         <div class="ibox-title">
            <h5>ChangePassword</h5>
         </div>
         <div class="ibox-content">
            <form class="form-horizontal" action="{{url('merchant/pages/Merchant/changepassword')}}" method="post" >
               {{csrf_field()}}
               <div class="form-group">
                  <label class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-6">  <input type="password" name="password"  placeholder="Enter current password" class="form-control" required="">
                  </div>
               </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">New Password</label>
                  <div class="col-lg-6">  <input type="password" name="new_password"  placeholder="Enter new password" class="form-control" required="">
                  </div>
               </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label">Confirm Password</label>
                  <div class="col-lg-6">  <input type="password" name="confirm_password" placeholder="Confirm password" class="form-control"
                     required="">
                  </div>
               </div>


                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                     <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
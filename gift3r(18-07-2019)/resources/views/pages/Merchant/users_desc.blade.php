@extends('layout/main') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Admin</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">Home</a>
                        </li>
                        <li class="">
                      <a href="{{url('merchant/pages/Merchant/users')}}">  <strong>users</strong></a>     
                        </li>
                      <li class="active">
                @foreach($descs as $desc)
                <a href="merchant/pages/Merchant/users_desc/{{$desc->id}}"> 
                <strong>userDescription</strong>
                </a>  
                @endforeach   
                        </li>
                    </ol>
                </div>
            </div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-9 animated fadeInRight">
		@foreach($descs as $desc)
			<div class="mail-box-header">

				<div class="pull-right tooltip-demo">
	
					<form action="{{url('owner/complaints')}}" method="post">
						{{csrf_field()}} 
						<input type="hidden" name="id"
							value="{{$desc->id}}"> 
                </form>

					

				</div>
				<h3><span class="font-noraml">User: </span>{{$desc->name}}</h3>
				<div class="mail-tools tooltip-demo m-t-md">


					

					<h3>
						
						<span class="font-noraml">email: </span>{{$desc->email}}
					
					</h3>
					<h3>
						
						<span class="font-noraml">phone number: </span>{{$desc->phone}}
					
					</h3>
					<h3>
						
						<span class="font-noraml">Available balance: </span>{{$desc->available_balance}}
					
					</h3>
				
				
					
					
				</div>
			</div>
			<div class="pull-right" style="margin-right: 20px;margin-top: -136px;">
				<img src="{{$desc->image}}" width="100" height="100">
				
			</div>
@endforeach
			
			   



		</div>
	</div>
</div>
 @endsection

@extends('layout/main') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Profile</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('merchant/dashboard')}}">home</a>
                        </li>
                      <li class="active">
                            merchant profile
                        </li
                      
                    </ol>
                </div>
            </div>    
  
<div class="wrapper wrapper-content  animated fadeInRight">
 @foreach($profile as $pro) 
            <div class="row">
           
             <div class="col-sm-4">
                    <div class="ibox ">

                        <div class="ibox-content">
                            <div class="tab-content">
                              <div id="contact-1" class="tab-pane active">
                                    <div class="row m-b-lg">

                                        <div class="col-lg-12 ">
                                            <h2>Profile</h2>

                                            <div class="m-b-sm">
                                             <img  class="" src="{{$pro->image}}" style=" width: 250px; height: 250px;">
                                            </div>
                                          <div class="m-b-sm">
                                             <h2>{{$pro->name}}</h2>
                                            </div>
                                        </div>
                                      
                                    </div>
                                    <div class="client-detail profile">
                                      <form action="{{url('merchant/pages/Merchant/merchantprofile')}}" method="post">
                                    {{csrf_field()}}
                                   
                                    <input type="hidden" name="id" value="{{$pro->id}}">
                                   
                                  <!--  @if($pro->status==1)
                                   <button class="btn btn-success" type="submit" name="status" value="1">       
                                  Enabled
                                   </button> 
                                   <button class="btn btn-default " type="submit" name="status" value="0">
                                  Disable
                                   </button>
                         
                                   
                                   @else 
                                   <button class="btn btn-default " type="submit" name="status" value="1">
                                  Enable
                                   </button>                                   
                                   <button class="btn btn-danger " type="submit" name="status" value="0">       
                                  Disabled
                                   </button>
                                   @endif -->
                                    </form>
                                    <div class="full-height-scroll">
                                

                                       
                                    </div>
                                    <div>

                                    </div>
                                    </div>
                                </div>

                         
                                   </div>
                            </div>
                            </div>

            </div>
              <div class="col-sm-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Details</h5>

                       <div style="float: right; margin-top: -9px;">
                        <form action="add_merchants" method="post" >
                <a href="merchant/pages/Merchant/merchants"><button class="btn btn-success" type="button" >Manage Store</button></a> </form>

                </div>
               
                    </div>
                     
                    <div class="ibox-content">
 
                        <table class="table" >

                           <tbody >
                           
                            <tr >
                               <td style="border-style: none;"><a data-toggle="tab" href="#contact-1" class="client-link"> Name: </a></td>
                                                    <td style="border-style: none;">  {{$pro->name}}</td>
                            </tr>
                            <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Email: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$pro->email}}</td>
                            </tr>
                          <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link" href=""> Website: </a></td>
                                 <td style="line-height: 2.42857;" >  <a href="{{$pro->website}}"> {{$pro->website}}</a></td>
                            </tr>
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Address: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$pro->address}}</td>
                            </tr>
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> City: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$pro->city}}</td>
                            </tr>
                            
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link"> Zipcode: </a></td>
                                                    <td style="line-height: 2.42857;" > {{$pro->zipcode}}</td>
                            </tr>
                           
                             <tr>
                               <td style="line-height: 2.42857;"><a data-toggle="tab" href="#contact-1" class="client-link">Phone Number:</a></td>
                                                    <td style="line-height: 2.42857;"> {{$pro->phone_number}}</td>
                            </tr>
                            
                            </tbody>
                        </table>

                    </div>
                  
                    </div>
                </div>
            </div>
  @endforeach
    </div>
@endsection
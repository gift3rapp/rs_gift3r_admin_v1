<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse" id="menu"> 
                 <ul class=" nav nav metismenu" id="side-menu">
                    <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                         
                          <img src ="{{Session::get('user.image')}}"  style="width: 40px; margin-top:10px;" class="img-circle"> 
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo Session::get('user.name') ?> </strong>
                             </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                              <li><a href="admin/pages/Admin/changepassword">change password</a></li>
                                <li class="divider"></li>
                                <li><a href="{{url('logout')}}">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                  
                        </div>
                    </li>

                    
                    <li @if(Request::path()=="admin/dashboard") class="active"   @endif >

                        <a href="admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>


                    <li @if(Request::path()=="admin/pages/Admin/users") class="active" @endif   >

                        <a href="admin/pages/Admin/users"><i class="fa fa-th-large"></i> <span class="nav-label">Users</span> </a>

                    </li>
                   
                  
                    <li @if(Request::path()=="admin/pages/Admin/merchants") class="active" @endif>
                        <a href="admin/pages/Admin/merchants"><i class="fa fa-user"></i> <span class="nav-label">Merchants</span></a>
                    </li>
                   <!--  <li @if(Request::path()=="admin/pages/Admin/merchant_saled") class="active" @endif>
                        <a href="admin/pages/Admin/merchant_saled"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Sales</span></a>
                    </li> -->
                 
                </ul>

            </div>
        </nav>
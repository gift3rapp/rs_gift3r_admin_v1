<?php

namespace App\Library;

class Functions {

    public static function upload_picture($picture, $upload_picture_path) {
        $orig_picture_name = str_random(8) . "_" . $picture->getClientOriginalName();
        $picture_name = $orig_picture_name;
        for ($i = 2;; $i ++) {
            if (file_exists(base_path() . $upload_picture_path . $picture_name)) {
                $picture_name = $i . "_" . $orig_picture_name;
            } else {
                break;
            }
        }
        $final_name = str_replace(' ','',$picture_name);
        $picture->move(base_path() . $upload_picture_path, $final_name);
        return $final_name;
    }
 public static function generate_token() {
        $post_fields = "grant_type=client_credentials";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.orange.com/oauth/v2/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Basic " . base64_encode(env('ORANGE_CLIENT_ID') . ":" . env('ORANGE_CLIENT_SECRET'))
            ],
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_POSTFIELDS => $post_fields
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
        } else {
            //echo $response;
        }

        $response = json_decode($response, true);
        if (is_array($response) && isset($response["access_token"])) {
            return $response["access_token"];
        } else {
            return 0;
        }
    }

    public static function send_message($phone, $message) {
        $access_token = self::generate_token();
        if($access_token){
            $phone="+22507917070";
            $phone_2="+22587140703";
            $access_token = "Z5ArNp0azONxkb4dsO4kOPMLFP6z";
            $post_fields = '{"outboundSMSMessageRequest":{"address": "tel:'.$phone.'","senderAddress":"tel:'.env('ORANGE_SENDER').'","outboundSMSTextMessage":{"message": "'.$message.'"}}}';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.orange.com/smsmessaging/v1/outbound/tel%3A%2B22507917070/requests",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer " . $access_token,
                    "Content-Type: application/json"
                ],
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                CURLOPT_POSTFIELDS => $post_fields
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                //echo "cURL Error #:" . $err;
            } else {
                //echo $response;
            }
        }
    }
    
    
    public static function viaTwilio($phone_number,$text_message){
        //$twilio_phone_number = "GIFT3R";
           
         $post_fields = "To=" . urlencode($phone_number) . "&From=" .
                urlencode(env("TWILIO_FROM")).  "&Body=" . urlencode(
                        $text_message);
              
        $curl = curl_init();

     curl_setopt_array ( $curl,
                array (
                    CURLOPT_URL => "https://api.twilio.com/2010-04-01/Accounts/" .
                             env ( "TWILIO_SID" ) . "/Messages.json",

                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_SSL_VERIFYPEER => false,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                            CURLOPT_USERPWD => env ( "TWILIO_SID" ) . ":" .
                             env ( "TWILIO_TOKEN" ),
                            CURLOPT_POSTFIELDS => $post_fields,

                ) );
      
         curl_exec($curl);

        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          
            echo response ()->json (['error' => 'bad_request',
                                        'error_description' => "Invalid phone number"],400);
        } 
        else {
              $response = ["message" => "successfully send otp"];
            header('Content-Type: application/json');
           echo json_encode($response);
    }

}
}

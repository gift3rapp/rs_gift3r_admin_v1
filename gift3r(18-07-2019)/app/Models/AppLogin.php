<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppLogin extends Model
{
    //
    protected $table='app_login';
    protected  $fillable =['user_id','device_id','device_type','session_id','device_token'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //
    protected $table='admin';

    const UPLOAD_PICTURE_PATH = "/public/assets/uploads/";
    const PICTURE_PATH = "thumb/uploads/";
    const DEFAULT_PATH = "thumb/uploads/";
    public function getImageAttribute($value) 
    {
        if ($value) 
        {
            return url ( self::PICTURE_PATH . $value );
        }
        else
        {
           return url ( self::PICTURE_PATH .'0_default.png' );
       }
   }   
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table='payments';
    protected $fillable = ['user_id', 'card_number', 'card_type','cvv','expiry_date','amount','order_id','unique_ref','hash_key','card_id','zipcode'];

}


<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserCards extends Model
{
    //
  protected $table='user_cards';

  protected $fillable=['user_id','gift_card_id','available_balance','transaction_id','status','created_at','updated_at'];

  const UPLOAD_PICTURE_PATH = "/public/assets/uploads/";
  const PICTURE_PATH = "assets/uploads/";
  const DEFAULT_PATH = "thumb/uploads/";

  public function getImageAttribute($value) 
  {
    if ($value) 
    {
      return url ( self::PICTURE_PATH . $value );
    }
    else
    {
      return url ( self::PICTURE_PATH .'0_default.png' );
    }
  }


  public function giftcard()
  {
   return $this->belongsTo('App\Models\GiftCards', 'gift_card_id', 'id');
 }

 public function store()
 {
   return $this->belongsTo('App\Models\Store', 'store_id', 'id');
 }

 public static function getResponse($result , $user_id )
 {
  $res = [];
   $redeem_purchase=Transaction::where( function($q) use ($user_id)
   {
    $q->where('status',0)
    ->orWhere('status',2);
  })->where(['card_id'=>$result->card_id,'mypurchasedcard_id'=>$result->mypurchasedcard_id])->where('sender_id',$user_id)->get();
  

   $test=0;
   foreach ($redeem_purchase as $key) 
   {
      $test+= $key->transaction_balance;
   }
  
  
  $gift_card_sent=Transaction::where('status',1)->where('card_id',$result->card_id)->where(['sender_id'=>$user_id,'mypurchasedcard_id'=>$result->mypurchasedcard_id])->get();
  $test_sent=0;
  foreach ($gift_card_sent as $key) 
  {
    $test_sent  += $key->transaction_balance;
  }

  
  $gift_card_received=Transaction::where('status',1)->where('card_id',$result->card_id)->where(['receiver_id'=>$user_id,'mypurchasedcard_id'=>$result->mypurchasedcard_id])->get();
  $test_received=0;
  foreach ($gift_card_received as $key) 
  {
    $test_received+= $key->transaction_balance;
  }


   $gift_card_send=Transaction::where('status',5)->where('card_id',$result->card_id)->where(['receiver_id'=>$user_id,'mypurchasedcard_id'=>$result->mypurchasedcard_id])->get();
  $test_send_card=0;
  foreach ($gift_card_send as $key) 
  {
    $test_send_card+= $key->transaction_balance;
  }
   
 //echo $test . "<br>" . $test_sent . "<br>" . $test_received . "<br>" . $test_send_card; die;

 $card=($test) -($test_sent)+($test_received) +($test_send_card);
//sprint_r($card); die;
	$card_balance= round( $card, 2);


   if($card_balance)
  {
  $res=['gift_card_id'=>$result->card_id,
  'available_balance'=>$card_balance,
  'gift_card_name'=>$result->g_name,
  'store_name'=>$result->s_name,
  'store_image'=>$result->image,
  'mypurchasedcard_id'=>$result->mypurchasedcard_id,
  'message'=>$result->message
];
    return $res;
 }
 //}

// elseif($flag=3)
// {
// return $card_balance;
// }
//else{
//return $card_balance;
//}






}
}

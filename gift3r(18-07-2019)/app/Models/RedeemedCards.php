<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RedeemedCards extends Model
{
    //
	protected $table='cards_redeemed';

	public $timestamps = false;
	protected $fillable = [ 'id', 'receipt_num', 'card_id', 'amount_redeem'];
	public function giftcard()
	{
		return $this->belongsTo('App\Models\GiftCards', 'card_id', 'id');
	}
	
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillAmount extends Model
{
    //
    protected $table='bill_amount';
    protected $fillable=['id','receipt_num','bill_amount','created_at','updated_at'];
}

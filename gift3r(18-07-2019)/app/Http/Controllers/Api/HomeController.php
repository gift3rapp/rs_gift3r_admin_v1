<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Mail;

use App\Models\Users;

use App\Models\OTP;

use App\Models\Store;

use App\Models\UserCards;

use App\Models\Transaction;

use App\Models\Mypurchasedcard;

use App\Models\GiftCards;

use App\Models\RedeemedCards;

use App\Models\AppLogin;

use App\Models\BillAmount;

use App\Models\PushNotification;

use  App\Models\FavouriteStore;

use  App\Models\Payment;

use DB;

use Twilio;

use PDF;

use XmlAuthRequest;

use Plivo\RestClient;

use App\Library\Functions;



class HomeController extends Controller

{

    //

    private static function payment_data($session_id,$card_number,$card_type,$cvc,$expiry_date,$amount,$card_holdername,$user_id,$card_id,$zipcode)
    {
    	    require(public_path().'/xml/nuvei_account.php');
			require(public_path().'/xml/gateway_tps_xml.php');
			require(public_path().'/xml/nuvei_hpp_functions.php');
			 
			# These values are specific to the cardholder.
			$cardNumber = $card_number;		# This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
			$cardType = $card_type;			# See our Integrator Guide for a list of valid Card Type parameters
			$cardExpiry = $expiry_date;		# (if not using SecureCard) The 4 digit expiry date (MMYY).
			$cardHolderName = $card_holdername;		# (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
			$cvv = $cvc;	
			//print_r($cvv); die;		# (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
			$issueNo = '';			# (optional) Issue number for Switch and Solo cards.
			$email = '';			# (optional) If this is sent then Nuvei will send a receipt to this e-mail address.
			$mobileNumber = "";		# (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

			# These values are specific to the transaction.
			$orderId = '';			# This should be unique per transaction (12 character max).
			$amount = $amount;			# This should include the decimal point.
			$isMailOrder = false;		# If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.

			# These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
			$address1 = '';			# (optional) This is the first line of the cardholders address.
			$address2 = '';			# (optional) This is the second line of the cardholders address.
			$postcode = '';			# (optional) This is the cardholders post code.
			$country = '';			# (optional) This is the cardholders country name.
			$phone = '';			# (optional) This is the cardholders home phone number.

			# eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
			$cardCurrency = '';		# (optional) This is the three character ISO currency code returned in the rate request.
			$cardAmount = '';		# (optional) This is the foreign currency transaction amount returned in the rate request.
			$conversionRate = '';		# (optional) This is the currency conversion rate returned in the rate request.

			# 3D Secure reference. Only include if you have verified 3D Secure throuugh the Nuvei MPI and received an MPIREF back.
			$mpiref = '';			# This should be blank unless instructed otherwise by Nuvei.
			$deviceId = '';			# This should be blank unless instructed otherwise by Nuvei.

			$autoready = '';		# (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
			$multicur = false;		# This should be false unless instructed otherwise by Nuvei.

			$description = '';		# (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
			$autoReady = '';		# (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
			if(!isset($orderId) || $orderId == '') 
				$orderId = generateUniqueOrderId();
			//echo "order id: ".$orderId."<br/>";
			# Set up the authorisation object
			$auth = new XmlAuthRequest($terminalId,$orderId,$currency,$amount,$cardNumber,$cardType);
			if($cardType != "SECURECARD") $auth->SetNonSecureCardCardInfo($cardExpiry,$cardHolderName);
			if($cvv != "") $auth->SetCvv($cvv);
			if($cardCurrency != "" && $cardAmount != "" && $conversionRate != "") 
				$auth->SetForeignCurrencyInformation($cardCurrency,$cardAmount,$conversionRate);
			if($email != "") $auth->SetEmail($email);
			if($mobileNumber != "") $auth->SetMobileNumber($mobileNumber);
			if($description != "") $auth->SetDescription($description);
			 
			if($issueNo != "") $auth->SetIssueNo($issueNo);
			if($address1 != "" && $address2 != "" && $postcode != "") $auth->SetAvs($address1,$address2,$postcode);
			if($country != "") $auth->SetCountry($country);
			if($phone != "") $auth->SetPhone($phone);
			 
			if($mpiref != "") $auth->SetMpiRef($mpiref);
			if($deviceId != "") $auth->SetDeviceId($deviceId);
			 
			if($multicur) $auth->SetMultiCur();
			if($autoready) $auth->SetAutoReady($autoready);
			if($isMailOrder) $auth->SetMotoTrans();
			 
			# Perform the online authorisation and read in the result
			$response = $auth->ProcessRequestToGateway($secret,$testAccount, $gateway);
			 
			// echo "<pre>"; print_r($response); 
			 
			$expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() .$response->BankResponseCode(). $secret);
			//echo "Response Hash Key :".$response->Hash(); echo "<br/>";
			//echo "Expected Response Hash Key :". $expectedResponseHash; die;
			 
			if($response->IsError()){ 
				$resu='Your transaction was not processed. Error details: ' . $response->ErrorString();
						 $statusCode=200;
		$respons=[
			'statusCode'=>$statusCode,
			"error"=>true,
            "error_msg"=>$resu,
			'message'=>'',
			'payment_id'=>''  // change done for paymentdetail by manpreet 24-6-19
		];
			}
			elseif($expectedResponseHash == $response->Hash()) { //echo "tey";
				$user=DB::table('users')->where('id',$user_id)->first();
				$card_data=DB::table('gift_cards')->where('id',$card_id)->first();
				$store_data=DB::table('stores')->where('id',@$card_data->store_id)->first();
			 	switch($response->ResponseCode()) {
					case "A" :	# -- If using local database, update order as Authorised.
					$payment=Payment::create([
					'user_id'=>$user_id,
					'card_number'=>$cardNumber,
					'card_type'=>$cardType,
					'cvv'=>$cvv,
					'expiry_date'=>$cardExpiry,
					'amount'=>$amount,
					'order_id'=>$orderId,
					'card_id'=>$card_id,
					'zipcode'=>$zipcode,
					'unique_ref'=>$response->UniqueRef(),
					'hash_key'=>$response->Hash()
			]);
					if($payment){

						$payment_id=$payment->id;
						$payment_data=Payment::where('id',$payment_id)->first();
						$purchase_date=date('F d, Y',strtotime($payment_data->created_at));
						$is_send=1;
                          $pdf_url=self::pdfview($user,$amount,$card_number,$orderId,$purchase_date,$card_id,$payment_id);
                          $url=public_path('/pdf/'.$pdf_url);
                          $card_num=strlen($card_number);
						$val_data=$card_num-4;
						$a='';
						for($i=0;$i<$val_data;$i++)
						{
				           $a=$a.'X';
						}
		$card=$a.substr($card_number,$val_data,$card_num-1);
						 Mail::send( 'emails.payment-confirmation',
    			 [  "orderId" => $orderId,
    			 "amount"=>$amount,
    			 'user_name'=>$user->name,
    			 'purchase_date'=>$payment_data->created_at,
    			 'card_number'=>$card,
    			 'gift_name'=>@$card_data->name,
    			 'store_name'=>@$store_data->name
    				 ],

     function ($m) use($user,$url) 
     { 
                $m->from('admin@gift3r.com', 'Gift3r App');
                $m->attach($url);

        	$m->to($user->email)->subject('Payment Confirmation');
     } 
     );
					 $resu= 'Payment Processed successfully. Thanks you for your order';

			$statusCode=200;
		    $respons=[
			'statusCode'=>$statusCode,
			"error"=>false,
            "error_msg"=>'',
			'message'=>$resu,
			'payment_id'=>$payment_id // change done for paymentdetail by manpreet 24-6-19
		];

		
				}
							$uniqueRef = $response->UniqueRef();
							$responseText = $response->ResponseText();
							$approvalCode = $response->ApprovalCode();
							$avsResponse = $response->AvsResponse();
							$cvvResponse = $response->CvvResponse();
							break;
					case "R" :
					case "D" :
					case "C" :
					case "S" :
			 
					default  :	# -- If using local database, update order as declined/failed --
							$resu='PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
							$statusCode=200;
					$respons=[
						'statusCode'=>$statusCode,
						"error"=>true,
			            "error_msg"=>$resu,
						'message'=>'',
						'payment_id'=>''   // change done for paymentdetail by manpreet 24-6-19
					];

		
				}

			} else {
				$uniqueReference = $response->UniqueRef();
				$resu='PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
				if(isset($uniqueReference)) $resu= 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Unique Reference: ' . $uniqueReference . ' when mailing or calling.';
				$statusCode=200;
		$respons=[
			'statusCode'=>$statusCode,
			"error"=>true,
            "error_msg"=>$resu,
			'message'=>'',
			'payment_id'=>''    // change done for paymentdetail by manpreet 24-6-19
		];

		
			 }
			return $respons; 
    }



    private static function payment_data_v1($session_id,$card_number,$card_type,$cvc,$expiry_date,$amount,$card_holdername,$user_id,$card_id,$zipcode)
    {
    	    require(public_path().'/xml/nuvei_account.php');
			require(public_path().'/xml/gateway_tps_xml.php');
			require(public_path().'/xml/nuvei_hpp_functions.php');
			 
			# These values are specific to the cardholder.
			$cardNumber = $card_number;		# This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
			$cardType = $card_type;			# See our Integrator Guide for a list of valid Card Type parameters
			$cardExpiry = $expiry_date;		# (if not using SecureCard) The 4 digit expiry date (MMYY).
			$cardHolderName = $card_holdername;		# (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
			$cvv = $cvc;	
			//print_r($cvv); die;		# (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
			$issueNo = '';			# (optional) Issue number for Switch and Solo cards.
			$email = '';			# (optional) If this is sent then Nuvei will send a receipt to this e-mail address.
			$mobileNumber = "";		# (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

			# These values are specific to the transaction.
			$orderId = '';			# This should be unique per transaction (12 character max).
			$amount = $amount;			# This should include the decimal point.
			$isMailOrder = false;		# If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.

			# These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
			$address1 = '';			# (optional) This is the first line of the cardholders address.
			$address2 = '';			# (optional) This is the second line of the cardholders address.
			$postcode = $zipcode;			# (optional) This is the cardholders post code.
			$country = '';			# (optional) This is the cardholders country name.
			$phone = '';			# (optional) This is the cardholders home phone number.

			# eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
			$cardCurrency = '';		# (optional) This is the three character ISO currency code returned in the rate request.
			$cardAmount = '';		# (optional) This is the foreign currency transaction amount returned in the rate request.
			$conversionRate = '';		# (optional) This is the currency conversion rate returned in the rate request.

			# 3D Secure reference. Only include if you have verified 3D Secure throuugh the Nuvei MPI and received an MPIREF back.
			$mpiref = '';			# This should be blank unless instructed otherwise by Nuvei.
			$deviceId = '';			# This should be blank unless instructed otherwise by Nuvei.

			$autoready = '';		# (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
			$multicur = false;		# This should be false unless instructed otherwise by Nuvei.

			$description = '';		# (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
			$autoReady = '';		# (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
			if(!isset($orderId) || $orderId == '') 
				$orderId = generateUniqueOrderId();
			//echo "order id: ".$orderId."<br/>";
			# Set up the authorisation object
			$auth = new XmlAuthRequest($terminalId,$orderId,$currency,$amount,$cardNumber,$cardType,$cardHolderName,$cardExpiry);
			//print_r($auth); die;
			if($cardType != "SECURECARD") $auth->SetNonSecureCardCardInfo($cardExpiry,$cardHolderName);
			if($cvv != "") $auth->SetCvv($cvv);
			if($cardCurrency != "" && $cardAmount != "" && $conversionRate != "") 
				$auth->SetForeignCurrencyInformation($cardCurrency,$cardAmount,$conversionRate);
			if($email != "") $auth->SetEmail($email);
			if($mobileNumber != "") $auth->SetMobileNumber($mobileNumber);
			if($description != "") $auth->SetDescription($description);
			 
			if($issueNo != "") $auth->SetIssueNo($issueNo);
			if($postcode != "") $auth->SetAvs($address1,$address2,$postcode);
			if($country != "") $auth->SetCountry($country);
			if($phone != "") $auth->SetPhone($phone);
			 
			if($mpiref != "") $auth->SetMpiRef($mpiref);
			if($deviceId != "") $auth->SetDeviceId($deviceId);
			 
			if($multicur) $auth->SetMultiCur();
			if($autoready) $auth->SetAutoReady($autoready);
			if($isMailOrder) $auth->SetMotoTrans();
			 
			# Perform the online authorisation and read in the result
			$response = $auth->ProcessRequestToGateway($secret,$testAccount, $gateway);
			 
			 echo "<pre>"; print_r($response);
			 
			$expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);
			echo "Response Hash Key :".$response->Hash(); echo "<br/>";
			echo "Expected Response Hash Key :". $expectedResponseHash; die;
			 
			if($response->IsError()){ 
				$resu='Your transaction was not processed. Error details: ' . $response->ErrorString();
						 $statusCode=200;
		$respons=[
			'statusCode'=>$statusCode,
			"error"=>true,
            "error_msg"=>$resu,
			'message'=>''
		];
			}elseif($expectedResponseHash == $response->Hash()) {
				$user=DB::table('users')->where('id',$user_id)->first();
				$card_data=DB::table('gift_cards')->where('id',$card_id)->first();
				$store_data=DB::table('stores')->where('id',@$card_data->store_id)->first();
			 	switch($response->ResponseCode()) {
					case "A" :	# -- If using local database, update order as Authorised.
					$payment=Payment::create([
					'user_id'=>$user_id,
					'card_number'=>$cardNumber,
					'card_type'=>$cardType,
					'cvv'=>$cvv,
					'expiry_date'=>$cardExpiry,
					'amount'=>$amount,
					'order_id'=>$orderId,
					'unique_ref'=>$response->UniqueRef(),
					'hash_key'=>$response->Hash()
			]);
					if($payment){

						$payment_id=$payment->id;
						$payment_data=Payment::where('id',$payment_id)->first();
						$purchase_date=date('F d, Y',strtotime($payment_data->created_at));
						$is_send=1;
                          $pdf_url=self::pdfview($user,$amount,$card_number,$orderId,$purchase_date,$card_id,$payment_id);
                          $url=public_path('/pdf/'.$pdf_url);
                          $card_num=strlen($card_number);
						$val_data=$card_num-4;
						$a='';
						for($i=0;$i<$val_data;$i++)
						{
				           $a=$a.'X';
						}
		$card=$a.substr($card_number,$val_data,$card_num-1);
						 Mail::send( 'emails.payment-confirmation',
    			 [  "orderId" => $orderId,
    			 "amount"=>$amount,
    			 'user_name'=>$user->name,
    			 'purchase_date'=>$payment_data->created_at,
    			 'card_number'=>$card,
    			 'gift_name'=>@$card_data->name,
    			 'store_name'=>@$store_data->name
    				 ],

     function ($m) use($user,$url) 
     { 
                $m->from('admin@gift3r.com', 'Gift3r App');
                $m->attach($url);

        	$m->to($user->email)->subject('Payment Confirmation');
     } 
     );
					 $resu= 'Payment Processed successfully. Thanks you for your order';

			$statusCode=200;
		    $respons=[
			'statusCode'=>$statusCode,
			"error"=>false,
            "error_msg"=>'',
			'message'=>$resu,
		];

		
				}
							$uniqueRef = $response->UniqueRef();
							$responseText = $response->ResponseText();
							$approvalCode = $response->ApprovalCode();
							$avsResponse = $response->AvsResponse();
							$cvvResponse = $response->CvvResponse();
							break;
					case "R" :
					case "D" :
					case "C" :
					case "S" :
			 
					default  :	# -- If using local database, update order as declined/failed --
							$resu='PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
							$statusCode=200;
					$respons=[
						'statusCode'=>$statusCode,
						"error"=>true,
			            "error_msg"=>$resu,
						'message'=>''
					];

		
				}

			} else {
				$uniqueReference = $response->UniqueRef();
				$resu='PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
				if(isset($uniqueReference)) $resu= 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Unique Reference: ' . $uniqueReference . ' when mailing or calling.';
				$statusCode=200;
		$respons=[
			'statusCode'=>$statusCode,
			"error"=>true,
            "error_msg"=>$resu,
			'message'=>''
		];

		
			 }
			return $respons; 
    }


    private static function pdfview($user,$amount,$card_number,$orderId,$purchase_date,$card_id,$payment_id)
	{
		$invoice_number='INV-'.$payment_id;
		$invoice_date=date('F d,Y');

		$card_num=strlen($card_number);
		$val_data=$card_num-4;
		$a='';
		for($i=0;$i<$val_data;$i++)
		{
           $a=$a.'X';
		}
		$card=$a.substr($card_number,$val_data,$card_num-1);

		$card_data=DB::table('gift_cards')->where('id',$card_id)->first();
				$store_data=DB::table('stores')->where('id',@$card_data->store_id)->first();
        view()->share(['items'=>$user,'card_number'=>$card,'amount'=>$amount,'orderId'=>$orderId,'invoice_number'=>$invoice_number,'purchase_date'=>$purchase_date,'invoice_date'=>$invoice_date,'card_info'=>@$card_data,'store_info'=>@$store_data]);
          $pdf = PDF::loadView('api.pdfview');
           $file_name='invoice-'.$payment_id.'.pdf';
           $url=public_path('pdf/'.$file_name);
           $pdf->save($url);
            return $file_name; 
        
	}

    public function payment(request $request)
    {
    	 $user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}  
    	 $validator = Validator::make ($request->all (),[
             
			'card_number' => 'required',
			'card_holdername'=>'required',
			'cvv'=>'required',
			'expiry_date' => 'required',
			'card_type'=>'required',
			'amount'=>'required',
			'zipcode'=>'required',
			'card_id'=>'nullable'
            ])
;		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		}

		$res=self::payment_data($request->session_id,$request->card_number,$request->card_type,$request->cvv,$request->expiry_date,$request->amount,$request->card_holdername,$user_id,$request->card_id,$request->zipcode);
        $statusCode=200;

      
		$response=$res;
		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE); 
		
    }


     public function payment_v1(request $request)
    {
    	 $user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}  
    	 $validator = Validator::make ($request->all (),[
             
			'card_number' => 'required',
			'card_holdername'=>'required',
			'cvv'=>'required',
			'expiry_date' => 'required',
			'card_type'=>'required',
			'amount'=>'required',
			'zipcode'=>'required',
			'card_id'=>'nullable'
            ])
;		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		}

		$res=self::payment_data_v1($request->session_id,$request->card_number,$request->card_type,$request->cvv,$request->expiry_date,$request->amount,$request->card_holdername,$user_id,$request->card_id,$request->zipcode);
        $statusCode=200;

      
		$response=$res;
		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE); 
		
    }

    
	private static function createAppLoginSession($request, $user_id)

	{   

		$app_login = AppLogin::updateOrCreate(["device_id" => $request->device_id ? $request->device_id : 0],

			[ "user_id" =>  $user_id,

			"session_id" => str_random(32),

			"device_type" => $request->device_type,

			"device_id" => $request->device_id ? $request->device_id : null,

			"device_token" => $request->device_token,

			]);

		return $app_login;

	}



	private static function Otp($request)

	{      

		 $otpr = rand(1000, 9999);


		$otps=OTP::updateOrCreate(["country_code"=>$request->country_code,

			"phone"=>$request->phone_number],

			["country_code"=>$request->country_code,

			"phone"=>$request->phone_number,

			"otp"=>$otpr

			]);

		$phone_number = $request->country_code. $request->phone_number;
		$phone_number=trim($phone_number);


			// $phone_number = trim ( $phone_number, "+" );
		$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$phone_number=str_replace($arrayfind,$arrayreplace, $phone_number); 

			//print_r($phone_number);
		//$phone_number = str_replace(" ", "", $phone_number);



		    $text_message = "Use" . " " . $otpr."  as your OTP (One Time Password) to verify your mobile number for our app GIFT3R. Do not share with anyone";


        

		//Functions::viaTwilio($phone_number, $text_message);
             $auth_id='MAYZU5ODVINJY2ODC0YJ';
            $auth_token='OWYwYjZhNjUxODgwZTU4NjQ5ODU3ZjI2NTMyNDQ4';
            $client = new RestClient($auth_id, $auth_token);
        if($client->messages->create(
                '14582038931',
                [$phone_number],
                $text_message
            )){
          
          $response = ["message" => "successfully send otp"];
            header('Content-Type: application/json');
           echo json_encode($response);
            
        } 
        else {
         echo response ()->json (['error' => 'bad_request',
                                        'error_description' => "Invalid phone number"],400);     
    }




	}

	//otp to phone

	public function sendOtp(Request $request)

	{

		if (Users::where('email',$request->email)->exists()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Already existing email in records"

				],400);

		} 

		elseif(Users::where('phone',$request->phone_number)->exists()) 

		{



			$info=Users::where('phone',$request->phone_number)->first();

			if(empty($info->email))

			{

				$res=self::Otp($request);



			}

			else

			{

				return response ()->json (['error' => 'bad_request',

					'error_description' => "Already records in database"

					],400);



			}

		}

		else

		{

			$res=self::Otp($request);



		}



	}


//contactlist

	public function contactlist(Request $request)
	{
		
		
		$validator = Validator::make ($request->all (),[

			'number' => 'required'

			
			]);
		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		}
		
		 //print_r(count($request->number));
		 //die();
		$numberlist=$request->number;
		$namelist=$request->namelist;
		$addresslist=$request->addresslist;
		

		$status=[];
		$second=[];

		foreach($numberlist as $list)
		{
			$status1=0;
			$arrayfind=[' ','-','+','(',')','*'];
			$arrayreplace=['','','','','',''];
			$firstnumber=str_replace($arrayfind,$arrayreplace, $list);
			// $firstnumber=str_replace('-','', $firstnumber);
			$firstnumber=trim($firstnumber);
			if(strlen($firstnumber)>10)
			{
				$secondnumber=substr($firstnumber,(strlen($firstnumber)-10),(strlen($firstnumber)-1));
				$second[]=$secondnumber;
				
			}
			else
			{
				$second[]=trim($firstnumber);
				$secondnumber='1'.trim($firstnumber);
			}
			
			
			if(!empty($this->checknumber($firstnumber)))
			{
				$status1=1;
			}
			if(!empty($this->checknumber($secondnumber)))	
			{
				$status1=1;	
			}		
			
			$status[]=$status1;

		}
        
		$statusCode=200;
		$response=["number"=>$second,"status"=>$status,"namelist"=>$namelist,"addresslist"=>$addresslist];

		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);





	}

public function contactlists(Request $request)
	{
		
		
		$validator = Validator::make ($request->all (),[

			'number' => 'required'

			
			]);
		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		}
		
		$numberlist=$request->number;
		$namelist=$request->namelist;
		$addresslist=$request->addresslist;
		

		$status=[];

		foreach($numberlist as $list)
		{
			$status1=$this->checknumber($list);
			$status[]=$status1;

		}

		$statusCode=200;
		$response=["number"=>$numberlist,"status"=>$status,"namelist"=>$namelist,"addresslist"=>$addresslist];

		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);





	}
	//function to check number is on app or not

	public function checknumber($number)
	{
		 //echo "<pre>"; print_r($number); echo "</pre>";
		$status=0;
        $phone_number1=preg_replace('/\D/', '', $number);
        $phone_number=str_replace('-','',$phone_number1);
       // echo "<pre>"; print_r($phone_number1); echo "</pre>";
		$info=Users::where('phone','LIKE','%'.$phone_number1.'%')->first();

		if(!empty($info))
		{
			$status=1;
		}
		return $status;
	}

// api of check number is register number or not


	public function checknumberstatus(Request $request)
	{
		$validator = Validator::make ($request->all (),[

			'country_code' => 'Nullable',

			'phone_number' => 'required',

			]);

		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'true',

				'error_message' => $validator->getMessageBag ()

				->first ()],400);



		}
		$num=$request->country_code.$request->phone_number;

		$status=0;
 

        $phone_number=$request->phone_number;
		$info=Users::where('phone',trim($phone_number))->first();
		$info1=Users::where('phone',trim($num))->first();

		if(!empty($info))
		{
			$status=1;
		}
		if(!empty($info1))
		{
			$status=1;
		}
		$statusCode=200;
		$response=["status"=>$status,"error"=>"false","error_message"=>""];

		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
	}

    //signup



	public function signup(Request $request)

	{

	

		$validator = Validator::make ($request->all (),[

			'email' => 'required|unique:users',

			'phone_number' => 'required',

			]);

		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		}

		
		$session_id= str_random(32);

		$response=[];

		$status_code=200;

		$otpc=OTP::where('otp',$request->otp)->where('phone',$request->phone_number)->first();

		
		$phone_number=$request->phone_number;
		$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$phone_number=str_replace($arrayfind,$arrayreplace, $phone_number); 
		$num=$request->country_code . $request->phone_number;

		$user=Users::where('phone',$phone_number)->first();

		if(!empty($otpc->otp)){

		if ($request->otp==$otpc->otp) {

			if($user)

			{

			$user_update=Users::where('phone',$phone_number)->update(['email'=>$request->email,'name'=>$request->name,'password'=>md5($request->password)]);

 $user = Users::where('phone',$phone_number)->first();		

				

				$app=new AppLogin();

					$app->device_type=$request->device_type;

					$app->device_id=$request->device_id;

					$app->device_token=$request->device_token;

					$app->user_id=$user->id;

					$app->session_id=$session_id;

					$app->save();

					$response = ["session_id" => $session_id,

					"profile" => Users::getUser($user)];

					 $string =$request->password;
 
  				 $email = $request->email;

  				 Mail::send( 'emails.registration-confirmation',
    			 [  "password" => $string
    				 ],

     function ($m) use($request) 
     { 
                $m->from('testaativa@gmail.com', 'Your Name');

        	$m->to($request->email)->subject('Welcome to GIFT3Rapp');
     } 
     );

			}



			else

			{

				if($otpc)

				{

					$user=new Users();

					$user->email=$request->email;

					$user->name=$request->name;

					$user->phone=$phone_number;

					

					$user->password=md5($request->password);

					$user->save();

					$app=new AppLogin();

					$app->device_type=$request->device_type;

					$app->device_id=$request->device_id;

					$app->device_token=$request->device_token;

					$app->user_id=$user->id;

					$app->session_id=$session_id;

					$app->save();

					

					

					$response = ["session_id" => $session_id,

					"profile" => Users::getUser($user)];

					 $string =$request->password;
 
  				 $email = $request->email;

  				 Mail::send( 'emails.registration-confirmation',
    			 [  "password" => $string
    				 ],

     function ($m) use($request) 
     { 
                $m->from('testaativa@gmail.com', 'Your Name');

        	$m->to($request->email)->subject('Welcome to GIFT3R');
     } 
     );

				

				}



				else

				{

					return response ()->json (['error' => 'bad_request',

						'error_description' => "Please check your otp"

						],400);

				}

			}

		}else{

			return response ()->json (['error' => 'bad_request',

						'error_description' => "Otp code is invalid.Please check your otp"

						],400);

		}

	}else{

		return response ()->json (['error' => 'bad_request',

						'error_description' => "Otp code is invalid.Please check your otp"

						],400);

	}

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

	}





	//login 

	public function login(Request $request)

	{

		$validator = Validator::make ($request->all (),['email' => 'required',

			'password' => 'required',

			'device_type' =>'required',

			]);

		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()

				->first ()],400);



		} 

		$checkemail=Users::where("email", "=", $request->email)->first();

		
		

		$user = Users::where("email", "=", $request->email)->where("password", "=", md5($request->password))

		->first();

if (empty($checkemail) ) 

		{

			$statusCode = 400;

			$response = [

			"error" => "bad_request",

			"error_description" => "Your email does not exist please  Signup."

			];

		}

		elseif($user == null) 

		{

			$statusCode = 400;

			$response = [

			"error" => "bad_request",

			"error_description" => "Email or Password incorrect"

			];

		}

		elseif($user->status==0){

			$statusCode = 400;

			$response = [

			"error" => "bad_request",

			"error_description" => "You have been blocked by the admin.Ask admin to activate your account"

			];		

		}

		else 

		{

			 $session_id= str_random(32);

			 // $app_login = self::createAppLoginSession($request, $user->id);

			$checkdetail=AppLogin::where("user_id",'=',$user->id)->first();
			//print_r($checkdetail); die;
			if(!empty($checkdetail))
			{
				//echo "yes";
				AppLogin::where("id",$checkdetail->id)->update(["session_id" => $session_id,"device_type" => $request->device_type, "device_token" =>$request->device_token,"device_id" => $request->device_id]);
			}
			else
			{
				//echo "no"; 
				AppLogin::Create( ["session_id" => $session_id,"device_type" => $request->device_type,"device_token" => $request->device_token,
"device_id" => $request->device_id ,"user_id" => $user->id]);
                                                                                                
			}
			

                                                  

			$statusCode = 200;

			$response = [

			"session_id" =>  $session_id,

			"profile" => Users::getUser($user)

			];

		}

		$status_code=200;

		return response()->json($response, $statusCode, $headers = [], $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

	}

	public function changePassword(Request $request)

	{

		$user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}          

		$validator = Validator::make ( $request->all (),['password' => 'required',

			'new_password' =>'required',

			]);

		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()->first () 

				], 400 );

		}



		$user = AppLogin::where('session_id',$request->session_id)->pluck('user_id');

		$user = Users::where('id',$user)->first();

		if($user->password == md5($request->password))

		{

			$user->password=md5($request->new_password);

			$user->save();

			return response ()->json ([ 'message' => "password updated"

				], 200 );

		}

		else

		{

			return response ()->json(['error' => 'bad_request',

				'error_description' =>  "your old password is Incorrect.  "

				], 400 );

		}

	}

	//favourite store

	public function favouritestore(Request $request)
	{
			$user_id =  Users::validateSessionId($request->session_id);

			if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}  
		else
		{
			$validator = Validator::make ( $request->all (),['store_id' => 'required'

			

			]);

		if ($validator->fails ()) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()->first () 

				], 400 );

		}
		$store_id=$request->store_id;
		$checkexist=FavouriteStore::where(['user_id'=>$user_id,'store_id'=>$store_id])->first();

		//print_r(($checkexist)); die;

		if(!empty($checkexist))
		{
			
			if($checkexist->status==0)
			{

				$value=1;
				$msg="favourite this succesfully";
			}
			else
			{
				 $value=0;
			    $msg="unfavourite this succesfully";
			}

			FavouriteStore::where(['user_id'=>$user_id,'store_id'=>$store_id])->update(['status'=>$value]);

			return response ()->json ([

				'message' =>$msg ,'status'=>$value

				], 200 );

		}
		else
		{
			FavouriteStore::create(['user_id'=>$user_id,'store_id'=>$store_id,'status'=>1]);

			return response ()->json ([ 'message' => "favourite this succesfully.",'status'=>1

				], 200 );

		}





		}





	}

	//favourite store list

	 public function favouritestorelist(Request $request)
    {
    	$response=[];
    	$user_id =  Users::validateSessionId($request->session_id);

			if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}  

    	$validator = Validator::make ( $request->all (),['phone_number' => 'required'

			

			]);

		if ($validator->fails ())  
		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()->first () 

				], 400 );

		}

		$phone_number=str_replace(' ', '', $request->phone_number);
		$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$phone_number=str_replace($arrayfind,$arrayreplace, $phone_number); 
			$phone_number=trim($phone_number);
    	$checkphone = Users::where('phone',$phone_number)->first();
    	//////print_r($checkphone); die;

    	if (empty($checkphone)) 

		{

			return response ()->json ([ 'liststatus'=>0

				], 200 );

		}
		else
		{


			$favouritelist=Store::join('favourite_store','favourite_store.store_id','=','stores.id')->select('stores.id as s_id',

				'stores.name as s_name',

				'stores.image',

				'stores.zipcode',

				'stores.website',

				'stores.phone_number',

				'stores.email',

				'stores.city',

				'stores.address','favourite_store.user_id')
		        ->where('stores.status',1)

				->where('stores.deleted_at',NUll)
			    ->where('favourite_store.user_id',$checkphone->id)
                 ->where('favourite_store.status',1)
				//->skip($skip)->take($limit)

			->get();

			//print_r($favouritelist); die;

$user = Users::where(['id'=>$checkphone->id])->first();
			if(count($favouritelist)>0)
			{
				foreach($favouritelist as $list)
				{
					$card_count=GiftCards::where('store_id',$list->s_id)->count();

					// $cards=GiftCards::leftjoin('user_cards','user_cards.gift_card_id','=','gift_cards.id')->where('store_id',$list->s_id)->where('deleted_at',NULL)->select('gift_cards.id as gift_card_id','gift_cards.name as gift_card_name','gift_cards.price as price','user_cards.message as message')->get();

					$cards=GiftCards::where('store_id',$list->s_id)->where('deleted_at',NULL)->select('gift_cards.id as gift_card_id','gift_cards.name as gift_card_name','gift_cards.price as price')->get();

					// print_r($cards);
					// die();

					$status=$this->getfavouritestatus($user_id,$list->s_id);

					if($card_count)
					{
						$array[]=array(
						  'store_id'=>$list->s_id ?$list->s_id:"",
				          'name'=>$list->s_name ?$list->s_name:"",
				          'image'=>$list->image,
				          'zipcode'=>$list->zipcode ?$list->zipcode:"",
				          'address'=>$list->address ?$list->address:"",
				          'city'=>$list->city ?$list->city:"",
				          'website'=>$list->website ?$list->website:"",
				          'phone_no'=>$list->phone_number ?$list->phone_number:"",
				          'email'=>$list->email,
				          'available_cards'=>$cards ,
						  "status"=>$status);
						// $array=[];
						// $array=Store::getResponse($list);
						// $array['liststatus']=$this->getfavouritestatus($user_id,$list->s_id);
						// $array['profile']=Users::getUser($user);
						// $response[]=$array;
					}
				}
				
				$response=[
                   'profile'=>Users::getUser($user),
                   'favouritelist'=>$array
				];

				     $status_code=200;

					return response()->json($response,$status_code , $headers=[ ],

						$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
			}
			else
			{
				return response ()->json (['error' => 'bad_request',

				'error_description' => 'No Record Found.' ,'profile'=>Users::getUser($user)

				], 200 );

			}





		}






    }

    //  myfavouritestorelist

    public function myfavouritestorelist(Request $request)
    {
    	$user_id =  Users::validateSessionId($request->session_id);

			if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		}  

		$favouritelist=Store::join('favourite_store','favourite_store.store_id','=','stores.id')->select('stores.id as s_id',

				'stores.name as s_name',

				'stores.image',

				'stores.zipcode',

				'stores.website',

				'stores.phone_number',

				'stores.email',

				'stores.city',

				'stores.address')
		->where('stores.status',1)

				->where('stores.deleted_at',NUll)
			    ->where('favourite_store.user_id',$user_id)
                ->where('favourite_store.status',1)
				//->skip($skip)->take($limit)

			->get();
		

			//print_r($favouritelist); die;

			if(count($favouritelist)>0)
			{
				foreach($favouritelist as $list)
				{
					 $card_count=GiftCards::where('store_id',$list->s_id)->count();
					if($card_count){
					$array=[];
					$array=Store::getResponse($list);
					$array['status']=$this->getfavouritestatus($user_id,$list->s_id);
				$response[]=$array;

			}

			//}
				}
				
				//$user = Users::where(['id'=>$user_id])->first();

				$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
			}
			else
			{
				return response ()->json (['error' => 'bad_request',

				'error_description' => 'No Record Found.' 

				], 400 );

			}




    }







    // send app url to contact

    public function sendappurl(Request $request)
    {
    	$user_id =  Users::validateSessionId($request->session_id);

			if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403);

		} 
		$userdetail=Users::where('id',$user_id)->first();

    	$validator = Validator::make ( $request->all (),['phone_number' => 'required'

			

			]);

		if ($validator->fails ())  
		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => $validator->getMessageBag ()->first () 

				], 400 );

		}
		else
		{
			$phone_number = $request->phone_number;



			// $phone_number = trim ( $phone_number, "+" );

		$phone_number = str_replace(" ", "", $phone_number);
		$phone_number = str_replace("-", "", $phone_number);
		if(strlen($phone_number)>10)
		{
			$phone_number = str_replace("+", "", $phone_number);

		}
		else
		{
			$phone_number='1'.trim($phone_number);
		}

	   $applink="https://urlzs.com/2zzTx";
		//$applink="https://www.gift3rapp.com/apk/gift3r.apk";
		$applink1="https://testflight.apple.com/join/gstIrovl";
		//$applink="https://forms.gle/SQVTUGnG6DtbxWsPA";

		$text_message = "Your friend ".$userdetail->name." wants you to join GIFT3R App. Download today and start sharing small random acts of kindness with your loved onesplease Download GIFT3R app Google:" . " " .$applink."  Iphone : ".$applink1;



		// Functions::viaTwilio($phone_number, $text_message);
		$auth_id='MAYZU5ODVINJY2ODC0YJ';
            $auth_token='OWYwYjZhNjUxODgwZTU4NjQ5ODU3ZjI2NTMyNDQ4';
            $client = new RestClient($auth_id, $auth_token);
        if($client->messages->create(
                '14582038931',
                [$phone_number],
                $text_message
            )){
          
          $response = ["message" => "successfully send link"];
            header('Content-Type: application/json');
           echo json_encode($response);
			
		}


    }
}

    //forgot password

	public function forgotPassword(Request $request)

	{

		$user = Users::where('email',$request->email)->first();

		if($user==null)

		{

			return response()->json(['error' => 'bad_request',

				'error_description' => "email doesnot exists"

				], 400 );

		} 

		else

		{

			$string = str_random(32);
			$username=$user->name;

			Users::where('email',$request->email)->whereNull('deleted_at')->update(['recovery' => $string]);

			$email = $request->email;

			Mail::send( "api.email.forgot_password",

				[

				"recovery" => $string,
				"username" => $username,
				"email"   =>  $email

				],

				function ($m) use ($email) 

				{

					$m->from ( "testaativa@gmail.com", "support" );

					$m->to ($email)

					->subject ( "Choose a new password for GIFT3R" );

				} 

				);

			return response()->json(['message' => "The link to reset password has been sent to your e-mail"   

				] );

		}

	}

      //reset password form

	public function resetPassword(Request $request)

	{    

		$recovery = Users::where('recovery',$request->key)->first();

		if(! $recovery)

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' =>"link expired"

				], 200 );

		}

		return View('api.email.resetpassword',['id'=>$recovery->id]);

	}

	//reset password for new password

	public function resetPasswordPost(Request $request)

	{

		$validator = Validator::make ($request->all (),['new_password' => 'required', 

			'confirm_password' => 'required|same:new_password', 

			]);

		if($validator->fails ()) 

		{

			return response()->json(['error' => 'bad_request',

				'error_description' => $validator->getMessageBag()->first () 

				],400);

		}

		$id=$request->id;

		$user = Users::where('id',$id)->update(['password' => md5($request->new_password),

			'recovery' => 'null'

			]);

		return response ()->json(['message' =>"password updated"

			],200);

	}

	//logout

	public function logout(Request $request)

	{

		if($request->session_id==null)

		{

			return response()->json(['error'=>'bad_request',

				'error_description'=>"session id required" 

				],400);

		}

		else

		{

			AppLogin::where('session_id',$request->session_id)->delete();

			ob_get_clean();

			return response()->json(['message'=>"succesfully logout"],200);

		}

	}





	//store listing

	public function storeList(Request $request)

	{

		$user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' =>"Session Expired"

				], 403 );

		}

		$response=[];

		$limit=$request->limit?$request->limit:10;

		$skip=$request->page?($request->page-1)*$limit:0;



		//search

		$search = $request->search;

		

  

		if($search)

		{ 

			$info= Store::select('stores.id as s_id',

				'stores.name as s_name',

				'stores.image',

				'stores.zipcode',

				'stores.website',

				'stores.phone_number',

				'stores.email',

				'stores.city',

				'stores.address')->where('stores.status',1)->where('address','like','%'.$search.'%')->Orwhere('city','like','%'.$search.'%')->Orwhere('zipcode','like','%'.$search.'%')->skip($skip)->take($limit)->get();


              


			foreach ($info as $inf) 

			{

				$card_count=GiftCards::where('store_id',$inf->s_id)->count();

				if($card_count){
					$array=[];
					$array=Store::getResponse($inf);
					$array['status']=$this->getfavouritestatus($user_id,$inf->s_id);
				$response[]=$array;

			}



			}

		}

		else

		{

			$store=Store::where('stores.status',1)->select('stores.id as s_id',

				'stores.name as s_name',

				'stores.image',

				'stores.zipcode',

				'stores.website',

				'stores.phone_number',

				'stores.email',

				'stores.city',

				'stores.address')

				->where('deleted_at',NUll)

				//->skip($skip)->take($limit)

			->get();

	

			

			foreach ($store as $inf) 

			{ 	

			     //print_r($inf->s_id);

			    $card_count=GiftCards::where('store_id',$inf->s_id)->count();

			    //echo "<pre>"; print_r($card_count);

		if($card_count){

			$array=[];
					$array=Store::getResponse($inf);

					$array['status']=$this->getfavouritestatus($user_id,$inf->s_id);
				$response[]=$array;
				// $response[]=Store::getResponse($inf);

				



			}

		}

		}

	





		$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

	}

	public function getfavouritestatus($user_id,$store_id)
	{
		$status=0;
		$check=FavouriteStore::where(['user_id'=>$user_id,'store_id'=>$store_id,'status'=>1])->first();
		if(!empty($check) && !empty($check->status))
		{
			$status=1;
		}


		return $status;

	}

	public function store(Request $request)

	{

		$user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired"

				], 403 );

		}

		$response=[];

		$info=Store::where('stores.status',1)->where('id',$request->store_id)

		->select('stores.id as s_id',

			'stores.name as s_name',

			'stores.image',

			'stores.zipcode',

			'stores.website',

			'stores.phone_number',

			'stores.email',

			'stores.city',

			'stores.address')->get();



		foreach ($info as $inf) 

		{

			$response=Store::getResponse($inf);



		} 



		if(!$response)

		{

			$response=(['message'=>"No record found"]);



		}



		$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);



	}

	public function myCards(Request $request)

	{

		$user_id =  Users::validateSessionId($request->session_id);

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

				'error_description' => "Session Expired" 

				], 403 );

		}

		$search=$request->search;

		$response=[];



		$query=Usercards::leftjoin('gift_cards','gift_cards.id','user_cards.gift_card_id')->leftjoin('stores','gift_cards.store_id','stores.id')->leftjoin('transactions',function($join){
			$join->on('user_cards.transaction_id','=','transactions.id');
			$join->on('user_cards.mypurchasedcard_id','=','transactions.mypurchasedcard_id');
		})->where( function($q) use ($user_id)

			{

				$q->where('transactions.sender_id',$user_id)

				->orWhere('transactions.receiver_id',$user_id);

			})->where('user_cards.user_id',$user_id)->where('stores.status',1)->select('transactions.card_id' ,'gift_cards.name as g_name','stores.name as s_name','stores.image','transactions.mypurchasedcard_id','user_cards.message');

		

		if($search)

		{

			$query=$query->where('stores.status',1)->where( function($q) use ($request)

			{

				$q->where('gift_cards.name','like','%'.$request->search.'%')

				->orWhere('stores.name','like','%'.$request->search.'%');

			});



		}



		 $res=$query->orderBy('user_cards.transaction_id','DESC')->get();

		



		foreach ($res as $user_cards) 

		{   

			$check_null = UserCards::getResponse($user_cards,$user_id);

			

			if($check_null != 0){

			$response[]=UserCards::getResponse($user_cards,$user_id);

		}

		}

		$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);



	}

//send my cards

	public function sendCards(Request $request)

	{
       $response=[];
		$user_id =  Users::validateSessionId($request->session_id);
		$message="";
		if(!empty($request->message))
		{

			$message=$request->message;
		}

		if (! $user_id) 

		{

			return response ()->json (['error' => 'bad_request',

										'error_description' => "Session Expired" 

									], 403 );

		}

		

		//$number=Users::where('phone','LIKE','%'.$request->phone_number.'%')->get();

		$number=$request->phone_number;

		$num=preg_replace('/\s+/', '', $number);
		$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$num=str_replace($arrayfind,$arrayreplace, $num); 
			$num=trim($num);

		//$mobile = "919999999999";
        if (strlen($num)>10){
          // $mobile = substr($num, 3, 11);
           $mobile = substr($num,(strlen($num)-10),(strlen($num)-1));
        }else{
           $mobile=$num;
        }
       //  $mobile = substr($mobile, 2, 10);
           //echo $mobile;
         //print_r($mobile); die;
        
	 	$u_id=Users::where('phone','LIKE','%'. $mobile)->first();

	 //	print_r($u_id); die;

		$r_id=Users::where('phone','LIKE','%'.$mobile)->pluck('id')->first();

		$balance=GiftCards::where('id',$request->card_id)->where('deleted_at',NULL)->first();

		$bal=0;

		if(Users::where('phone','LIKE','%'.$mobile)->exists())

		{

			if($user_id==$r_id)

			{

				return response ()->json (['error' => 'bad_request',

										'error_description' => "you cannot send card to yourself" 

									], 400 );

			}



			$query=Usercards::leftjoin('gift_cards','gift_cards.id','user_cards.gift_card_id')->leftjoin('stores','gift_cards.store_id','stores.id')->leftjoin('transactions',function($join){
			$join->on('user_cards.transaction_id','=','transactions.id');
			$join->on('user_cards.mypurchasedcard_id','=','transactions.mypurchasedcard_id');
		})->where( function($q) use ($user_id)

			{

				$q->where('transactions.sender_id',$user_id)

				->orWhere('transactions.receiver_id',$user_id);

			})->where(['user_cards.user_id'=>$user_id,'user_cards.mypurchasedcard_id'=>$request->mypurchasedcard_id])->select('transactions.card_id' ,'gift_cards.name as g_name','stores.name as s_name','stores.image','transactions.mypurchasedcard_id','user_cards.message');

			 $res=$query->get();

       foreach ($res as $user_cards) 

		{             

	$bal=UserCards::getResponse($user_cards ,$user_id);

	//print_r($bal); die;

		}



			

			$trns=new Transaction();
			$trns->mypurchasedcard_id=$request->mypurchasedcard_id;
			$trns->sender_id=$user_id;

			$trns->receiver_id=$r_id;

			$trns->card_id=$request->card_id;

			$trns->status=1;

			$trns->transaction_balance=$bal['available_balance'] ?  $bal['available_balance'] : $balance['price'] ;

			$trns->save();

			

	 $last_id=Transaction::where(['card_id'=>$request->card_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->latest()->first();

         	if(Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$trns->card_id)->exists())

         	{

         	 $cards=Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$request->card_id)->update(['transaction_id'=>$last_id->id,'user_id'=>$trns->receiver_id,'message'=>$message]);

         	}

         	else

         	{

				$cards=new Usercards();
				$cards->mypurchasedcard_id=$request->mypurchasedcard_id;
				// $cards->user_id=$trns->sender_id;
				$cards->user_id=$trns->receiver_id;
				$cards->gift_card_id=$request->card_id;

				$cards->transaction_id=$trns->id;
				$cards->message=$message;

				$cards->save();



			

			}

			    if($user_id!=$r_id)
			    {
			    	$userdetail=Users::where('id',$user_id)->first();

				            $app_login=DB::table('app_login')->where('user_id','=',$r_id)->orderBy('created_at','DESC')->get();	
							//print_r($app_login); die;	  
				       if(!empty($app_login))
				       {
				       	  foreach($app_login as $a)
				       	  {
				       	  	   if ($a->device_type == 'android') {
				                $serverKey='AAAADFLKBRo:APA91bHZBpMBieKp4XQ7m2I7FrPLoshHN-xWsNmB-wmbc25EbfFXRIJW8TM7JgzXa9DEauGZz0rZWrXq58fhy_D2SgsKpbz1_7WQafxikHdnoB_DAjd_s3d23kvPM-P4QeaB8Ohtg2SL';
								    $registrationIds = $a->device_token;
								#prep the bundle
								     $msg = array
								          (
										'body' 	=> 'Your Friend '.$userdetail->name.' Send you an eGift Card to '.$bal['store_name'],
										'title'	=> 'GIFT3R',
								             	'icon'	=> 'myicon',/*Default Icon*/
								              	'sound' => 'mySound'/*Default sound*/
								          );
									$fields = array
											(
												'to'		=> $registrationIds,
												'notification'	=> $msg
											);
									
									
									$headers = array
											(
												'Authorization: key=' .$serverKey,
												'Content-Type: application/json'
											);
								#Send Reponse To FireBase Server	
										$ch = curl_init();
										curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
										curl_setopt( $ch,CURLOPT_POST, true );
										curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
										curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
										curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
										curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
										$result = curl_exec($ch );
										curl_close( $ch );
										//return $result;

				            } 

				            elseif ($a->device_type == 'iphone') {

				               //print_r($a->device_token); die;

				                   $url = "https://fcm.googleapis.com/fcm/send";

				                    $registrationIds = [ $a->device_token ];

				                     $serverKey ='AAAADFLKBRo:APA91bHZBpMBieKp4XQ7m2I7FrPLoshHN-xWsNmB-wmbc25EbfFXRIJW8TM7JgzXa9DEauGZz0rZWrXq58fhy_D2SgsKpbz1_7WQafxikHdnoB_DAjd_s3d23kvPM-P4QeaB8Ohtg2SL';

				                     $title = 'GIFT3R';

				                     $body = 'Your Friend '.$userdetail->name.' Send you an eGift Card to '.$bal['store_name'];

				                     $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' =>'0','msg_data'=>'');

				                     $arrayToSend = array('registration_ids' => $registrationIds, 'notification'=>$notification,'priority'=>'high');

				                     $json = json_encode($arrayToSend);

				                     $headers = array();

				                     $headers[] = 'Content-Type: application/json';

				                     $headers[] = 'Authorization: key='. $serverKey;



				                     $ch = curl_init();

				                     curl_setopt($ch, CURLOPT_URL, $url);

				                     curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");

				                     curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

				                     curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

				                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



				                     //Send the request

				                     $result = curl_exec($ch);

				                     if ($result === FALSE) 

				                     {

				                        die('FCM Send Error: ' . curl_error($ch));

				                     }



				                     curl_close( $ch );

				                     //return $result;

				            }
				       	  }
				       }
               }

			$response=['message'=>"send cards succesfully"];

        }



		else

		{ 
			$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$mobile=str_replace($arrayfind,$arrayreplace, $mobile); 
			$user=new Users();

			$user->phone=$mobile;

			$user->save();
			$r_id=$user->id;   
			if($user_id==$r_id)

			{

				return response ()->json (['error' => 'bad_request',

										'error_description' => "you cannot send card to yourself" 

									], 400 );

			}



			$query=Usercards::leftjoin('gift_cards','gift_cards.id','user_cards.gift_card_id')->leftjoin('stores','gift_cards.store_id','stores.id')->leftjoin('transactions',function($join){
			$join->on('user_cards.transaction_id','=','transactions.id');
			$join->on('user_cards.mypurchasedcard_id','=','transactions.mypurchasedcard_id');
		})->where( function($q) use ($user_id)

			{

				$q->where('transactions.sender_id',$user_id)

				->orWhere('transactions.receiver_id',$user_id);

			})->where(['user_cards.user_id'=>$user_id,'user_cards.mypurchasedcard_id'=>$request->mypurchasedcard_id])->select('transactions.card_id' ,'gift_cards.name as g_name','stores.name as s_name','stores.image','transactions.mypurchasedcard_id','user_cards.message');

			 $res=$query->get();

       foreach ($res as $user_cards) 

		{             

	$bal=UserCards::getResponse($user_cards ,$user_id);

	//print_r($bal); die;

		}



			

			$trns=new Transaction();
			$trns->mypurchasedcard_id=$request->mypurchasedcard_id;
			$trns->sender_id=$user_id;

			$trns->receiver_id=$r_id;

			$trns->card_id=$request->card_id;

			$trns->status=1;

			$trns->transaction_balance=$bal['available_balance'] ?  $bal['available_balance'] : $balance['price'] ;

			$trns->save();

			

	 $last_id=Transaction::where(['card_id'=>$request->card_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->latest()->first();

         	if(Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$trns->card_id)->exists())

         	{

         	 $cards=Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$request->card_id)->update(['transaction_id'=>$last_id->id,'user_id'=>$trns->receiver_id,'message'=>$message]);

         	}

         	else

         	{

				$cards=new Usercards();
				$cards->mypurchasedcard_id=$request->mypurchasedcard_id;
				// $cards->user_id=$trns->sender_id;
				$cards->user_id=$trns->receiver_id;
				$cards->gift_card_id=$request->card_id;

				$cards->transaction_id=$trns->id;
				$cards->message=$message;

				$cards->save();



			

			}


			$userdetail=Users::where('id',$user_id)->first();


			if(strlen($mobile)>10)
		{
			$mobile = str_replace("+", "", $mobile);;

		}
		else
		{
			$mobile='1'.trim($mobile);
		}

		 //$applink="https://www.gift3rapp.com/apk/gift3r.apk";
		// $applink="http://tiny.cc/qfsy8y";
		 $applink="https://urlzs.com/2zzTx";
		 $applink1="https://testflight.apple.com/join/gstIrovl";

		// $applink="https://forms.gle/SQVTUGnG6DtbxWsPA";

		$text_message = "Your friend ".$userdetail->name." send you a GIFT3R eGift Card. Download the app to receive your gift card download GIFT3R app Google:" . " " . $applink."  Iphone : ".$applink1;



		// Functions::viaTwilio($phone_number, $text_message);
		$auth_id='MAYZU5ODVINJY2ODC0YJ';
            $auth_token='OWYwYjZhNjUxODgwZTU4NjQ5ODU3ZjI2NTMyNDQ4';
            $client = new RestClient($auth_id, $auth_token);
        if($client->messages->create(
                '14582038931',
                [$mobile],
                $text_message
            )){
          
         
			
		}


			$response=['message'=>"send cards succesfully"];

		}

	

		$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

			$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);



		



}

//send cards detail

public function sendcarddetails(Request $request)
{
	$user_id =  Users::validateSessionId($request->session_id);
	if (! $user_id) 

	{

		return response ()->json (['error' => 'bad_request',

			'error_description' => "Session Expired" 

			], 403 );

	}

	 $gift_card_sent=Transaction::where('status',1)->where(['sender_id'=>$user_id])->get();
	 $response=(['gift_card_sent'=>$gift_card_sent]);
	 $status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

		$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
	 
}



	//redeem cards

public function redeemCards(Request $request)

{

	$user_id =  Users::validateSessionId($request->session_id);

	if (! $user_id) 

	{

		return response ()->json (['error' => 'bad_request',

			'error_description' => "Session Expired" 

			], 403 );

	}

$assist_name="";

if(!empty($request->assist_name))
{
	$assist_name=$request->assist_name;

}
	 //$amount=Transaction::where('card_id',$request->card_id)->pluck('transaction_balance')->last();

	 $amount=0;

 $query=Usercards::leftjoin('gift_cards','gift_cards.id','user_cards.gift_card_id')->leftjoin('stores','gift_cards.store_id','stores.id')->leftjoin('transactions',function($join){
			$join->on('user_cards.transaction_id','=','transactions.id');
		})->where( function($q) use ($user_id)

			{

				$q->where('transactions.sender_id',$user_id)

				->orWhere('transactions.receiver_id',$user_id);

			})->where(['user_cards.user_id'=>$user_id,'user_cards.mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('card_id',$request->card_id)->select('transactions.mypurchasedcard_id','transactions.card_id' ,'gift_cards.name as g_name','stores.name as s_name','stores.image','user_cards.message');

			$res=$query->get();

			// print_r($res); die;

        foreach ($res as $user_cards) 
        {             

	         $amount=UserCards::getResponse($user_cards ,$user_id);
        }



  // print_r($amount); die;

	if(floatval($request->amount) > floatval($amount['available_balance']))

	{	

		return response ()->json (['error' => 'bad_request',

			'error_description' => "Insufficient balance" 

			], 400);



	}

    else

	{

	$bill=BillAmount::Create(["receipt_num"=>$request->receipt_number,"bill_amount"=>$request->bill_amount]);

	

		if($request->amount <= $request->bill_amount)

		{	

			$trns=new Transaction();

		$trns->mypurchasedcard_id=$request->mypurchasedcard_id;
		$trns->sender_id=$user_id;

		$trns->card_id=$request->card_id;

		$trns->status=2;

		$trns->transaction_balance=-($request->amount);

		$trns->bill_amount_id=$bill->id;

		$trns->assist_name=$assist_name;

		$trns->save();

       

		$last_id=Transaction::where(['card_id'=>$request->card_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->latest()->first();

         

         if(Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$trns->card_id)->exists())

         {

         	$cards=Usercards::where(['user_id'=>$trns->sender_id,'mypurchasedcard_id'=>$request->mypurchasedcard_id])->where('gift_card_id',$trns->card_id)->update(['transaction_id'=>$last_id->id]);

         }

         else

         {

			$cards=new Usercards();

			$cards->mypurchasedcard_id=$request->mypurchasedcard_id;

			$cards->user_id=$user_id;

			$cards->gift_card_id=$request->card_id;

			$cards->transaction_id=$trns->id;

			$cards->save();

		}
         $user=DB::table('users')->where('id',$user_id)->first();
         $card_data=DB::table('gift_cards')->where('id',$request->card_id)->first();
         $store_data=DB::table('stores')->where('id',$card_data->store_id)->first();
         $balance_amount=$amount['available_balance']-$request->amount;
         		 Mail::send( 'emails.redeem-confirmation',
    			 [  
    			"amount"=>$request->amount,
    			 'user_name'=>$user->name,
    			 'balance_amount'=>$balance_amount,
    			 'gift_name'=>$card_data->name,
    			 'store_name'=>$store_data->name
    				 ],

     function ($m) use($user) 
     { 
                $m->from('admin@gift3r.com', 'Gift3r App');
                //$m->attach($url);

        	$m->to($user->email)->subject('Redeem Amount To Merchant');
     } 
     );
		$response=(['message'=>"Amount redeemed to merchant"]);

			

		}else{

             return response ()->json (['error' => 'bad_request',

			'error_description' => "Your total bill is less than you paid amount" 

			], 400);

		}

	}

	$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

		$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

	

}

//buy cards

public function buyCards(Request $request)

{

	

	$user_id =  Users::validateSessionId($request->session_id);

	$payment_id="";

	if(!empty($request->payment_id))
	{
		$payment_id=$request->payment_id;
	}

	$message="";
	if(!empty($request->message))
	{
		$message=$request->message;
	}

	if (! $user_id) 
	{

		return response ()->json (['error' => 'bad_request',

			'error_description' => "Session Expired" 

			], 403 );

	}

	$number=$request->phone_number;

	$num=preg_replace('/\s+/', '', $number);
	$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$num=str_replace($arrayfind,$arrayreplace, $num); 
			$num=trim($num);
			if(strlen($num)>10)
			{
				$num=substr($num,(strlen($num)-10),(strlen($num)-1));
				
				
			}

	$u_id=Users::where('phone','LIKE','%'.$num)->first();

	$r_id=Users::where('phone','LIKE','%'.$num)->pluck('id')->first();

	$balance_sum=GiftCards::where('id',$request->card_id)->where('deleted_at',NULL)->first();

		

		//send card to friend

	if($num)

	{

       if(Users::where('phone',$num)->exists())

		{

			if($user_id==$r_id)

			{

					return response ()->json (['error' => 'bad_request',

										'error_description' => "you cannot send card to yourself" 

									], 400 );

			

            }

            $uniqueid=$request->card_id.$user_id.time().mt_rand(0,1000);
			$purchasecard=new Mypurchasedcard();

			$purchasecard->card_id=$request->card_id;

			$purchasecard->sender_id=$user_id;

			$purchasecard->receiver_id=$r_id;

			$purchasecard->usercard=$uniqueid;

			$purchasecard->amount=$balance_sum['price'];

			$purchasecard->message=$request->message;

			$purchasecard->payment_id=$payment_id;

			$purchasecard->save();

			$purchasecardid=Mypurchasedcard::where(['card_id'=>$request->card_id,'sender_id'=>$user_id])->orderBy('id', 'desc')->first();


			$trns=new Transaction();

			$trns->mypurchasedcard_id=$purchasecardid->id;

			$trns->sender_id=$user_id;

			$trns->receiver_id=$r_id;

			$trns->card_id=$request->card_id;

			$trns->status=5;

			$trns->transaction_balance= $balance_sum['price'];

			$trns->save();
		

			$last_id=Transaction::where(['card_id'=>$request->card_id,'mypurchasedcard_id'=>$purchasecardid->id])->latest()->first();

         	if(Usercards::where(['user_id'=>$trns->receiver_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$trns->card_id)->exists())

         	{

         		$cards=Usercards::where(['user_id'=>$trns->receiver_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$trns->card_id)->update(['transaction_id'=>$last_id->id,'user_id'=>$last_id->receiver_id,'message'=>$message]);

         	}

         	else

         	{

				$cards=new Usercards();

			   $cards->mypurchasedcard_id=$purchasecardid->id;
				$cards->user_id=$last_id->receiver_id;

				$cards->gift_card_id=$request->card_id;

				$cards->transaction_id=$trns->id;
				$cards->message=$message;

				$cards->save();

			}

			$userdetail=Users::where('id',$user_id)->first();

				            $app_login=DB::table('app_login')->where('user_id','=',$r_id)->orderBy('created_at','DESC')->get();	
							//print_r($app_login); die;	  
				       if(!empty($app_login))
				       {
				       	  foreach($app_login as $a)
				       	  {
				       	  	   if ($a->device_type == 'android') {
				                $serverKey='AAAADFLKBRo:APA91bHZBpMBieKp4XQ7m2I7FrPLoshHN-xWsNmB-wmbc25EbfFXRIJW8TM7JgzXa9DEauGZz0rZWrXq58fhy_D2SgsKpbz1_7WQafxikHdnoB_DAjd_s3d23kvPM-P4QeaB8Ohtg2SL';
								    $registrationIds = $a->device_token;
								#prep the bundle
								     $msg = array
								          (
										'body' 	=> 'Your Friend '.$userdetail->name.' shared a GIFT3Rapp eGift card ',
										'title'	=> 'GIFT3R',
								             	'icon'	=> 'myicon',/*Default Icon*/
								              	'sound' => 'mySound'/*Default sound*/
								          );
									$fields = array
											(
												'to'		=> $registrationIds,
												'notification'	=> $msg
											);
									
									
									$headers = array
											(
												'Authorization: key=' .$serverKey,
												'Content-Type: application/json'
											);
								#Send Reponse To FireBase Server	
										$ch = curl_init();
										curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
										curl_setopt( $ch,CURLOPT_POST, true );
										curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
										curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
										curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
										curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
										$result = curl_exec($ch );
										curl_close( $ch );
										//return $result;

				            } 

				            elseif ($a->device_type == 'iphone') {

				               //print_r($a->device_token); die;

				                   $url = "https://fcm.googleapis.com/fcm/send";

				                    $registrationIds = [ $a->device_token ];

				                     $serverKey ='AAAADFLKBRo:APA91bHZBpMBieKp4XQ7m2I7FrPLoshHN-xWsNmB-wmbc25EbfFXRIJW8TM7JgzXa9DEauGZz0rZWrXq58fhy_D2SgsKpbz1_7WQafxikHdnoB_DAjd_s3d23kvPM-P4QeaB8Ohtg2SL';

				                     $title = 'GIFT3R';

				                     $body = 'Your Friend '.$userdetail->name.' shared a GIFT3Rapp eGift card  ';

				                     $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' =>'0','msg_data'=>'');

				                     $arrayToSend = array('registration_ids' => $registrationIds, 'notification'=>$notification,'priority'=>'high');

				                     $json = json_encode($arrayToSend);

				                     $headers = array();

				                     $headers[] = 'Content-Type: application/json';

				                     $headers[] = 'Authorization: key='. $serverKey;



				                     $ch = curl_init();

				                     curl_setopt($ch, CURLOPT_URL, $url);

				                     curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");

				                     curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

				                     curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

				                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



				                     //Send the request

				                     $result = curl_exec($ch);

				                     if ($result === FALSE) 

				                     {

				                        die('FCM Send Error: ' . curl_error($ch));

				                     }



				                     curl_close( $ch );

				                     //return $result;

				            }
				       	  }
				       }



			$response=(['message'=>"send cards succesfully"]);

			$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

		$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

			

		}

		else

		{
			$arrayfind=[' ','-','+','(',')'];
			$arrayreplace=['','','','',''];
			$num=str_replace($arrayfind,$arrayreplace, $num); 
			$user=new Users();

			$user->phone=$num;

			$user->save();
			$r_id=$user->id;

			if($user_id==$r_id)

			{

					return response ()->json (['error' => 'bad_request',

										'error_description' => "you cannot send card to yourself" 

									], 400 );

			

            }

            $uniqueid=$request->card_id.$user_id.time().mt_rand(0,1000);
			$purchasecard=new Mypurchasedcard();

			$purchasecard->card_id=$request->card_id;

			$purchasecard->sender_id=$user_id;

			$purchasecard->receiver_id=$r_id;

			$purchasecard->usercard=$uniqueid;

			$purchasecard->amount=$balance_sum['price'];

			$purchasecard->message=$request->message;

			$purchasecard->payment_id=$payment_id;

			$purchasecard->save();

			$purchasecardid=Mypurchasedcard::where(['card_id'=>$request->card_id,'sender_id'=>$user_id])->orderBy('id', 'desc')->first();


			$trns=new Transaction();

			$trns->mypurchasedcard_id=$purchasecardid->id;

			$trns->sender_id=$user_id;

			$trns->receiver_id=$r_id;

			$trns->card_id=$request->card_id;

			$trns->status=5;

			$trns->transaction_balance= $balance_sum['price'];

			$trns->save();
		

			$last_id=Transaction::where(['card_id'=>$request->card_id,'mypurchasedcard_id'=>$purchasecardid->id])->latest()->first();

         	if(Usercards::where(['user_id'=>$trns->receiver_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$trns->card_id)->exists())

         	{

         		$cards=Usercards::where(['user_id'=>$trns->receiver_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$trns->card_id)->update(['transaction_id'=>$last_id->id,'user_id'=>$last_id->receiver_id,'message'=>$message]);

         	}

         	else

         	{

				$cards=new Usercards();

			   $cards->mypurchasedcard_id=$purchasecardid->id;
				$cards->user_id=$last_id->receiver_id;

				$cards->gift_card_id=$request->card_id;

				$cards->transaction_id=$trns->id;
				$cards->message=$message;

				$cards->save();

			}

			$userdetail=Users::where('id',$user_id)->first();
			if(strlen($num)>10)
		{
			$num = str_replace("+", "", $num);;

		}
		else
		{
			$num='1'.trim($num);
		}

		//$applink="https://www.gift3rapp.com/apk/gift3r.apk";
		$applink="https://urlzs.com/2zzTx";
		$applink1="https://testflight.apple.com/join/gstIrovl";

		// $applink="https://forms.gle/SQVTUGnG6DtbxWsPA";

		$text_message = "Your friend ".$userdetail->name." send you a GIFT3R eGift Card. Download the app to receive your gift card download GIFT3R app Google:" . " " . $applink."  Iphone : ".$applink1;



		// Functions::viaTwilio($phone_number, $text_message);
		$auth_id='MAYZU5ODVINJY2ODC0YJ';
            $auth_token='OWYwYjZhNjUxODgwZTU4NjQ5ODU3ZjI2NTMyNDQ4';
            $client = new RestClient($auth_id, $auth_token);
        if($client->messages->create(
                '14582038931',
                [$num],
                $text_message
            )){
          
         
			
		}

			$response=(['message'=>"send cards succesfully"]);

			$status_code=200;

		return response()->json($response,$status_code , $headers=[ ],

		$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
			

		}

	}

	else

	{	

		$balance_sum=GiftCards::where('id',$request->card_id)->first();

		//print_r($balance_sum);die;
		    $uniqueid=$request->card_id.$user_id.time().mt_rand(0,1000);
			$purchasecard=new Mypurchasedcard();

			$purchasecard->card_id=$request->card_id;

			$purchasecard->sender_id=$user_id;

			$purchasecard->usercard=$uniqueid;

			$purchasecard->amount=$balance_sum['price'];

			$purchasecard->message=$request->message;

			$purchasecard->payment_id=$payment_id;

			$purchasecard->save();

			$purchasecardid=Mypurchasedcard::where(['card_id'=>$request->card_id,'sender_id'=>$user_id])->orderBy('id', 'desc')->first();
		

		$card_buy=new Transaction();

		$card_buy->mypurchasedcard_id=$purchasecardid->id;

		$card_buy->sender_id=$user_id;

		$card_buy->card_id=$request->card_id;

		$card_buy->transaction_balance=$balance_sum['price'];

		$card_buy->status=0;

		$card_buy->save();

		

		$last_id=Transaction::where('card_id',$request->card_id)->latest()->first();

        if(Usercards::where(['user_id'=>$card_buy->sender_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$card_buy->card_id)->exists())

        {

         	$cards=Usercards::where(['user_id'=>$card_buy->sender_id,'mypurchasedcard_id'=>$purchasecardid->id])->where('gift_card_id',$card_buy->card_id)->update(['transaction_id'=>$last_id->id,'message'=>$message]);

        }

        else

        {

			$cards=new Usercards();

			$cards->mypurchasedcard_id=$purchasecardid->id;
			$cards->user_id=$user_id;

			$cards->gift_card_id=$request->card_id;

			$cards->transaction_id=$card_buy->id;

			$cards->message=$message;
			$cards->save();

		}

		$response=(['message'=>"buy cards succesfully"]);

		$status_code=200;

	return response()->json($response,$status_code , $headers=[ ],

		$options = JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

	}

	}

	

	

	



}




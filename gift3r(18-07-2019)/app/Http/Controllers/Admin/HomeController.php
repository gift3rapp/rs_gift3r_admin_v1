<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\Users;
use App\Models\Store;
use App\Models\GiftCards;
use App\Models\City;
use App\Models\ZipCode;
use App\Models\Transaction;
use App\Models\Category;
use App\Library\Functions;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
  
 //login
  public function home(Request $request)
  {
   return View('home/login');
  }
   
//post login
 public function postlogin(Request $request){
   $validator = Validator::make ( $request->all (), [ 
    'email' => 'required',
    'password' => 'required']);
   if ($validator->fails ()) 
   {
    return redirect ( '/' )->withErrors ( $validator )->withInput ();
  }
  $password = md5 ( $request->password );
  $email=$request->email;

  $admin = Admin::where ( 'email', $email )->where('password',$password)->first ();
  if ($admin) {
    session (
      array (
        'user' => array (
          "email" => $admin->email,
          "name"=>$admin->name,
          "image"=>$admin->image,
          "role"=>'user'
          )
        ) );
    return redirect ( 'admin/dashboard' );
  } else {
    return redirect ()->back ()
    ->withErrors (
      array (
        "Wrong Email/Password"
        ) )
    ->withInput ();
  }

}
 //get form change password
public function changepassword(){
return View('pages/Admin/changepassword');
}
//post form change password
public function changepasswordpost(Request $request){
  $validator = Validator::make ( $request->all (), [ 
    'password' => 'required',
    'new_password' => 'required', 
    'confirm_password' => 'required|same:new_password', ] );
  if ($validator->fails ()) 
  {
    return redirect ()->back()->withErrors ( $validator );
  }
  $id =Admin::pluck('id'); 

   // check if entered password is correct
  $user = Admin::where('id', '=',$id)->first();
  $password = md5($request->password);
  
  if ($password==$user->password)
    {
      $user->password=md5($request->new_password);
      $user->save();
    }
  else
    {
      return redirect ()->back()->withErrors ("Password didnot match");
    }
 return redirect ()->back()->with('success',"Password Successfully changed");


}
     
//dashboard
public function dashboard(Request $request){
return View('pages/Admin/dashboard');
}

//count on dasboard
public function users(Request $request){
  $user_count=Users::where('deleted_at',NULL)->count();
 $merchant_count=Store::where('deleted_at',NULL)->count();
 $sale=Transaction::leftjoin('gift_cards','transactions.card_id','gift_cards.id')->count();
return View('pages/Admin/dashboard',['u_count'=>$user_count,'m_count'=>$merchant_count,'sale'=>$sale]);
}
//merchant saled
public function Sale(Request $request){

$merchant=Store::select('stores.id','stores.name','stores.email','stores.image','stores.phone_number','stores.website','stores.is_merchant')->paginate(5);

$merchan=Store::select('stores.id','stores.name','stores.email','stores.image','stores.phone_number','stores.website','stores.is_merchant')->first();

 
   // $mer_count=GiftCards::leftjoin('transactions','gift_cards.id','transactions.card_id')->where('transactions.sender_id',$id)->groupby('gift_cards.store_id')->count();
$mer_count=Transaction::where('sender_id',$merchan->id)->count('id');


  return view('pages/Admin/merchant_saled',['merchants'=>$merchant,'counts'=>$mer_count]);
  }
//get user details
public function userDetails(Request $request)
{
$search = '';
if($request->search){
$search = $request->search;
$user = Users::where(function($query) use($search)
                {
                    $query->where('name','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%')
                    ->orWhere('phone','like','%'.$search.'%');

                })->where('deleted_at',NULL)->orderBy('created_at','DESC')->paginate(5);
}
else{
$user=Users::where('deleted_at',NULL)->orderBy('created_at','DESC')->paginate(5);
}

return View('pages/Admin/users',['users'=>$user,'search'=>$search]);
}


   //update status
public function updateuser(Request $request){
$user_status=Users::where('deleted_at',NULL)->where('id',$request->id)->update(['status'=>$request->status]);
return redirect::back();
}
    
//add user
public function addUser(Request $request){
return View('pages/Admin/add_users');
}

public function addUserpost(Request $request){
 $validator = Validator::make($request->all(),[
   'name' => 'required',
   'email' => 'required|unique:users',
   'number'=>'required', ]);

 if($validator->fails())
 {
   return Redirect::back()->withErrors($validator )->withInput ();   
 } 
 $users = new Users();    
 $users->name = $request->name;
 $users->email = $request->email;
 $users->phone = $request->number;
 $users->save();
 return redirect::back()->with('success','sucesfully added');

}
//edit user form
public function editUser(Request $request){
$id=$request->id;
$user = Users::where('id',$id)->first();
return View('pages/Admin/edit_users',['user'=>$user,'id'=>$id]);
}
public function editUserpost(Request $request){
 $validator = Validator::make($request->all(),[
   'name' => 'required',
   'email' => 'required',
   'number'=>'required', ]);

 if($validator->fails())
 {
   return Redirect::back()->withErrors($validator )->withInput ();   
 } 

  $users = Users::where('id',$request->id)->update(['name'=>$request->name,'email'=>$request->email,'phone'=>$request->number]);    

 return redirect::back()->with('success','sucesfully updated');

}


     // merchnat details
public function merchnatsDetails(Request $request){
$search = '';
if($request->search){
$search = $request->search;
$merchant=Store::where('stores.name','like','%'.$search.'%')->where('deleted_at',NULl)->orderBy('created_at','DESC')->paginate(5);
}
else{
$merchant=Store::where('deleted_at',NULl)->orderBy('created_at','DESC')->paginate(5);
}
return View('pages/Admin/merchants',['merchants'=>$merchant,'search'=>$search]);
}

    //update status
public function updatemerchant(Request $request){

  // $mer=Store::where('deleted_at',NULL)->first();
   $mer_status=Store::where('deleted_at',NULL)->where('id',$request->id)->update(['status'=>$request->status]);

 // $mer_delete=Store::where('deleted_at',NULL)->where('id',$request->id)->update(['deleted_at'=>1]);

  // $merchant = Store::all();
  return redirect::back();
}

 //get form add merchants
public function addMerchants(Request $request){
//dropdown for category
  $c_id=Category::get();

  return View('pages/Admin/add_merchants',['c_id'=>$c_id]);
}
 //add merachnts info
public function addMerchantsPost(Request $request){
 $validator = Validator::make($request->all(),[
   'name' => 'required',
   'email' => 'required',
   'number'=>'required',
   'address' => 'required',
   'city' => 'required',
   'code'=>'required',
   'link' => 'required',
   'image'=>'image']);

 if($validator->fails())
 {
   return Redirect::back()->withErrors($validator )->withInput();   
 }
 
 $check_store = Store::where('email',$request->email)->WhereNull('deleted_at')->first();

 if($check_store){
 return Redirect::back()->withErrors('Email already exists!'); 
 }
 
 $store_password = str_random(10);
 $web=new Store();
 if($request->file('image')){
   $web->image=Functions::upload_picture($request->file('image'), '/public/assets/uploads/' ); 
 }
 else{
 $web->image = 'default_store.png';
 }

 $web->website=$request->link;
 $web->name=$request->name;
 $web->email=$request->email;
 $web->phone_number=$request->number;
 $web->latitude=$request->lat;
 $web->longitude=$request->log;
 $web->address=$request->address;
 $web->city=$request->city;
 $web->zipcode= $request->code ;
 $web->password = md5($store_password);
 $web->save();
 

 $merchants=Store::where('email',$request->email)->first();
 if($merchants==null)
 {
   return response()->json([ 
     'error' => 'bad_request',
     'error_description' => 'email doesnot exists'  
     ], 400 );
 } 
 else{
   $string = $store_password;
 
   $email = $request->email;

   Mail::send( 'emails.registration-merchantconfirmation',
     [  "password" => $string
     ],

     function ($m) use($request) 
     { 
                $m->from('testaativa@gmail.com', 'Your Name');

        	$m->to($request->email)->subject('Welcome to GIFT3Rapp ');
     } 
     );

 }


 return redirect::back()->with('success','sucesfully added');


}


 //get form edit merchants
public function editMerchants(Request $request)
{
//dropdown for category
  $id=$request->id;
   $c_id=Category::get();
   $merchants = Store::where('id',$request->id)->select('website','name','email','phone_number','latitude','longitude','address',
                                                        'city','zipcode','category_id','image as merchant_image')->first();
 return View('pages/Admin/edit_merchants',['c_id'=>$c_id,'id'=>$id,'merchants'=>$merchants]);
}
public function editMerchantsPost(Request $request)
{

 $validator = Validator::make($request->all(),[
   'name' => 'required',
   'email' => 'required',
   'number'=>'required',
   'address' => 'required',
   'city' => 'required',
   'code'=>'required',
   'link' => 'required',
   'image'=>'image']);

 if($validator->fails())
 {
   return Redirect::back()->withErrors($validator )->withInput();   
 }


if($request->image){
$image = Functions::upload_picture($request->file('image'), '/public/assets/uploads/');
}
else{
$image = $request->old_image;
}
   $web=Store::where('id',$request->id)->update(['website'=>$request->link,'name'=>$request->name,'email'=>$request->email,'phone_number'=>$request->number,'latitude'=>$request->lat,'longitude'=>$request->log,'address'=>$request->address,'city'=>$request->city,'zipcode'=>$request->code,'category_id'=>$request->category,'image'=>$image
    ]);
   return redirect::back()->with('success','sucesfully updated');

}



     //merchants desc
public function merchantsDesc(Request $request)
{ 
   $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id as g_id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email','stores.phone_number','stores.website','stores.city','stores.address','stores.zipcode','stores.id as s_id','stores.is_merchant','stores.status')->where('stores.id',$request->id)->first();
  
  $descs=Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id as g_id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('stores.id',$request->id)->get();

$num_cards = GiftCards::where('store_id',$request->id)->where('deleted_at',NULL)->count();
$total_sales = Transaction::leftJoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('stores','stores.id','gift_cards.store_id')
                          ->where('stores.id',$request->id)
                          ->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })->sum('transactions.transaction_balance');
$total_redeemed_amt = Transaction::leftJoin('gift_cards','gift_cards.id','transactions.card_id')->leftjoin('stores','stores.id','gift_cards.store_id')
                          ->where('stores.id',$request->id)
                          ->where(function($query)
                {
                    $query->where('transactions.status',2);

                })->sum('transactions.transaction_balance');


  return view('pages/Admin/merchants_desc',['descs' => $desc,'id'=>$request->id,'num_cards'=>$num_cards,'total_sales'=>$total_sales,'redeemed_amt'=>$total_redeemed_amt]);
}





     //merchants desc
public function showAllMerCards(Request $request)
{ 

  $descs=Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id as g_id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.deleted_at',NULL)->where('stores.id',$request->id)->orderBy('stores.created_at','DESC')->get();

  return view('pages/Admin/mer-all-cards',['cards' => $descs,'id'=>$request->id]);
}
    //user desc
public function usersDesc(Request $request)
{

  $id=$request->id;
  $desc =Transaction::select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')
                            ->where('transactions.sender_id',$request->id)->get();
  return view('pages/Admin/users_desc',['descs' => $desc,'id'=>$id]);
}
//user_cards
public function usersCard(Request $request)
{ 
 $id=$request->id;
 $status = $request->status;
 if($status==0){
  $user_cards=Transaction::where('sender_id',$request->id)->where(function($query)
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);

                })
                
                          ->select('bill_amount.receipt_num',
                                  'bill_amount.bill_amount',
                                  'u.name as s_name',
                                  'r.name as r_name',
                                  'transactions.status',
                                  'transactions.id as t_id',
                                  'gift_cards.name as g_name',
                                  'transactions.transaction_balance',
                                  'stores.name','stores.id as store_id',
                                  'transactions.receiver_id','gift_cards.id as g_id','transactions.updated_at',DB::raw('COUNT(gift_cards.id) as count'))
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('stores','gift_cards.store_id','stores.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->groupBy('transactions.card_id')->get();
                            }
elseif($status==1){
  $user_cards=Transaction::where('receiver_id',$request->id)->where(function($query)
                {
                    $query->where('transactions.status',1)
                    ->orWhere('transactions.status',5);

                })
                          ->select('bill_amount.receipt_num',
                                  'bill_amount.bill_amount',
                                  'u.name as s_name',
                                  'r.name as r_name',
                                  'transactions.status',
                                  'transactions.id as t_id',
                                  'gift_cards.name as g_name',
                                  'transactions.transaction_balance',
'stores.name','stores.id as store_id',
                                  'transactions.receiver_id','gift_cards.id as g_id','transactions.updated_at',DB::raw('COUNT(gift_cards.id) as count'))
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
 ->leftjoin('stores','gift_cards.store_id','stores.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->groupBy('transactions.card_id')->get();
}     
elseif($status==2){
  $user_cards=Transaction::where('sender_id',$request->id)->where(function($query)
                {
                    $query->where('transactions.status',1)
                    ->orWhere('transactions.status',5);

                })
                          ->select('bill_amount.receipt_num',
                                  'bill_amount.bill_amount',
                                  'u.name as s_name',
                                  'r.name as r_name',
                                  'transactions.status',
                                  'transactions.id as t_id',
                                  'gift_cards.name as g_name',
                                  'transactions.transaction_balance',
'stores.name','stores.id as store_id',
                                  'transactions.receiver_id','gift_cards.id as g_id','transactions.updated_at',DB::raw('COUNT(gift_cards.id) as count'))
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
 ->leftjoin('stores','gift_cards.store_id','stores.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->groupBy('transactions.card_id')->get();
}     
                 
return view('pages/Admin/user_cards',['cards'=>$user_cards,'id'=>$id]);
}

//store nd card history
public function history(Request $request)
{
    $start_date = '';
    $end_date = '';
 $query = DB::table('transactions');
    if($request->start_date||$request->end_date){
    $start_date = $request->start_date;
    $end_date = $request->end_date;
    if($request->start_date && !$request->end_date){
    $query->where('transactions.created_at','>=',$start_date);
    }
    elseif(!$request->start_date && $request->end_date){
    $query->where('transactions.created_at','<=',$end_date);
    }
    elseif($request->start_date && $request->end_date){
    $query->where('transactions.created_at','>=',$start_date)->where('transactions.created_at','<=',$end_date);
    }    
  }

  
  $id=$request->id;

  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email','stores.phone_number')->where('stores.id',$request->id)
  ->first();
 $history=$query->select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id','transactions.sender_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)
                            ->where(function($db) use($request){
                                 $db->where('sender_id',$request->user_id)
                                 ->orwhere('receiver_id',$request->user_id);
                            })
                            ->paginate(50);
                            
                  
                            // return $request->id;
                          //  return $history;
return view('pages/Admin/transactionhistory',['history'=>$history,'id'=>$id,'desc'=>$desc,'user_id'=>$request->user_id, 'start_date'=>$start_date,'end_date'=>$end_date]);
}

public function purchasedHistory(Request $request)
{
  $id=$request->id;
  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('stores.id',$request->id)->first();


 $history=Transaction::select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->card_id)
                            ->where('transactions.id',$request->id)
                            ->get();
                            // return $request->id;
return view('pages/Admin/transactionhistory',['history'=>$history,'id'=>$id,'desc'=>$desc]);
}

public function receivedHistory(Request $request)
{
  $id=$request->card_id;
  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('stores.id',$request->card_id)->first();


 $history=Transaction::select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->card_id)
                            ->where('transactions.id',$request->id)
                            ->paginate(50);
                            // return $request->id;
return view('pages/Admin/transactionhistory',['history'=>$history,'id'=>$id,'desc'=>$desc]);
}

public function sendcardshistory(Request $request)
{
 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('stores.id',$request->id)->first();
 $history=Transaction::select('u.name as s_name',
                              'r.name as r_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->where('transactions.status',1)->paginate(50);
                            return view('pages/Admin/sendcards',['history'=>$history,'id'=>$id,'desc'=>$desc]);
}
public function buycardshistory(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
    $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=0,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));

 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at','transactions.mypurchasedcard_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->where(function($query) 
                {
                    $query->where('transactions.status',0)
                    ->orWhere('transactions.status',5);
                })->orderby('transactions.id','desc')->paginate(50);
                            return view('pages/Admin/buycards',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,
                            'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}

public function purchasedcarddetail(Request $request)
{
  $html="";

  $purchasedcarddetail=Transaction::leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')->where('transactions.mypurchasedcard_id',$request->purchasecard_id)->select('u.name as sender','r.name as receiver','transactions.transaction_balance','transactions.created_at','transactions.status','u.phone as usercontact','r.phone as receivercontact','transactions.assist_name')->get();
                            //print_r( $purchasedcarddetail);

  if(count($purchasedcarddetail)>0)
  {
    foreach($purchasedcarddetail as $detail)
    {
      $action="";
      if($detail->status=="0")
      {
        $action="Buy For Self";
      }
      if($detail->status=="5")
      {
        $action="Buy For Other";
      }
      if($detail->status=="1")
      {
        $action="Send Card";
      }
      if($detail->status=="2")
      {
        $action="Redeem Card";
      }

       $html.="<tr><td>".$detail->sender."</td><td>".$detail->usercontact."</td><td>".$detail->receiver."</td><td>".$detail->receivercontact."</td><td>".abs($detail->transaction_balance)."</td><td>".$action."</td><td>".$detail->assist_name."</td><td>".date ( "jS M, Y", strtotime ( $detail ->created_at))."</td></tr>";


    }

  }

  return $html;




}

public function redeemcardshistory(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
  $id=$request->id;
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
        $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=2,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));
  
  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();


 $history=$query->select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.created_at',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->where('transactions.status',2)->paginate(50);
                            // return $request->id;
return view('pages/Admin/redeemcards',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}


public function userCardsHistory(Request $request)
{
 $id=$request->id;
  $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('stores.id',$request->id)->first();
   $card_id=$desc->id;

 $history=Transaction::select('bill_amount.receipt_num',
                                    'bill_amount.bill_amount',
                                    'u.name as s_name',
                                    'r.name as r_name',
                                    'transactions.status',
                                    'transactions.id as t_id',
                                    'gift_cards.name as g_name',
                                    'transactions.transaction_balance',
                                    'transactions.receiver_id','gift_cards.id as g_id')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('card_id',$request->id)->paginate(50);
                            // return $request->id;
return view('pages/Admin/user_cards_history',['history'=>$history,'id'=>$id,'desc'=>$desc,'c_id'=>$card_id]); 
}
//delete merchant

public function deletemerchant(Request $request)
{
  $mer=Store::where('id',$request->mer_id)->update(['deleted_at'=>1]);
 return redirect::back();
}
public function deleteusers(Request $request)
{
   $user_status=Users::where('id',$request->user_id)->update(['deleted_at'=>1]);
  // $user = Users::all();
  return redirect::back();
  
}

public function showCards(Request $request){
$num_purchased_cards = Transaction::where('sender_id',$request->id)->where(function($query)
                {
                    $query->where('status',0)
                    ->orWhere('status',5);

                })->count();
$num_received_cards = Transaction::where('receiver_id',$request->id)->where(function($query)
                {
                    $query->where('status',1)
                    ->orWhere('status',5);

                })->count();
$num_sent_cards = Transaction::where('sender_id',$request->id)->where(function($query)
                {
                    $query->where('status',1)
                    ->orWhere('status',5);

                })->count();
return view('pages/Admin/cards')->with(['purchased'=>$num_purchased_cards,'received'=>$num_received_cards,'sent'=>$num_sent_cards,'id'=>$request->id]);
}

public function showMerCards(Request $request){
$num_purchased = Transaction::where('card_id',$request->id)->where(function($query) 
                {
                    $query->where('status',0)
                    ->orWhere('status',5);
                })->count();
$total_sales = Transaction::where('card_id',$request->id)->where(function($query) 
                {
                    $query->where('status',0)
                    ->orWhere('status',5);
                })->sum('transaction_balance');
$redemption_amount= Transaction::where('card_id',$request->id)->where('status',2)->sum('transaction_balance');
return view('pages/Admin/mer_cards')->with(['purchased'=>$num_purchased,'total_sales'=>$total_sales,'redemption_amount'=>$redemption_amount,'id'=>$request->id]);
}



//logout
public function logout(Request $request) 
{
  $type= session('user.role');
  $request->session ()
  ->flush ();

  return redirect('admin/login'); 

} 

public function totalPurchased(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
    $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=0,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));

 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')
                            ->where('gift_cards.store_id',$request->id)
                            ->where(function($db)
                {
                    $db->where('transactions.status',0)
                       ->orwhere('transactions.status',5);

                })->paginate(50);
                           
                           
                            
return view('pages/Admin/total_purchased',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}


public function totalRedemption(Request $request)
{
    $search = '';
    $start_date = '';
    $end_date = '';
  $query = DB::table('transactions');
    if($request->search||$request->start_date||$request->end_date){
     $search = $request->search;
    $start_date = $request->start_date;
    $end_date = $request->end_date;
  $query = Transaction::getSearch($request,$status=2,$query);
  }
  $dates = Transaction::generateDateRange(Carbon::parse('2018-01-01'),Carbon::parse('2018-12-31'));

 $id=$request->id;
 $desc = Store::leftjoin('gift_cards','gift_cards.store_id','stores.id')->select('gift_cards.id','gift_cards.name as g_name','gift_cards.price','stores.name','stores.image','stores.email')->where('gift_cards.id',$request->id)->first();
 $history=$query->select('u.name as s_name',
                              'transactions.id as t_id',
                              'gift_cards.name as g_name',
                              'transactions.transaction_balance',
                               'transactions.receiver_id','gift_cards.id as g_id','transactions.created_at','bill_amount.bill_amount','bill_amount.receipt_num','transactions.assist_name')
                            ->leftjoin('users as u','transactions.sender_id','u.id')
                            ->leftjoin('users as r','transactions.receiver_id','r.id')
                            ->leftjoin('users','transactions.sender_id','users.id')
                            ->leftjoin('gift_cards','transactions.card_id','gift_cards.id')
                            ->leftjoin('bill_amount','transactions.bill_amount_id','bill_amount.id')->where('gift_cards.store_id',$request->id)->where('transactions.status',2)->orderby('transactions.id','desc')->paginate(50);
                            
return view('pages/Admin/total_redemption',['history'=>$history,'id'=>$id,'desc'=>$desc,'dates'=>$dates,
    'search'=>$search,'start_date'=>$start_date,'end_date'=>$end_date]);
}
}

<?php
 require('nuvei_account.php');
require('gateway_tps_xml.php');
require('nuvei_hpp_functions.php');
 
# These values are specific to the cardholder.
$cardNumber = '4111111111111111';		# This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
$cardType = 'VISA';			# See our Integrator Guide for a list of valid Card Type parameters
$cardExpiry = '0807';		# (if not using SecureCard) The 4 digit expiry date (MMYY).
$cardHolderName = 'John Albert';		# (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
$cvv = '214';			# (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
$issueNo = '';			# (optional) Issue number for Switch and Solo cards.
$email = '';			# (optional) If this is sent then Nuvei will send a receipt to this e-mail address.
$mobileNumber = "";		# (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

# These values are specific to the transaction.
$orderId = '';			# This should be unique per transaction (12 character max).
$amount = '15.05';			# This should include the decimal point.
$isMailOrder = false;		# If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.

# These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
$address1 = '';			# (optional) This is the first line of the cardholders address.
$address2 = '';			# (optional) This is the second line of the cardholders address.
$postcode = '';			# (optional) This is the cardholders post code.
$country = '';			# (optional) This is the cardholders country name.
$phone = '';			# (optional) This is the cardholders home phone number.

# eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
$cardCurrency = '';		# (optional) This is the three character ISO currency code returned in the rate request.
$cardAmount = '';		# (optional) This is the foreign currency transaction amount returned in the rate request.
$conversionRate = '720';		# (optional) This is the currency conversion rate returned in the rate request.

# 3D Secure reference. Only include if you have verified 3D Secure throuugh the Nuvei MPI and received an MPIREF back.
$mpiref = '';			# This should be blank unless instructed otherwise by Nuvei.
$deviceId = '';			# This should be blank unless instructed otherwise by Nuvei.

$autoready = '';		# (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
$multicur = false;		# This should be false unless instructed otherwise by Nuvei.

$description = '';		# (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
$autoReady = '';		# (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
if(!isset($orderId) || $orderId == '') 
	$orderId = generateUniqueOrderId();
echo "order id: ".$orderId."<br/>";
# Set up the authorisation object
$auth = new XmlAuthRequest($terminalId,$orderId,$currency,$amount,$cardNumber,$cardType);
if($cardType != "SECURECARD") $auth->SetNonSecureCardCardInfo($cardExpiry,$cardHolderName);
if($cvv != "") $auth->SetCvv($cvv);
if($cardCurrency != "" && $cardAmount != "" && $conversionRate != "") $auth->SetForeignCurrencyInformation($cardCurrency,$cardAmount,$conversionRate);
if($email != "") $auth->SetEmail($email);
if($mobileNumber != "") $auth->SetMobileNumber($mobileNumber);
if($description != "") $auth->SetDescription($description);
 
if($issueNo != "") $auth->SetIssueNo($issueNo);
if($address1 != "" && $address2 != "" && $postcode != "") $auth->SetAvs($address1,$address2,$postcode);
if($country != "") $auth->SetCountry($country);
if($phone != "") $auth->SetPhone($phone);
 
if($mpiref != "") $auth->SetMpiRef($mpiref);
if($deviceId != "") $auth->SetDeviceId($deviceId);
 
if($multicur) $auth->SetMultiCur();
if($autoready) $auth->SetAutoReady($autoready);
if($isMailOrder) $auth->SetMotoTrans();
 
# Perform the online authorisation and read in the result
$response = $auth->ProcessRequestToGateway($secret,$testAccount, $gateway);
 
 
 
$expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);
//print_r($response->Hash());
 
if($response->IsError()) echo 'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
elseif($expectedResponseHash == $response->Hash()) {
 	switch($response->ResponseCode()) {
		case "A" :	# -- If using local database, update order as Authorised.
				echo 'Payment Processed successfully. Thanks you for your order.<br/>';
				echo "<pre>"; print_r($response);
				$uniqueRef = $response->UniqueRef();
				$responseText = $response->ResponseText();
				$approvalCode = $response->ApprovalCode();
				$avsResponse = $response->AvsResponse();
				$cvvResponse = $response->CvvResponse();
				break;
		case "R" :
		case "D" :
		case "C" :
		case "S" :
 
		default  :	# -- If using local database, update order as declined/failed --
				echo 'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
	}
} else {
	$uniqueReference = $response->UniqueRef();
	echo 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
	if(isset($uniqueReference)) echo 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Unique Reference: ' . $uniqueReference . ' when mailing or calling.';
 }
 
?>
<?php
 
# This function returns the URL that should be used as the "action" for the form posting the Nuvei's servers.
function requestURL() {
	global $gateway, $testAccount;
	$url = 'https://';
	if($testAccount) { $url .= 'test'; }
	switch (strtolower($gateway)) {
		case 'cashflows' : $url .= 'cashflows.nuvei.com'; break;
		case 'payius' : $url .= 'payments.payius.com'; break;
		default :
		case 'nuvei'  : $url .= 'payments.nuvei.com'; break;
	}
   $url .= '/merchant/paymentpage';

	//print_r($url);
	return $url;
}
 
# This simply reduces the PHP code required to build the form.
function writeHiddenField($fieldName, $fieldValue) {
	echo "<input type='hidden' name='" . $fieldName . "' value='" . $fieldValue . "' />\r";
}
 
# This generates a DATETIME value in the correct format expected in the request.
function requestDateTime() {
	return date('d-m-Y:H:i:s:000');
}
 
# If you are not using your own Order ID's and need to use unique random ones, this function will generate one for you.
function generateUniqueOrderId() {
	$seconds = date('H')*3600+date('i')*60+date('s');
	return date('zy') . $seconds;
}
 
# This is used to generate the Authorisation Request Hash.
function authRequestHash($orderId, $amount, $dateTime) {
	global $terminalId, $secret, $receiptPageURL, $validationURL;
	return md5($terminalId . $orderId . $amount . $dateTime . $receiptPageURL . $validationURL . $secret);
}
 
# This function is used to validate that the Authorisation Response Hash from the server is correct.
#     If authResponseHashIsValid(...) != $_REQUEST["HASH"] then an error should be shown and the transaction should not be approved.
function authResponseHashIsValid($orderId, $amount, $dateTime, $responseCode, $responseText, $responseHash) {
	global $terminalId, $secret;
	return (md5($terminalId . $orderId . $amount . $dateTime . $responseCode . $responseText . $secret)==$responseHash);
}
 
?>
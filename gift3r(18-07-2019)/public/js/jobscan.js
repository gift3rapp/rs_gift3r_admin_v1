$("#btnAddSkill").click(
		function() {
			$.post($("#myForm").attr("action"), $("#myForm :input")
					.serializeArray(), function(info) {
				$("#result").html(info);
			});
			clearInput();
		});
$("#myForm").submit(function() {
	return false;
});
function clearInput() {
	$("#myForm :input").each(function() {
		$("#skill").val('');
	});
}
function centerModals() {
	$('.modal').each(
			function(i) {
				var $clone = $(this).clone().css('display', 'block').appendTo(
						'body');
				var top = Math.round(($clone.height() - $clone.find(
						'.modal-content').height()) / 2);
				top = top > 0 ? top : 0;
				$clone.remove();
				$(this).find('.modal-content').css("margin-top", top);
			});
}
function notifyInline(idToPrependTo, jsonResult) {
	var uniqueid = Date.now();
	var jsonArray = JSON.parse(jsonResult);
	var message = jsonArray[0];
	var status = jsonArray[1];
	var colorClass = (status === 'error') ? 'alert-danger' : '';
	var notice = '<div id="' + uniqueid + '" class="alert alert-info fade in '
			+ colorClass + '">'
			+ '<a href="#" class="close" data-dismiss="alert">&times;</a>'
			+ '<p>' + message + '</p>' + '<div>';
	$('#' + idToPrependTo).prepend(notice);
	setTimeout(function() {
		$('#' + uniqueid).fadeOut();
	}, 3000);
}